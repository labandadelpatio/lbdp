
<?php
/*  Fichero para la vista de search de las entregas 
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 07/11/2017*/
class DELIVERABLES_SEARCH {


    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

	function __construct(){
		$this->Render();

	}

	function Render(){
    include '../Views/Header.php'; //header necesita los string
		?>
			<h1><?php echo $strings['Search']; ?></h1>

    <div id = "searchform"> <?php //Mostraremos el formulario de SEARCH que permite buscar por todos los campos requerido?>
			<form method="post" enctype="multipart/form-data" name="formadd" action='../Controllers/Deliverables_Controller.php?action=SEARCH' autocomplete="off" onsubmit="">
    <div>
        <label><?php echo $strings['Login']; ?>:</label>
        <input type="text" id="login" name="login" maxlength="9" size="9" onblur="" />
				<div class="error" id="loginE"></div></div>
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id='idtrabajo' name="idtrabajo" maxlength="6" size="6" onblur="" />
				<div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Alias Evaluado']; ?>:</label>
        <input type="text" id="alias" name="alias" maxlength="6" size="6" onblur="" />
				<div class="error" id="aliasE"></div></div>
    <div>
        <label><?php echo $strings['Horas']; ?>:</label>
        <input type="text" id="horas" name="horas" size="2" maxlength="2" onblur="" />
				<div class="error" id="horasE"></div></div>
    <div>
        <label><?php echo $strings['Ruta']; ?>:</label>
        <input type="text" id="ruta" name="ruta" maxlength="60" size="60" onblur="" />
				<div class="error" id="rutaE"></div></div>


         <div>
        <div id="imgtable"><button type="submit"  id='SEARCH'><img src="../Views/icons/Find.png"></button>
        <a href="../Controllers/Deliverables_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
      </form>
    </div>

		<?php
			 include '../Views/Footer.php'; //header necesita los string

  }
}


?>
