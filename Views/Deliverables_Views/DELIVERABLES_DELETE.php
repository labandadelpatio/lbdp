
<?php
/*  Fichero para la vista de delete de las entregas 
   Autor: lbdp
  Fecha: 07/11/2017*/
class DELIVERABLES_DELETE{

    
    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista
	
	function __construct($valores){
		$this->Render($valores);

	}

	function Render($valores){
  include '../Views/Header.php'; //header necesita los string

?>
<h1><?php echo $strings['delete']; ?></h1>
<div id="delete"><table>
        <tr>
            <th><?php echo $strings['Login']; ?></th>
            <td><?php echo $valores[0]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <td><?php echo $valores[1]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Alias Evaluado']; ?></th>
            <td><?php echo $valores[2]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Horas']; ?></th>
            <td><?php echo $valores[3]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Ruta']; ?></th>
            <td><?php echo $valores[4]; ?></td>
        </tr>
       
        </table>

        <form name='x' method='post' action='../Controllers/Deliverables_Controller.php?action=DELETE'>
            <input type="hidden" name="login" value="<?php echo $valores[0]?>"/>
             <input type="hidden" name="idtrabajo" value="<?php  echo $valores[1]?>"/>
        </form>
            <div id="imgtabledel">
            <div id="imgtable"><button type="submit" onclick=x.submit()>
            <img src="../Views/icons/Erase.png"/></button><a href="../Controllers/Deliverables_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        </div>


<?php
       include '../Views/Footer.php'; //header necesita los string

  } 
}


?>
