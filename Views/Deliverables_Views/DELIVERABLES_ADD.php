
<?php
/*  Fichero para la vista de add de las entregas 
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 07/11/2017*/
class DELIVERABLES_ADD {

    
    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct(){
        $this->Render();

    }

    function Render(){
    include '../Views/Header.php'; //header necesita los string
        ?>
            <h1><?php echo $strings['Add']; ?></h1>

    <div id = "formulario"> <?php //Mostaremos el formulario de ADD  ?>
            <form method="post" enctype="multipart/form-data" name="formAdd" action='../Controllers/Deliverables_Controller.php?action=ADD' autocomplete="off" onsubmit="return validar_Deliverables(login, idtrabajo, aliasEvaluado, horas, ruta)">
    <div>
        <label><?php echo $strings['Login']; ?>:</label>
        <input type="text" id="login" name="login" maxlength="9" size="9" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="loginE"></div></div>
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id='idtrabajo' name="idtrabajo" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,6) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Alias Evaluado']; ?>:</label>
        <input type="text" id="alias" name="alias" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="aliasE"></div></div>
    <div>
        <label><?php echo $strings['Horas']; ?>:</label>
        <input type="text" id="horas" name="horas" size="2" maxlength="2" onblur="comprobarVacio(this) && comprobarEntero(this, 0, 99)" />
                <div class="error" id="horasE"></div>
    </div>
    <div>
        <label><?php echo $strings['Ruta']; ?>:</label>
        <input type="file" id="ruta" name="ruta" maxlength="60" size="60" accept="File/rar, .zip, .jpeg, File/gif" onblur="comprobarVacio(this)" />
                <div class="error" id="rutaE"></div></div>

    <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <button type="reset" name="limpiar"><img src="../Views/icons/Undo.png"></button><a href="../Controllers/Deliverables_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>

        <?php
             include '../Views/Footer.php'; //header necesita los string

  }
}


?>
