
<?php
/*  Fichero para la vista del showcurrent de las entregas 
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class DELIVERABLES_SHOWCURRENT {

	function __construct($valores){
		$this->Render($valores);

	}

	function Render($valores){
    include '../Views/Header.php'; //header necesita los string

?>
<h1><?php echo $strings['current']; ?></h1>
<div id="current"><table>
        <tr>
            <th><?php echo $strings['Login']; ?></th>
            <td><?php echo $valores[0]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <td><?php echo $valores[1]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Alias Evaluado']; ?></th>
            <td><?php echo $valores[2]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Horas']; ?></th>
            <td><?php echo $valores[3]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Ruta']; ?></th>
            <td><a href=<?php echo $valores[4]; ?> download><?php echo $valores[4]; ?></a></td></td>
        </tr>

        </table>

        <div id="imgtableshw">
            <a href="../Controllers/Deliverables_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        </div>



<?php
        include '../Views/Footer.php'; //header necesita los string

  }
}


?>
