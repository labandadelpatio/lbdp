
<?php
/*  Fichero para la vista del showall de las entregas
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 07/11/2017
*/
class DELIVERABLES_SHOWALL {


    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

	function __construct($fila, $resultado){
		$this->Render($fila, $resultado);

	}

	function Render($fila, $resultado){
  include_once '../Functions/TienePermisoFuncAcc.php';  
  include_once'../Views/Header.php'; //header necesita los string


?>
    <div id="showall">
        <div id="imgtable">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 0, 2)){ //según los permisos , funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
				<a href="../Controllers/Deliverables_Controller.php?action=SEARCH"><img src="../Views/icons/Find.png"/></a>
<?php }
?>
				<a href="../Controllers/Deliverables_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        <table>
        <tr>
            <th><?php echo $strings['Login']; ?></th>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <th><?php echo $strings['Nombre Trabajo']; ?></th>
            <th><?php echo $strings['Alias Evaluado']; ?></th>
            <th><?php echo $strings['Horas']; ?></th>
            <th><?php echo $strings['Ruta']; ?></th>
            <th><?php echo $strings['opciones']; ?></th>
   </tr>


<?php
if(tienePermisoFuncAcc($_SESSION['login'], 0, 0)){ //según los permisos , funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono

	while($fila = $resultado->fetch_assoc()){ //mientras haya datos recorreremos el array mostrando los dtaos en una tala

	   if(isUser($fila['login']) || isAdmin($_SESSION['login'])){

	            echo "<tr>";
	            echo "<td>".$fila['login']."</td>";
	            echo "<td>".$fila["IdTrabajo"]."</td>";
                echo "<td>".$fila["NombreTrabajo"]."</td>";
	            echo "<td>".$fila["Alias"]."</td>";
	            echo "<td>".$fila["Horas"]."</td>";
	            echo "<td><a href= \"" . $fila["Ruta"] . "\" download>" . $fila["Ruta"] . "</a></td>";

	
?>
            <td>
                <div id="imagsall">

<?php 
			if( tienePermisoFuncAcc($_SESSION['login'], 0, 3)){ //según los permisos , funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
									<a href="../Controllers/Deliverables_Controller.php?action=EDIT&login=<?php echo $fila['login']?>&idtrabajo=<?php echo $fila['IdTrabajo'] ?>"><img src="../Views/icons/Modify.png", width="14"/></a>
<?php }
			if( tienePermisoFuncAcc($_SESSION['login'], 0, 5)){ //según los permisos , funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
									<a href="../Controllers/Deliverables_Controller.php?action=SHOWCURRENT&login=<?php echo $fila['login']?>&idtrabajo=<?php echo $fila['IdTrabajo'] ?>"><img src="../Views/icons/How-to.png" width="14"/></a>
<?php }
			if( tienePermisoFuncAcc($_SESSION['login'], 0, 4)){ //según los permisos , funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
									<a href="../Controllers/Deliverables_Controller.php?action=DELETE&login=<?php echo $fila['login']?>&idtrabajo=<?php echo $fila['IdTrabajo'] ?>"><img src="../Views/icons/Erase.png" width="14"/></a></div></td>
<?php }
                echo "</tr>";
  
}
  }
}
?>
        </table>
</div>
<?php
       include '../Views/Footer.php'; //header necesita los string

  }
}


?>
