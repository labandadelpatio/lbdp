<!--
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017s
En este fichero se encuentra el mensaje que se le muestra al usuario cada vez que realiza una accion
-->

<?php

class MESSAGE{

	private $string; //definimos una varible string para almacenar los caracteres
	private $volver; //definimos una variable volver que tendrá la direccion a donde debe ir atras cuando salte el mensaje

	function __construct($string, $volver){
		$this->string = $string; //asignamos un valor a la variable previamente definida
		$this->volver = $volver; //asignamos un valor a la variable previamente definida
		$this->render();
	}

	function render(){

		include '../Locales/Strings_'.$_SESSION['idioma'].'.php'; //incluimos los archivos de idiomas
		include 'Header.php'; //y el archivo de cabecera
?>
		<div id="message"><strong>
<?php		
		echo $strings[$this->string];
?>
            </strong><br/><br/>
<?php

		echo '<a href=\'' . $this->volver . "'><img src='../Views/icons/Exit.png'/></a>";
		include 'Footer.php'; //incluimos la vista del Pie
	
?>
</div>
<?php
} 

}
?>