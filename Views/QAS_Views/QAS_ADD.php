
<?php
/*  Fichero para la vista de add de los qas
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class QAS_ADD {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista

    function __construct(){
        $this->Render();

    }

  //funcion Render()
  //A través de está función crearemos la vista

    function Render(){
    include '../Views/Header.php'; //header necesita los string

?>



 <h1><?php echo $strings['Add']; ?></h1>

    <div id = "formulario"> <?php //Definimos el formulario que nos permitirá añadir un QA?>
            <form method="post" enctype="multipart/form-data" name="formAdd" action='../Controllers/QAS_Controller.php?action=ADD' autocomplete="off" onsubmit="return validar_QAS(idtrabajo, loginEvaluador, loginEvaluado, aliasEvaluado)">
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id="idtrabajo" name="idtrabajo" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,6) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Login Evaluador']; ?>:</label>
        <input type="text" id='loginEvaluador' name="loginEvaluador" maxlength="9" size="9" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
        <div class="error" id="loginEvaluadorE"></div></div>
    <div>
        <label><?php echo $strings['Login Evaluado']; ?>:</label>
        <input type="text" id="loginEvaluado" name="loginEvaluado" maxlength="9" size="9" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="loginEvaluadoE"></div></div>
    <div>
        <label><?php echo $strings['Alias Evaluado']; ?>:</label>
        <input type="text" id="aliasEvaluado" name="aliasEvaluado" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="aliasEvaluadoE"></div></div>

        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <button type="reset" name="limpiar"><img src="../Views/icons/Undo.png"></button><a href="../Controllers/QAS_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>

<?php
include '../Views/Footer.php';

?>
<?php

  }
}


?>
