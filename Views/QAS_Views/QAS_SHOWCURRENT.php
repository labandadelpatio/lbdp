<?php
/*  Fichero para la vista del showcurrent de los qas
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class QAS_SHOWCURRENT {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista

	function __construct($valores){
        $this->mostrarDatos($valores);
    }

  //funcion mostrarDatos()
  //A través de está función crearemos la vista

    function mostrarDatos($valores){
        include '../Views/Header.php';

?>
<h1><?php echo $strings['current']; ?></h1>

    <div id="current"><table>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <td><?php echo $valores[0]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Login Evaluador']; ?></th>
            <td><?php echo $valores[1]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Login Evaluado']; ?></th>
            <td><?php echo $valores[2]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Alias Evaluado']; ?></th>
            <td><?php echo $valores[3]; ?></td>
        </tr>
        </table>
        <div id="imgtableshw"><a href="../Controllers/QAS_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        </div>



<?php
        include '../Views/Footer.php'; //incluimos la vista del pie
    }
}

?>