
<?php
/*  Fichero para la vista de search de los qas
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class QAS_SEARCH {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista

	
	function __construct(){
		$this->Render();

	}

      //funcion Render()
  //A través de está función crearemos la vista

	function Render(){
    include '../Views/Header.php'; //header necesita los string
?>
 
<h1><?php echo $strings['Search']; ?></h1>

<div id = "searchform"> <?php //Definimos el formulario que nos permitira buscar por todos los campos de QAS?>
<form method="post" name="formSearch" action='../Controllers/QAS_Controller.php?action=SEARCH' accept-charset="UTF-8" id="formSearch" autocomplete="off" onsubmit="" >
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id="idtrabajo" name="idtrabajo" maxlength="6" size="6" onblur="comprobarTexto(this,6);"/><div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Login Evaluador']; ?>:</label>
        <input type="text" id="loginEvaluador" name="loginEvaluador" maxlength="9" size="9" onblur="comprobarTexto(this,9);" /><div class="error" id="loginEvaluadorE"></div></div>
    <div>
        <label><?php echo $strings['Login Evaluado']; ?>:</label>
        <input type="text" id="loginEvaluado" name="loginEvaluado" maxlength="9" size="9" onblur="comprobarTexto(this,9)" /><div class="error" id="loginEvaluadoE"></div></div>
    <div>
        <label><?php echo $strings['Alias Evaluado']; ?>:</label>
        <input type="text" id="aliasEvaluado" name="aliasEvaluado" maxlength="6" size="6" onblur="comprobarTexto(this,6)" /><div class="error" id="aliasEvaluadoE"></div></div>
    
    <div>
        <div id="imgtable"><button type="submit" onclick=formSearch.submit() id='SEARCH'><img src="../Views/icons/Find.png"></button>
        <a href="../Controllers/QAS_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
</div>


<?php
include '../Views/Footer.php';

?>
<?php

  } 
}


?>
