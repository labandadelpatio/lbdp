
<?php
/*  Fichero para la vista de edit de los qas
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class QAS_EDIT {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista

    function __construct($valores){
        $this->mostrartupla($valores);
    }

    //funcion mostrartuplar()
  //A través de está función crearemos la vista
    function mostrartupla($valores){
        include '../Views/Header.php';

?>
<h1><?php echo $strings['Edit']; ?></h1>
<div id = "editar"> <?php //Definimos el formulario qe nos permitirá editar los campos de los QAS?>
<form method="post" enctype="multipart/form-data" name="formEdit" action='../Controllers/QAS_Controller.php?action=EDIT' id="formEdit" autocomplete="off" onsubmit="return validar_QAS(idtrabajo, loginEvaluador, loginEvaluado, aliasEvaluado)" >
    <div>
        <label>Id Trabajo:</label>
        <input type="text" id="idtrabajo" name="idtrabajo" maxlength="6" size="6" value="<?php echo $valores[0];?>" readonly/>
                <div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Login Evaluador']; ?>:</label>
        <input type="text" id="loginEvaluador" name="loginEvaluador" maxlength="9" size="9" value="<?php echo $valores[1];?>" readonly/>
                <div class="error" id="loginEvaluadorE"></div></div>
    <div>
        <label><?php echo $strings['Login Evaluado']; ?>:</label>
        <input type="text" id="loginEvaluado" name="loginEvaluado" maxlength="9" size="9" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" value="<?php echo $valores[2];?>" />
                <div class="error" id="loginEvaluadoE"></div></div>
    <div>
        <label><?php echo $strings['Alias Evaluado']; ?>:</label>
        <input type="text" id="aliasEvaluado" name="aliasEvaluado" maxlength="6" size="6" value="<?php echo $valores[3];?>" readonly/>
                <div class="error" id="aliasEvaluadoE"></div></div>

    <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit"><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/QAS_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
</div>


<?php
include '../Views/Footer.php';

?>
<?php

  }
}


?>
