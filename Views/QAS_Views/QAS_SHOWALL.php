<?php
/*  Fichero para la vista del showall de los qas
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class QAS_SHOWALL {

//function __construct()
//Mediante esta función declararemos el constructor de la vista


	function __construct($fila, $resultado){
        $this->mostrarDatos($fila, $resultado);
    }

      //funcion mostrarrDatos()
  //A través de está función crearemos la vista
    function mostrarDatos($fila, $resultado){
        include '../Views/Header.php';


?>
    <div id="showall">
        <div id="imgtable">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 1, 1)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/QAS_Controller.php?action=ADD"><img src="../Views/icons/Create.png"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 1, 2)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/QAS_Controller.php?action=SEARCH"><img src="../Views/icons/Find.png"/></a>
<?php }
?>
					<a href="../Controllers/QAS_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        <table>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <th><?php echo $strings['Login Evaluador']; ?></th>
            <th><?php echo $strings['Login Evaluado']; ?></th>
            <th><?php echo $strings['Alias Evaluado']; ?></th>
						<th><?php echo $strings['Ruta']; ?></th>
            <th><?php echo $strings['opciones']; ?></th>
        </tr>

<?php
if( tienePermisoFuncAcc($_SESSION['login'], 1, 0)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguienet while
        while($fila = $resultado->fetch_assoc())
        {
					if($_SESSION['login'] == $fila['LoginEvaluador'] || isAdmin($_SESSION['login'])){
            echo "<tr>";
            echo "<td>".$fila['IdTrabajo']."</td>";
            echo "<td>".$fila['LoginEvaluador']."</td>";
            echo "<td>".$fila['LoginEvaluado']."</td>";
            echo "<td>".$fila['AliasEvaluado']."</td>";
						echo "<td><a href= \"" . obtenerRuta($fila['IdTrabajo'],$fila['AliasEvaluado']) . "\"> ".obtenerRuta($fila['IdTrabajo'],$fila['AliasEvaluado'])."</td>";
?>
            <td>
                <div id="imagsall">

                    <a href="../Controllers/EvaluationQAS_Controller.php?action=SHOWALL&idtrabajo=<?php echo $fila['IdTrabajo'] ?>&loginEvaluador=<?php echo $fila['LoginEvaluador']?>&aliasEvaluado=<?php echo $fila['AliasEvaluado']?>"><img src="../Views/icons/right3green.png", width="14"/></a>

<?php
if( tienePermisoFuncAcc($_SESSION['login'], 1, 5)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
                    <a href="../Controllers/QAS_Controller.php?action=SHOWCURRENT&idtrabajo=<?php echo $fila['IdTrabajo'] ?>&loginEvaluador=<?php echo $fila['LoginEvaluador']?>&aliasEvaluado=<?php echo $fila['AliasEvaluado']?>"><img src="../Views/icons/How-to.png" width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 1, 4)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/QAS_Controller.php?action=DELETE&idtrabajo=<?php echo $fila['IdTrabajo'] ?>&loginEvaluador=<?php echo $fila['LoginEvaluador']?>&aliasEvaluado=<?php echo $fila['AliasEvaluado']?>"><img src="../Views/icons/Erase.png" width="14"/></a>

<?php
      }
?>
                </div>
            </td>
<?php

                echo "</tr>";
							}
						}
  			}

?>
        </table>
</div>


<?php
        include '../Views/Footer.php';
    }
}

?>
