<?php
/*  Fichero para la vista del showall de historias de usuario
  Autor: lbdp
  Fecha: 27/11/2017*/
class STORIES_SHOWALL {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista


	function __construct($fila, $resultado){
        $this->mostrarDatos($fila, $resultado);
    }

  //funcion mostrarDatos()
  //A través de está función crearemos la vista

    function mostrarDatos($fila, $resultado){
        include '../Views/Header.php';


?>
    <div id="showall">
        <div id="imgtable">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 9, 1)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/Stories_Controller.php?action=ADD"><img src="../Views/icons/Create.png"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 9, 2)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/Stories_Controller.php?action=SEARCH"><img src="../Views/icons/Find.png"/></a>
<?php }
?>
					<a href="../Controllers/Stories_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        <table>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <th><?php echo $strings['Nombre Trabajo']; ?></th>
            <th><?php echo $strings['ID Historia']; ?></th>
            <th><?php echo $strings['Texto Historia']; ?></th>
            <th><?php echo $strings['opciones']; ?></th>

        </tr>

<?php
if( tienePermisoFuncAcc($_SESSION['login'], 9, 0)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
        while($fila = $resultado->fetch_assoc())
        {
            echo "<tr>";
            echo "<td>".$fila["IdTrabajo"]."</td>";
            echo "<td>".moverTrabajo($fila['IdTrabajo'])."</td>";
            echo "<td>".$fila["IdHistoria"]."</td>";
            echo "<td>".$fila["TextoHistoria"]."</td>";
?>
            <td>
                <div id="imagsall">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 9, 5)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Stories_Controller.php?action=SHOWCURRENT&idtrabajo=<?php echo $fila['IdTrabajo'] ?>&idhistoria=<?php echo $fila['IdHistoria']?>"><img src="../Views/icons/How-to.png" width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 9, 3)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Stories_Controller.php?action=EDIT&idtrabajo=<?php echo $fila['IdTrabajo'] ?>&idhistoria=<?php echo $fila['IdHistoria']?>"><img src="../Views/icons/Modify.png", width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 9, 4)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Stories_Controller.php?action=DELETE&idtrabajo=<?php echo $fila['IdTrabajo'] ?>&idhistoria=<?php echo $fila['IdHistoria']?>"><img src="../Views/icons/Erase.png" width="14"/></a></div></td>
<?php
					}
                echo "</tr>";
        }
			}
?>
        </table>
</div>


<?php
        include '../Views/Footer.php'; //incluimos la vista del Pie
    }
}

?>
