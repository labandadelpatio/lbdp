
<?php
/*  Fichero para la vista de search de historias de usuario
Autor: lbdp
  Fecha: 27/11/2017*/
class STORIES_SEARCH {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista

	
	function __construct(){
		$this->Render();

	}

    //funcion Render()
  //A través de está función crearemos la vista

	function Render(){
    include '../Views/Header.php'; //header necesita los string
    ?>
			<h1><?php echo $strings['Search']; ?></h1>

<div id = "searchform">
<form method="post" name="formSearch" action='../Controllers/Stories_Controller.php?action=SEARCH' accept-charset="UTF-8" id="formSearch" autocomplete="off" onsubmit="" >
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id="idtrabajo" name="idtrabajo" maxlength="6" size="6" onblur="comprobarTexto(this,6);"/><div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['ID Historia']; ?>:</label>
        <input type="text" id="idhistoria" name="idhistoria" maxlength="2" size="2" onblur="comprobarTexto(this,2);" /><div class="error" id="idhistoriaE"></div></div>
    <div>
        <label><?php echo $strings['Texto Historia']; ?>:</label>
        <textarea maxlength="300" cols="50" rows="4" id="textohistoria" name="textohistoria" onblur="comprobarTexto(this,300)" ></textarea>
        <div class="error" id="textohistoriaE"></div></div>
    
    <div>
        <div id="imgtable"><button type="submit"  id='SEARCH'><img src="../Views/icons/Find.png"></button>
        <a href="../Controllers/Stories_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
</div>
 

<?php
include '../Views/Footer.php';

?>
<?php

  } 
}


?>
