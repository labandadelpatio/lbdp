
<?php
/*  Fichero para la vista de delete de historias de usuario
Autor: lbdp
  Fecha: 27/11/2017*/
class STORIES_DELETE{

//function __construct() 
//Mediante esta función declararemos el constructor de la vista

	function __construct($valores){
        $this->borrarDatos($valores);
    }

  //funcion borrarDatos()
  //A través de está función crearemos la vista
    function borrarDatos($valores){
        include '../Views/Header.php';

?>
<h1><?php echo $strings['delete']; ?></h1>
<div id="delete"><table>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?>:</th>
            <td><?php echo $valores[0]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['ID Historia']; ?>:</th>
            <td><?php echo $valores[1]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Texto Historia']; ?>:</th>
            <td><?php echo $valores[2]; ?></td>
        </tr>
        </table>
    <form name='x' method='post' action='../Controllers/Stories_Controller.php?action=DELETE'>
        <input type="hidden" name="idtrabajo" value="<?php echo $valores[0]; ?>"/>
        <input type="hidden" name="idhistoria" value="<?php echo $valores[1]; ?>"/>
    </form>
        <div id="imgtabledel">
            <div id="imgtable"><button type="submit" onclick=x.submit()>
            <img src="../Views/icons/Erase.png"/></button><a href="../Controllers/Stories_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        </div>



<?php
        include '../Views/Footer.php';
    }
}

?>