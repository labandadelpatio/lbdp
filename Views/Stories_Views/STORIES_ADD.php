
<?php
/*  Fichero para la vista de add de historias de usuario
Autor: lbdp
  Fecha: 27/11/2017*/
class STORIES_ADD {

    //function __construct() 
//Mediante esta función declararemos el constructor de la vista


    function __construct(){
        $this->Render();

    }

  //funcion Render()
  //A través de está función crearemos la vista

    function Render(){
    include '../Views/Header.php'; //header necesita los string
        ?>
            <h1><?php echo $strings['Add']; ?></h1>

    <div id = "formulario">
            <form method="post" enctype="multipart/form-data" name="formAdd" action='../Controllers/Stories_Controller.php?action=ADD' autocomplete="off" onsubmit="return validar_Stories(idtrabajo, idhistoria, textohistoria)">
    <div>
        <label><?php echo $strings['ID Trabajo']?></label>
        <input type="text" id="idtrabajo" name="idtrabajo" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,6) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['ID Historia']?></label>
        <input type="text" id='idhistoria' name="idhistoria" maxlength="2" size="2" onblur="comprobarVacio(this) && comprobarEntero(this,0,99) && comprobarEspaciosEnBlanco(this)" />
        <div class="error" id="idhistoriaE"></div></div>
    <div>
        <label><?php echo $strings['Texto Historia']?></label>
        <textarea maxlength="300" cols="50" rows="4" id="textohistoria" name="textohistoria" onblur="comprobarVacio(this) && comprobarTexto(this,300) && comprobarEspaciosEnBlanco(this)" ></textarea>
                <div class="error" id="textohistoriaE"></div></div>

        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <button type="reset" name="limpiar"><img src="../Views/icons/Undo.png"></button><a href="../Controllers/Stories_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>

        <?php
            include '../Views/Footer.php';
    }
}

?>
