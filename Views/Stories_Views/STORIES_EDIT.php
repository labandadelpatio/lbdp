
<?php
/*  Fichero para la vista de edit de historias de usuario
Autor: lbdp
  Fecha: 27/11/2017*/
class STORIES_EDIT {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista


    function __construct($valores){
        $this->Render($valores);

}

    //funcion Render()
  //A través de está función crearemos la vista

    function Render($valores){

    include '../Views/Header.php';

?>
    <h1><?php echo $strings['Edit']; ?></h1>
    <div id = "editar">
    <form method="post" enctype="multipart/form-data" name="formEdit" action='../Controllers/Stories_Controller.php?action=EDIT' id="formEdit" autocomplete="off" onsubmit="return validar_Stories(idtrabajo, idhistoria, textohistoria)" >
    <div>
        <label><?php echo $strings['ID Trabajo']?></label>
        <input type="text" id="idtrabajo" name="idtrabajo" maxlength="6" size="6" value="<?php echo $valores[0];?>" readonly />
                <div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['ID Historia']?></label>
        <input type="text" id="idhistoria" name="idhistoria" maxlength="2" size="2" value="<?php echo $valores[1];?>" readonly />
                <div class="error" id="idhistoriaE"></div></div>
    <div>
         <label><?php echo $strings['Texto Historia']?></label>
        <textarea maxlength="300" cols="50" rows="4" id="textohistoria" name="textohistoria" onblur="comprobarVacio(this) && comprobarTexto(this,300) && comprobarEspaciosEnBlanco(this)" ><?php echo $valores[2];?></textarea>
                <div class="error" id="textohistoriaE"></div></div>

    <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit"><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Stories_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>
<?php
        include '../Views/Footer.php';
    }
}

?>
