
<?php
/*  Fichero para la vista de search de los permisos
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class PERMISSIONS_SEARCH {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista
	
	function __construct(){
		$this->Render();

	}

     //funcion Render()
    //A través de está función crearemos la vista

	function Render(){
      include '../Views/Header.php'; //header necesita los string
		?>
			<h1><?php echo $strings['Search']; ?></h1>

    <div id = "searchform">
			<form method="post" enctype="multipart/form-data" name="formadd" action='../Controllers/Permissions_Controller.php?action=SEARCH' autocomplete="off" >
    <div>
        <label><?php echo $strings['ID Grupo']; ?>:</label>
        <input type="text" id="IdGrupo" name="IdGrupo" maxlength="6" size="6" />
        <div class="error" id="IdGrupoE"></div></div>
    <div>
        <label><?php echo $strings['ID Funcionalidad']; ?>:</label>
        <input type="text" id='IdFuncionalidad' name="IdFuncionalidad" maxlength="6" size="6"  />
        <div class="error" id="IdfuncionalidadE"></div></div>   
    <div>
        <label><?php echo $strings['ID Accion']; ?>:</label>
        <input type="text" id="IdAccion" name="IdAccion" maxlength="6" size="6"  /><div class="error" id="dniE"></div></div>
   
    
        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Permissions_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>
		
		<?php
		 include '../Views/Footer.php'; //header necesita los string

  } 
}


?>
