
<?php
/*  Fichero para la vista del showall de los permisos
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class PERMISSIONS_SHOWALL {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista


	function __construct($fila, $resultado){
		$this->Render($fila, $resultado);

	}
    //funcion Render()
    //A través de está función crearemos la vista

	function Render($fila, $resultado){
      include '../Views/Header.php'; //header necesita los string


?>
    <div id="showall">
        <div id="imgtable">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 7, 2)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/Permissions_Controller.php?action=SEARCH"><img src="../Views/icons/Find.png"/></a>
<?php }
?>
					<a href="../Controllers/Permissions_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        <table>
        <tr>
            <th><?php echo $strings['ID Grupo']; ?></th>
            <th><?php echo $strings['Nombre Grupo']; ?></th>
            <th><?php echo $strings['ID Funcionalidad']; ?></th>
            <th><?php echo $strings['Nombre Funcionalidad']; ?></th>
            <th><?php echo $strings['ID Accion']; ?></th>
            <th><?php echo $strings['Nombre Accion']; ?></th>
        </tr>

<?php
if( tienePermisoFuncAcc($_SESSION['login'], 7, 0)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
        while($fila = $resultado->fetch_array())
        {
            echo "<tr>";
            echo "<td>".$fila[0]."</td>";
            echo "<td>".$fila[1]."</td>";
            echo "<td>".$fila[2]."</td>";
            echo "<td>".$fila[3]."</td>";
            echo "<td>".$fila[4]."</td>";
            echo "<td>".$fila[5]."</td>";
            echo "</tr>";
        }
}
?>
        </table>
</div>


<?php
        include '../Views/Footer.php'; //header necesita los string
  }
}


?>
