
<?php
/*  Fichero para la vista del showcurrent de los trabajos
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class JOB_SHOWCURRENT {

    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

	function __construct($valores){
        $this->mostrarDatos($valores);
    }
    //funcion Render()
    //A través de está función crearemos la vista
    function mostrarDatos($valores){
        include '../Views/Header.php';

?>
<h1><?php echo $strings['current']; ?></h1>

    <div id="current"><table>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <td><?php echo $valores[0]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Nombre Trabajo']; ?></th>
            <td><?php echo $valores[1]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Fecha Inicio']; ?></th>
            <td><?php echo $valores[2]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Fecha Fin']; ?></th>
            <td><?php echo $valores[3]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Porcentaje Nota']; ?></th>
            <td><?php echo $valores[4]; ?></td>
        </tr>
        </table>
        <div id="imgtableshw"><a href="../Controllers/Jobs_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        </div>



<?php
        include '../Views/Footer.php';
    }
}

?>