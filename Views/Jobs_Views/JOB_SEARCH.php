
<?php
/*  Fichero para la vista de search de trabajos
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class JOB_SEARCH {

    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

	
	function __construct(){
		$this->Render();

	}

     //funcion Render()
    //A través de está función crearemos la vista

	function Render(){
    include '../Views/Header.php'; //header necesita los string
   
?>
 <h1><?php echo $strings['Search']; ?></h1>

<div id = "searchform"> <?php //Definimos el formulario que nos permite buscar cualquier campo de un trabajo?>
<form method="post" name="formSearch" action='../Controllers/Jobs_Controller.php?action=SEARCH' accept-charset="UTF-8" id="formSearch" autocomplete="off" onsubmit="" >
    <div>
        <label><?php echo $strings['ID Trabajo']; ?></label>
        <input type="text" id="idtrabajo" name="idtrabajo" maxlength="6" size="6" onblur=" comprobarTexto(this,6);" /><div class="error" id="idtrabajoE"></div></div>

    <div>
        <label><?php echo $strings['Nombre Trabajo']; ?></label> 
        <select  id="nomtrabajo" name ="nomtrabajo" onblur="comprobarVacio(this)" >
        <option value ="" selected>Seleccione Entrega</option>
        <option value ="ET">ET</option>
        <option value ="QA" >QA</option></select>
        <input type="text" id="nombtrabajo" name="nombtrabajo" size="57" maxlength="57" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,57)" />
        <div class="error" id="nomtrabajoE"></div>
        <div class="error" id="nombtrabajoE"></div></div>   

    <div>
        <label><?php echo $strings['Fecha Inicio']; ?></label>
        <input type="text" id="fecini" name="fecini" class="tcal" value="" readonly />
        <div class="error" id="feciniE"></div></div>

	<div>
        <label><?php echo $strings['Fecha Fin']; ?></label>
        <input type="text" id="fecfin" name="fecfin" class="tcal" value="" readonly />
        <div class="error" id="fecfinE"></div></div>

     <div>
        <label><?php echo $strings['Porcentaje Nota']; ?></label>
        <input type="text" id='porcentajenota' name="porcentajenota" maxlength="5" size="5" onblur=" comprobarTexto(this,2);" />
        <div class="error" id="porcentajenotaE"></div></div>
    
    <div>
        <div id="imgtable"><button type="submit" onclick=formSearch.submit() id='SEARCH'><img src="../Views/icons/Find.png"></button>
        <a href="../Controllers/Jobs_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
</div>


<?php
include '../Views/Footer.php';

?>
<?php

  } 
}


?>
