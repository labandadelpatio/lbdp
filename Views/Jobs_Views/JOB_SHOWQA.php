
<?php
/*  Fichero para la vista que muestra los trabajos que son qas
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class JOB_SHOWQA {

    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista


	function __construct($fila, $resultado){
        $this->mostrarDatos($fila, $resultado);
    }

    //funcion mostrarDatos()
    //A través de está función crearemos la vista
    function mostrarDatos($fila, $resultado){
        include '../Views/Header.php';

?>
    <div id="showall">
        <div id="imgtable">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 8, 1)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/Jobs_Controller.php?action=ADD"><img src="../Views/icons/Create.png"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 8, 2)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/Jobs_Controller.php?action=SEARCH"><img src="../Views/icons/Find.png"/></a>
<?php }
?>
					<a href="../Controllers/QAS_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        <table>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <th><?php echo $strings['Nombre Trabajo']; ?></th>
            <th><?php echo $strings['Fecha Inicio']; ?></th>
            <th><?php echo $strings['Fecha Fin']; ?></th>
            <th><?php echo $strings['Porcentaje Nota']; ?></th>
            <th><?php echo $strings['opciones']; ?></th>
        </tr>

<?php
if( tienePermisoFuncAcc($_SESSION['login'], 1, 0)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
        while($fila = $resultado->fetch_assoc()){
            echo "<tr>";
            echo "<td>".$fila["IdTrabajo"]."</td>";
            echo "<td>".$fila["NombreTrabajo"]."</td>";
            echo "<td>".$fila["FechaIniTrabajo"]."</td>";
            echo "<td>".$fila["FechaFinTrabajo"]."</td>";
            echo "<td>".$fila["PorcentajeNota"]."</td>";
?>
            <td>
                <div id="imagsall">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 1, 9)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>   
                    <a href="../Controllers/QAS_Controller.php?action=SHOWALL&idtrabajo=<?php echo $fila['IdTrabajo']?>"><img src="../Views/icons/right3green.png" width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 8, 5)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
                    <a href="../Controllers/Jobs_Controller.php?action=SHOWCURRENT&idtrabajo=<?php echo $fila['IdTrabajo'] ?>"><img src="../Views/icons/How-to.png" width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 8, 3)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Jobs_Controller.php?action=EDIT&idtrabajo=<?php echo $fila['IdTrabajo'] ?>"><img src="../Views/icons/Modify.png", width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 8, 4)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Jobs_Controller.php?action=DELETE&idtrabajo=<?php echo $fila['IdTrabajo'] ?>"><img src="../Views/icons/Erase.png" width="14"/></a></div></td>
<?php
					}
                echo "</tr>";
        }
			}
?>
      </table>
</div>


<?php
        include '../Views/Footer.php';
    }
}
?>
