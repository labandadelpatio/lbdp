
<?php
/*  Fichero para la vista de add de trabajos
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017 */
class JOB_ADD {
 
 //function __construct() 
 //Mediante esta función declararemos el constructor de la vista


    function __construct(){
        $this->Render();
    }

     //funcion Render()
    //A través de está función crearemos la vista

	
	function Render(){
    include '../Views/Header.php'; //header necesita los string
		?>
			<h1><?php echo $strings['Add']; ?></h1>

    <div id = "formulario"> <?//Definimos el formulario que nos permitirá añadir un trabajo?>
			<form method="post" enctype="multipart/form-data" name="formadd" action='../Controllers/Jobs_Controller.php?action=ADD' autocomplete="off" onsubmit="return validar_Jobs(idtrabajo, nomtrabajo, nombtrabajo, fecini, fecfin, porcentajenota)">
       
        <div>
        <label><?php echo $strings['ID Trabajo']; ?></label>
       <input type="text" id='idtrabajo' name="idtrabajo" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,6)" />
        <div class="error" id="idtrabajoE"></div></div> 


  
    <div>
        <label><?php echo $strings['Nombre Trabajo']; ?></label> 
        <select  id="nomtrabajo" name ="nomtrabajo" onblur="comprobarVacio(this)" >
        <option value ="" selected>Seleccione Entrega</option>
        <option value ="ET">ET</option>
        <option value ="QA" >QA</option></select>

        <input type="text"  id="nombtrabajo" name="nombtrabajo" maxlength="57" size="57" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,57)" />

        <div class="error" id="nomtrabajoE"></div>
        <div class="error" id="nombtrabajoE"></div>

    </div>   

    <div>
        <label><?php echo $strings['Fecha Inicio']; ?>:</label>
       <input type="text" id="fecini" name="fecini" class="tcal" readonly onblur="comprobarVacio(this)" />
        <div class="error" id="feciniE"></div></div>

	<div>
        <label><?php echo $strings['Fecha Fin']; ?>:</label>
        <input type="text" id="fecfin" name="fecfin" class="tcal" readonly onblur="comprobarVacio(this)" />
                <div class="error" id="fecfinE"></div></div>

     <div>
        <label><?php echo $strings['Porcentaje Nota']; ?></label>
        <input type="text" id='porcentajenota' name="porcentajenota" maxlength="5" size="5" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarReal(this,0,0,99)" />
        <div class="error" id="porcentajenotaE"></div></div>


        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
            <button type="reset" name="limpiar"><img src="../Views/icons/Undo.png"></button><a href="../Controllers/Jobs_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
                </div>
		

<?php
include '../Views/Footer.php';

?>
<?php

  } 
}


?>
