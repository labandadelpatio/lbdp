
<?php
/*  Fichero para la vista de delete de trabajos
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class JOB_EDIT {

  //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct($valores){
        $this->mostrartupla($valores);
    }
     //funcion mostrartupla()
    //A través de está función crearemos la vista

	function mostrartupla($valores){
        include '../Views/Header.php';

?>
<h1><?php echo $strings['Edit']; ?></h1>
<div id = "editar"> <?php //Definimos el formulario que nos permitirá editar los datos relativos a un TRABAJO?>
<form method="post" enctype="multipart/form-data" name="formEdit" action='../Controllers/Jobs_Controller.php?action=EDIT' id="formEdit" autocomplete="off" onsubmit="return validar_Jobs(idtrabajo, nomtrabajo, nombtrabajo, fecini, fecfin, porcentajenota)" >
    <div>
        <label><?php echo $strings['ID Trabajo']; ?></label>
         <input type="text" id="idtrabajo" name="idtrabajo" maxlength="6" size="6" value="<?php echo $valores[0];?>" readonly onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(campo,60)" />
                <div class="error" id="idtrabajoE"></div></div>

    <div>
        <label><?php echo $strings['Nombre Trabajo']; ?></label> 
        <select  id="nomtrabajo" name ="nomtrabajo" onblur="comprobarVacio(this)" >
        <option value ="ET" <?php if(substr($valores[1],0,2)=="ET") echo "selected"?> >ET</option>
        <option value ="QA" <?php if(substr($valores[1],0,2)=="QA") echo "selected"?> >QA</option></select>
        <input type="text" id="nombtrabajo" name="nombtrabajo" size="57" maxlength="57" value="<?php echo substr($valores[1],3);?>" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,57)" />
        <div class="error" id="nomtrabajoE"></div>
        <div class="error" id="nombtrabajoE"></div></div>

    <div>
        <label><?php echo $strings['Fecha Inicio']; ?> </label>
         <input type="text" id="fecini" name="fecini" class="tcal" value="<?php echo $valores[2];?>" readonly onblur="comprobarVacio(this)" />
                <div class="error" id="feciniE"></div></div>

	<div>
        <label><?php echo $strings['Fecha Fin']; ?>: </label>
       <input type="text" id="fecfin" name="fecfin" class="tcal" value="<?php echo $valores[3];?>"  readonly onblur="comprobarVacio(this)" />
                <div class="error" id="fecfinE"></div></div>

     <div>
        <label><?php echo $strings['Porcentaje Nota']; ?></label>
        <input type="text" id='porcentajenota' name="porcentajenota" maxlength="5" size="5" value="<?php echo $valores[4];?>" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarReal(this,0,0,99)" />
        <div class="error" id="porcentajenotaE"></div></div>


    <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit"><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Jobs_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
</div>


<?php
include '../Views/Footer.php';

?>
<?php

  }
}


?>
