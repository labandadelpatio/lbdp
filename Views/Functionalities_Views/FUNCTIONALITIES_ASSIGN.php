
<?php
/*  Fichero para la vista de asignar acciones a funcionalidades
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class FUNCTIONALITIES_ASSIGN {


    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct($valores, $lista, $resultado){
      $this->Render($valores, $lista, $resultado);

    }

     //funcion Render()
    //A través de está función crearemos la vista

    function Render($valores, $lista, $resultado){
    include '../Views/Header.php'; //header necesita los string
    include_once '../Functions/TieneAccion.php'; //incluimos la funcion TieneAccion

?>
    <h1><?php echo $strings['Asignar']; ?></h1>

   <div id = "editar">
       <div id="asignar"><p><strong style="font-size:15px;"><?php echo $valores[1]; ?>:</strong>
       <?php echo $valores[2]; ?></p></div>
        <?php echo $strings['Acciones disponibles']; ?>:<br><br>
       <!--  <form method="post" enctype="multipart/form-data" name="formEdit" action='../Controllers/Functionalities_Controller.php?action=ASSIGN' autocomplete="off">-->
<?php
            while( $lista = $resultado->fetch_assoc() ){ //mientras haya acciones las recorremos 
            if(!tieneAccion($valores[0],$lista['idaccion'])){
?>
            <a href="../Controllers/FuncAcc_Controller.php?action=ASSIGN&idfuncionalidad=<?php echo $valores[0]; ?>&idaccion=<?php echo $lista['idaccion']; ?>" id='idaccion' name='idaccion'><img  src="../Views/icons/noCheck.png" /></a>
<?php
            }
            else{
?>
            <a href="../Controllers/FuncAcc_Controller.php?action=DESASSIGN&idfuncionalidad=<?php echo $valores[0]; ?>&idaccion=<?php echo $lista['idaccion']; ?>" id='idaccion' name='idaccion'><img  src="../Views/icons/check.png" /></a>
<?php
            }
?>
            <label><strong><?php echo $lista['nombreaccion'] ?></strong></label></br>

<?php
            }
?>



        <div><div id="imgtable"><!--<button type="submit" name="submit" id="submit"><img src="../Views/icons/OK.png"></button>-->
        <a href="../Controllers/Functionalities_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form></div>
<?php
include '../Views/Footer.php';

?>
<?php

  }
}


?>
