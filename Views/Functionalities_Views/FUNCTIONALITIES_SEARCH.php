
<?php
/*  Fichero para la vista de search de funcionalidades
   Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class FUNCTIONALITIES_SEARCH {

    
    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista
	
	function __construct(){
		$this->Render();

	}

     //funcion Render()
    //A través de está función crearemos la vist

	function Render(){
    include '../Views/Header.php'; //header necesita los string

?>
    <h1><?php echo $strings['Search']; ?></h1>
    <div id = "searchform">
			<form method="post"  name="formadd" action='../Controllers/Functionalities_Controller.php?action=SEARCH' autocomplete="off">
    <div>
        <label><?php echo $strings['ID Funcionalidad']; ?>:</label>
        <input type="text" id="idfuncionalidad" name="idfuncionalidad" maxlength="6" size="6" onblur="comprobarVacio(this); comprobarTexto(this,15);" />
        <div class="error" id="idfuncionalidadE"></div></div>
    <div>
        <label><?php echo $strings['Nombre Funcionalidad']; ?>:</label>
        <input type="text" id='nombrefuncionalidad' name="nombrefuncionalidad" maxlength="60" size="60" onblur="comprobarVacio(this); comprobarTexto(this,20);" />
        <div class="error" id="nombrefuncionalidadE"></div></div>   
    <div>
        <label><?php echo $strings['Descripcion Funcionalidad']; ?>:</label>
        <textarea id="descripcion" name="descripfuncionalidad"  maxlength="100" cols="50" rows="4" onblur="" ></textarea>
        <div class="error" id="descripcionE"></div></div>


         <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Functionalities_Controller.php"><img src="../Views/icons/Exit.png"/></a></div>
        </form></div>

<?php
include '../Views/Footer.php';

?>
<?php

  } 
}


?>
