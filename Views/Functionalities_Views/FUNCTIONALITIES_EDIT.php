
<?php
/*  Fichero para la vista de edit de funcionalidades
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class FUNCTIONALITIES_EDIT {


    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct($valores){
        $this->Render($valores);

    }

     //funcion Render()
    //A través de está función crearemos la vista

    function Render($valores){
    include '../Views/Header.php'; //header necesita los string

?>
    <h1><?php echo $strings['Edit']; ?></h1>

   <div id = "editar">
            <form method="post" enctype="multipart/form-data" name="formEdit" action='../Controllers/Functionalities_Controller.php?action=EDIT' autocomplete="off" onsubmit="return validar_Functionalities(idfuncionalidad, nombrefuncionalidad, descripcion)">
    <div>
        <label><?php echo $strings['ID Funcionalidad']; ?>:</label>
        <input type="text" id="idfuncionalidad" name="idfuncionalidad" value="<?php echo $valores[0]?>" maxlength="6" size="6" readonly onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,6)" />
                <div class="error" id="idfuncionalidadE"></div></div>
    <div>
        <label><?php echo $strings['Nombre Funcionalidad']; ?>:</label>
        <input type="text" id='nombrefuncionalidad' name="nombrefuncionalidad" value="<?php echo $valores[1]?>" maxlength="60" size="60" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,60)" />
        <div class="error" id="nombrefuncionalidadE"></div></div>
    <div>
        <label><?php echo $strings['Descripcion Funcionalidad']; ?>:</label>
        <textarea id="descripcion" name="descripfuncionalidad"  maxlength="100" cols="50" rows="4" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,100)" ><?php echo $valores[2]?></textarea>
                <div class="error" id="descripcionE"></div></div>

        <div><div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Functionalities_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form></div>
<?php
include '../Views/Footer.php';

?>
<?php

  }
}


?>
