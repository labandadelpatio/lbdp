
<?php
/*  Fichero para la vista de add de las funcionalidades
   Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/ 
class FUNCTIONALITIES_ADD {


    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

  function __construct(){
    $this->Render();

  }

   //funcion Render()
    //A través de está función crearemos la vista

  function Render(){
    include '../Views/Header.php'; //header necesita los string

?>
    <h1><?php echo $strings['Add']; ?></h1>

   <div id = "formulario"> <?//Definimos el formulario que no permitirá añadir una FUNCIONALIDAD?>
      <form method="post" enctype="multipart/form-data" name="formAdd" action='../Controllers/Functionalities_Controller.php?action=ADD' autocomplete="off" onsubmit="return validar_Functionalities(idfunc, nombre, descripcion)">
    <div>
        <label><?php echo $strings['ID Funcionalidad']; ?>:</label>
        <input type="text" id="idfunc" name="idfuncionalidad" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,6)" />
        <div class="error" id="idfuncE"></div></div>
    <div>
        <label><?php echo $strings['Nombre Funcionalidad']; ?>:</label>
        <input type="text" id='nombre' name="nombrefuncionalidad" maxlength="60" size="60" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,60)" />
        <div class="error" id="nombreE"></div></div>
    <div>
        <label><?php echo $strings['Descripcion Funcionalidad']; ?>:</label>
         <textarea name="descripfuncionalidad" id="descripcion" rows="4" cols="50" maxlength="100" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarTexto(this,100)"></textarea> 
        <div class="error" id="descripcionE"></div></div>
    <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <button type="reset" name="limpiar"><img src="../Views/icons/Undo.png"></button><a href="../Controllers/Functionalities_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form></div>
<?php
include '../Views/Footer.php';

?>
<?php

  }
}


?>

