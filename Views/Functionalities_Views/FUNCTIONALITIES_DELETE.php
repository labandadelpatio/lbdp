
<?php
/*  Fichero para la vista de delete de funcionalidades
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class FUNCTIONALITIES_DELETE{
    
    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista
	
	function __construct($valores){
		$this->Render($valores);

	}

     //funcion Render()
    //A través de está función crearemos la vist

	function Render($valores){
    include '../Views/Header.php'; //header necesita los string
 
?>
 
 <h1><?php echo $strings['delete']; ?></h1>
<div id="delete"><table>
        <tr>
            <th><?php echo $strings['ID Funcionalidad']; ?></th>
            <td><?php echo $valores[0]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Nombre Funcionalidad']; ?></th>
            <td><?php echo $valores[1]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Descripcion Funcionalidad']; ?></th>
            <td><?php echo $valores[2]; ?></td>
        </tr>
       
        </table>
    <form name='x' method='post' action='../Controllers/Functionalities_Controller.php?action=DELETE'>
        <input type="hidden" name="idfuncionalidad" value="<?php echo $valores[0]; ?>"/>
    </form>
        <div id="imgtabledel">
            <div id="imgtable"><button type="submit" onclick=x.submit()>
            <img src="../Views/icons/Erase.png"/></button><a href="../Controllers/Functionalities_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        </div>


<?php
include '../Views/Footer.php';

?>
<?php

  } 
}


?>
