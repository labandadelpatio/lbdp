
<?php
/*  Fichero para la vista de search de grupos de usuarios
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class GROUPS_SEARCH {


     //function __construct() 
    //Mediante esta función declararemos el constructor de la vista


	function __construct(){
		$this->Render();

	}

    //funcion Render()
    //A través de está función crearemos la vista

	function Render(){
    include '../Views/Header.php'; //header necesita los string
    
?>


 	<h1><?php echo $strings['Search']; ?></h1>

    <div id = "searchform"> <?php //Definimos el formulario que nos permitirá buscar por cualquier campo un GRUPO?>
			<form method="post"  name="formSearch" action='../Controllers/Groups_Controller.php?action=SEARCH' autocomplete="off" onsubmit="">
    <div>
        <label><?php echo $strings['ID Grupo']; ?>:</label>
        <input type="text" id="idgrupo" name="idgrupo" maxlength="6" size="6" onblur="" />
        <div class="error" id="idgrupoE"></div></div>
    <div>
        <label><?php echo $strings['Nombre']; ?>:</label>
        <input type="text" id='nombregrupo' name="nombregrupo" maxlength="60" size="60" onblur="" />
        <div class="error" id="nombregrupoE"></div></div>
    <div>
        <label><?php echo $strings['Descripcion Grupo']; ?>:</label>
        <textarea name="descripgrupo" id="descripcion" cols="50" rows="4" maxlength="100" onblur="comprobarCampoAñadir(this, formAdd)"></textarea>
        <div class="error" id="descripcionE"></div></div>
    <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Groups_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>



<?php
include '../Views/Footer.php';

?>
<?php

  }
}


?>
