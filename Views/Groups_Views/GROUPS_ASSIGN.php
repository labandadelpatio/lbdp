
<?php
/*  Fichero para la vista de asignar permisos a grupos
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class GROUPS_ASSIGN {

  //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct($valores, $lista, $resultado){
        $this->Render($valores, $lista, $resultado);

    } 
    //funcion Render()
    //A través de está función crearemos la vista

    function Render($valores, $lista, $resultado){
    include '../Views/Header.php'; //header necesita los string
    include_once '../Functions/TienePermiso.php';
?>
    <h1><?php echo $strings['Asignar']; ?></h1>

   <div id = "editar"> 
       <div id="asignar"><p><strong style="font-size:15px;"><?php echo $valores[1]; ?>:</strong> 
       <?php echo $valores[2]; ?></p></div>
        <?php echo $strings['Funcionalidades y acciones disponibles']; ?>:<br><br>
            <?php
            while( $lista = $resultado->fetch_array() ){
            if(!tienePermiso($valores[0],$lista[0],$lista[1])){
?>
            <a href="../Controllers/Permissions_Controller.php?action=ASSIGN&idgrupo=<?php echo $valores[0]; ?>&idfuncionalidad=<?php echo $lista[0]; ?>&idaccion=<?php echo $lista[1]; ?>" id='idaccion' name='idaccion'><img  src="../Views/icons/noCheck.png" /></a>
<?php
            }
            else{
?>
            <a href="../Controllers/Permissions_Controller.php?action=DESASSIGN&idgrupo=<?php echo $valores[0]; ?>&idfuncionalidad=<?php echo $lista[0]; ?>&idaccion=<?php echo $lista[1]; ?>"" id='idaccion' name='idaccion'><img  src="../Views/icons/check.png" /></a>
<?php
            }
?>
            <label><strong><?php echo $lista[2] ?>:</strong> <?php echo $lista[3] ?></label></br>

<?php
            }
?>



        <div><div id="imgtable"><!--<button type="submit" name="submit" id="submit"><img src="../Views/icons/OK.png"></button>-->
        <a href="../Controllers/Groups_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form></div>
<?php
include '../Views/Footer.php';

?>
<?php

  }
}


?>
