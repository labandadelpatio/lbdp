<?php

/*Archivo php header, contiene la cabecera de la web, así como las opciones de cambio de idioma, desconexión y registro, visible en todas las vistas.
Autor: lbdp
  Fecha: 27/11/2017*/

//incluimos todos los ficheros a utilizar

    include_once '../Functions/TienePermisoFuncAcc.php'; 
    include_once '../Functions/Authentication.php';
    include_once '../Functions/isUser.php';
    include_once '../Functions/isAdmin.php';
    include_once '../Functions/ObtenerRuta.php';
      include_once '../Functions/moverTexto.php';
        include '../Functions/moverTrabajo.php';


	if (!isset($_SESSION['idioma'])) { //comprobamos que el idioma no es null
		$_SESSION['idioma'] = 'SPANISH'; //por defecto el idioma sera el español

		include '../Locales/Strings_' . $_SESSION['idioma'] . '.php'; //incluimos el archivo
	}
	else{
		//$_SESSION['idioma'] = 'SPANISH'; // quitar y solucionar el problema de que inicilice el idioma a galego
		include '../Locales/Strings_' . $_SESSION['idioma'] . '.php';
	}

?>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../Views/css/tcal.css">
    <link rel="stylesheet" type="text/css" href="../Views/css/codeET1estilo.css" media="screen">
    <script type="text/javascript" src="../Views/javascript/tcal.js"></script>
    <script type="text/javascript" src="../Views/javascript/md5.js"></script>
    <?php include '../Views/javascript/validar.php'; ?>
    <title>Alumnotrom</title>
</head>
<body>
<header>
	<p style="text-align:center">
		<h1>

<?php
			echo $strings['Alumnotrom'];
?>
		</h1>
	</p>

	<div id = "lenganddes">
		<form name='idiomaform' action="../Functions/CambioIdioma.php" method="post">
			<?php echo $strings['idioma']; ?>
			<select name="idioma" onChange='this.form.submit()'>
		        <option value="SPANISH" <?php if($_SESSION['idioma']=="SPANISH") echo "selected" ?>><?php echo $strings['ESPAÑOL']; ?></option>
                <option value="ENGLISH" <?php if($_SESSION['idioma']=="ENGLISH") echo "selected" ?>><?php echo $strings['INGLES']; ?></option>
                <option value="GALICIAN" <?php if($_SESSION['idioma']=="GALICIAN") echo "selected" ?>><?php echo $strings['GALLEGO']; ?></option>
			</select>
		</form>
	</div>

<?php

	if (IsAuthenticated()){
?>

<?php
		echo $strings['Bienvenido'] . ': ' . $_SESSION['login'] . '<br>';
?>
    <div id="conexion">
		<a href='../Functions/Desconectar.php'>
			<img src='../Views/icons/descon.png'>
		</a>
	</div>

<?php

	}
	else{
		echo $strings['Usuario no autenticado'];
?>
		<a href='../Controllers/Register_Controller.php'><img src='../Views/icons/user-add-128.png' width="20"></a>
<?php
	}
?>


</header>

<div id = 'main'>
<?php
	//session_start();
	if (IsAuthenticated()){ //solo se mostrara el menu si el usuario esta registrado

		/*if(Admin()){*/
		include 'admins_menuLateral.php';
	    //}

	    //else
	    //include 'users_menuLateral.php';
	}
?>
<article>
