
<?php
/*  Fichero para la vista del showall de grupos de acciones
     Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 07/11/2017*/
class ACTION_SHOWALL {

    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista


	function __construct($fila, $resultado){
		$this->Render($fila, $resultado);

	}
    //funcion Render()
    //A través de está función crearemos la vista
	function Render($fila, $resultado){
  include '../Views/Header.php'; //header necesita los string


?>
    <div id="showall">
        <div id="imgtable">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 6, 1)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/Action_Controller.php?action=ADD"><img src="../Views/icons/Create.png"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 6, 2)){ //según los permisos, funcionalidades y acciones que tenga se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/Action_Controller.php?action=SEARCH"><img src="../Views/icons/Find.png"/></a>
<?php }
?>
					<a href="../Controllers/Action_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        <table>
        <tr>
            <th><?php echo $strings['ID Accion']; ?></th>
            <th><?php echo $strings['Nombre Accion']; ?></th>
            <th><?php echo $strings['Descripcion Accion']; ?></th>
            <th><?php echo $strings['opciones']; ?></th>
        </tr>

<?php
if( tienePermisoFuncAcc($_SESSION['login'], 6, 0)){ //según los permisos, funcionalidades y acciones que tenga se le mostrará o no el siguiente icono
        while($fila = $resultado->fetch_assoc()) //mientra haya datos los recorrerá mostrandolos en un tabla
        {
            echo "<tr>";
            echo "<td>".$fila["idaccion"]."</td>";
            echo "<td>".$fila["nombreaccion"]."</td>";
            echo "<td>".$fila["descripaccion"]."</td>";
?>
            <td>
                <div id="imagsall">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 6, 5)){ //según los permisos, funcionalidades y acciones que tenga se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Action_Controller.php?action=SHOWCURRENT&idaccion=<?php echo $fila['idaccion'] ?>"><img src="../Views/icons/How-to.png" width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 6, 3)){ //según los permisos, funcionalidades y acciones que tenga se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Action_Controller.php?action=EDIT&idaccion=<?php echo $fila['idaccion'] ?>"><img src="../Views/icons/Modify.png" width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 6, 4)){ //según los permisos, funcionalidades y acciones que tenga se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Action_Controller.php?action=DELETE&idaccion=<?php echo $fila['idaccion'] ?>"><img src="../Views/icons/Erase.png" width="14"/></a></div>
						</td>
<?php
					}
                echo "</tr>";
        }
			}

?>
        </table>
</div>


<?php
          include '../Views/Footer.php'; //header necesita los string

  }
}


?>
