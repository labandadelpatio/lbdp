
<?php
/*  Fichero para la vista de edit de acciones
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 10/12/2017*/
class ACTION_EDIT {

    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct($valores){
        $this->Render($valores);

    }

        //funcion Render()
    //A través de está función crearemos la vista

    function Render($valores){

        include '../Views/Header.php'; //header necesita los string
        ?>
            <h1><?php echo $strings['Edit']; ?></h1>

    <div id = "editar">
            <form method="post"  name="formEdit" action='../Controllers/Action_Controller.php?action=EDIT' autocomplete="off" onsubmit="return validar_Action(idaccion, nombreaccion, descripcion)">
    <div>
        <label><?php echo $strings['ID Accion']; ?>:</label>
        <input type="text" id="idaccion" name="idaccion" readonly value="<?php echo $valores[0] ?>" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this, 6) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="idaccionE"></div></div>
    <div>
        <label><?php echo $strings['Nombre Accion']; ?>:</label>
        <input type="text" id='nombreaccion' name="nombreaccion" value="<?php echo $valores[1] ?>" maxlength="60" size="60" onblur="comprobarVacio(this) && comprobarTexto(this,60) && comprobarEspaciosEnBlanco(this)" />
        <div class="error" id="nombreaccionE"></div></div>
    <div>
        <label><?php echo $strings['Descripcion Accion']; ?>:</label>
        <textarea name="descripaccion" id="descripcion" maxlength="100" cols="50" rows="4" onblur="comprobarVacio(this) && comprobarTexto(this,100) && comprobarEspaciosEnBlanco(this)"><?php echo $valores[2] ?></textarea>
                <div class="error" id="descripcionE"></div></div>

        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Action_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>

        <?php
              include '../Views/Footer.php'; //header necesita los string

  }
}


?>
