
<?php
/*  Fichero para la vista de search de acciones
     Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 07/11/2017*/
class ACTION_SEARCH {

    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

	
	function __construct(){
		$this->Render();

	}
    //funcion Render()
    //A través de está función crearemos la vista

	function Render(){
       include '../Views/Header.php'; //header necesita los string
		?>
			<h1><?php echo $strings['Search']; ?></h1>

    <div id = "formulario"> 
			<form method="post"  name="formadd" action='../Controllers/Action_Controller.php?action=SEARCH' autocomplete="off" onsubmit="">
    <div>
        <label><?php echo $strings['ID Accion']; ?>:</label>
        <input type="text" id="idaccion" name="idaccion" maxlength="6" size="6"  />
        <div class="error" id="idaccionE"></div></div>
    <div>
        <label><?php echo $strings['Nombre Accion']; ?>:</label>
        <input type="text" id='nombreaccion' name="nombreaccion" maxlength="60" size="60" />
        <div class="error" id="nombreaccionE"></div></div>   
    <div>
        <label><?php echo $strings['Descripcion Accion']; ?>:</label>
        <textarea id="descripaccion" name="descripaccion" maxlength="100" cols="50" rows="4"></textarea>
        <div class="error" id="descripaccionE"></div></div>
    
        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Action_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>
		
		<?php
			  include '../Views/Footer.php'; //header necesita los string

  } 
}


?>
