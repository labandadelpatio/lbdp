
<?php
/*  Fichero para la vista del showcurrent de las evaluaciones de los qas
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class EVALUATIONQAS_SHOWCURRENT {
	
    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista
    
	function __construct($valores){
		$this->Render($valores);

	}

	function Render($valores){
    include '../Views/Header.php'; //header necesita los string

?>
<h1><?php echo $strings['current']; ?></h1>
<div id="current"><table>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <td><?php echo $valores[0]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Login Evaluador']; ?></th>
            <td><?php echo $valores[1]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Alias Evaluado']; ?></th>
            <td><?php echo $valores[2]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['ID Historia']; ?></th>
            <td><?php echo $valores[3]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Correcto Alumno']; ?></th>
            <td><?php echo $valores[4]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Comentario Alumno']; ?></th>
            <td><?php echo $valores[5]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Correcto Profesor']; ?></th>
            <td><?php echo $valores[6]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Comentario Profesor']; ?></th>
            <td><?php echo $valores[7]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Ok']; ?></th>
            <td><?php echo $valores[8]; ?></td>
        </tr>
        </table>

        <div id="imgtableshw">
           <a href="../Controllers/EvaluationQAS_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        </div>



<?php
         include '../Views/Footer.php'; //header necesita los string

  } 
}


?>
