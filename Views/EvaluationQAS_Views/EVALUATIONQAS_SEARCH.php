
<?php
/*  Fichero para la vista de search de las evaluaciones de los qas
   Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class EVALUATIONQAS_SEARCH {

	 //function __construct() 
    //Mediante esta función declararemos el constructor de la vista
	function __construct(){
		$this->Render();

	}

	function Render(){
      include '../Views/Header.php'; //header necesita los string
		?>
			<h1><?php echo $strings['Search']; ?></h1>

    <div id = "searchform"> <?php //Definimos el formulario que nos permitirá buscar por el campo que se desee sobre una EVALUATION?>
			<form method="post"  name="formadd" action='../Controllers/EvaluationQAS_Controller.php?action=SEARCH' autocomplete="off" onsubmit="">
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id="IdTrabajo" name="idtrabajo" maxlength="6" size="6" onblur="" /><div class="error" id="IdTrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Login Evaluador']; ?>:</label>
        <input type="text" id='LoginEvaluador' name="loginEvaluador" maxlength="9" size="9" onblur="" />
        <div class="error" id="LoginEvaluadoE"></div></div>   
    <div>
        <label><?php echo $strings['Alias Evaluado']; ?>:</label>
        <input type="text" id="AliasEvaluado" name="aliasEvaluado" maxlength="6" size="6" onblur="" />
        <div class="error" id="aliasEvaluadoE"></div></div>
    <div>
        <label><?php echo $strings['ID Historia']; ?>:</label>
        <input type="text" id="IdHistoria" name="idhistoria" size="2" maxlength="2" onblur="" />
        <div class="error" id="nameE"></div></div>
    <div>
        <label><?php echo $strings['Correcto Alumno']; ?>:</label>
        <input type="text" id="CorrectoA" name="correctoA" maxlength="1" size="1" onblur="" />
        <div class="error" id="CorrectoAE"></div></div>
    <div>
        <label><?php echo $strings['Comentario Alumno']; ?>:</label>
        <textarea id="ComenIncorrectoA" name="ComenIncorrectoA" maxlength="300" cols="40" rows="5" onblur="" ></textarea>
        <div class="error" id="ComenIncorrectoAE"></div></div>
    <div>
        <label><?php echo $strings['Correcto Profesor']; ?>:</label>
        <input type="text" id="CorrectoP" name="correctoP" maxlength="1" size="1" onblur="" /
               ><div class="error" id="CorrectoPE"></div></div>
     <div>
        <label><?php echo $strings['Comentario Profesor']; ?>:</label>
         <textarea id="ComentIncorrectoP" name="comentIncorrectoP" maxlength="300" cols="40" rows="5" onblur="" ></textarea>
         <div class="error" id="comentIncorrectoPE"></div></div>
     <div>
        <label><?php echo $strings['Ok']; ?>:</label>
        <input type="text" id="ok" name="ok" maxlength="1" size="1" onblur="" />
         <div class="error" id="okE"></div></div>

    
        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/EvaluationQAS_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>
		
		<?php
			  include '../Views/Footer.php'; //header necesita los string

  } 
}


?>
