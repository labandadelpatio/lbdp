
<?php
/*  Fichero para la vista del showall de las evaluaciones de los qas
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class EVALUATIONQAS_SHOWALL {

    function __construct($fila, $resultado){
        $this->Render($fila, $resultado);

    }

    function Render($fila, $resultado){
      include '../Views/Header.php'; //header necesita los string


?>
    <div id="showall">
        <div id="imgtable">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 10, 1)){
?>
                    <a href="../Controllers/EvaluationQAS_Controller.php?action=ADD"><img src="../Views/icons/Create.png"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 10, 2)){
?>
                    <a href="../Controllers/EvaluationQAS_Controller.php?action=SEARCH"><img src="../Views/icons/Find.png"/></a>
<?php }
?>
                    <a href="../Controllers/QAS_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        <table>
        <tr>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <th><?php echo $strings['Nombre Trabajo']?></th>
            <th><?php echo $strings['Login Evaluador']; ?></th>
            <th><?php echo $strings['Alias Evaluado']; ?></th>
            <th><?php echo $strings['ID Historia']; ?></th>
            <th><?php echo $strings['Comentario Alumno']?></th>
            <th><?php echo $strings['opciones']; ?></th>
        </tr>

<?php
//if( tienePermisoFuncAcc($_SESSION['login'], 10, 0)){
        while($fila = $resultado->fetch_assoc() )
        {

            if($_SESSION['login']){
            echo "<tr>";
            echo "<td>".$fila['idtrabajo']."</td>";
            echo "<td>".moverTrabajo($fila['idtrabajo'])."</td>";
            echo "<td>".$fila['loginEvaluador']."</td>";
            echo "<td>".$fila['aliasEvaluado']."</td>";
            echo "<td>".$fila['idhistoria']."</td>";
            echo "<td>".$fila['comenIncorrectoA']."</td>";
?>
            <td>



<?php
if( tienePermisoFuncAcc($_SESSION['login'], 10, 5)){

?>
                     <a href="../Controllers/EvaluationQAS_Controller.php?action=SHOWCURRENT&idtrabajo=<?php echo $fila['idtrabajo']?>&loginEvaluador=<?php echo $fila['loginEvaluador']?>&aliasEvaluado=<?php echo $fila['aliasEvaluado']?>&idhistoria=<?php echo $fila['idhistoria'] ?>"><img src="../Views/icons/How-to.png", width="14"/></a>
                    
<?php }

?>
                    <a href="../Controllers/EvaluationQAS_Controller.php?action=EDIT&idtrabajo=<?php echo $fila['idtrabajo']?>&loginEvaluador=<?php echo $fila['loginEvaluador']?>&aliasEvaluado=<?php echo $fila['aliasEvaluado']?>&idhistoria=<?php echo $fila['idhistoria'] ?>"><img src="../Views/icons/Modify.png", width="14"/></a>
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 10, 4)){
?>
                   <a href="../Controllers/EvaluationQAS_Controller.php?action=DELETE&idtrabajo=<?php echo $fila['idtrabajo']?>&loginEvaluador=<?php echo $fila['loginEvaluador']?>&aliasEvaluado=<?php echo $fila['aliasEvaluado']?>&idhistoria=<?php echo $fila['idhistoria'] ?>"><img src="../Views/icons/Erase.png", width="14"/></a>
<?php }
?>
                                    </div>
                            </td>

<?php
                echo "</tr>";
            }
        }
            //}

?>
        </table>
</div>

<?php
         include '../Views/Footer.php'; //header necesita los string
  }
}


?>
