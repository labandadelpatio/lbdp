
<?php
/*  Fichero para la vista de edit de las evaluaciones de los qas
   Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class EVALUATIONQAS_EDIT {

    function __construct($valores){
        $this->Render($valores);

    }

    function Render($valores){
      include '../Views/Header.php'; //header necesita los string
        ?>
            <h1><?php echo $strings['Edit']; ?></h1>

    <div id = "editar">
            <form method="post"  name="formEdit" action='../Controllers/EvaluationQAS_Controller.php?action=EDIT' autocomplete="off" onsubmit="return validar_EvaluationQAS(idtrabajo, loginEvaluador, aliasEvaluado, idhistoria, correctoA, comenIncorrectoA, correctoP, comenIncorrectoP, ok)">
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id="idtrabajo" name="idtrabajo" readonly value="<?php echo $valores['IdTrabajo']?>" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,6) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Login Evaluador']; ?>:</label>
        <input type="text" id='loginEvaluador' name="loginEvaluador" readonly value="<?php echo $valores['LoginEvaluador']?>" maxlength="9" size="9" onblur=" comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
        <div class="error" id="loginEvaluadorE"></div></div>
    <div>
        <label><?php echo $strings['Alias Evaluado']; ?>:</label>
        <input type="text" id="aliasEvaluado" name="aliasEvaluado" readonly value="<?php echo $valores['AliasEvaluado']?>" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="aliasEvaluadoE"></div></div>
    <div>
        <label><?php echo $strings['ID Historia']; ?>:</label>
        <input type="text" id="idhistoria" name="idhistoria" readonly value="<?php echo $valores['IdHistoria']?>" size="2" maxlength="2" onblur="comprobarVacio(this) && comprobarEntero(this,0,99) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="idhistoriaE"></div></div>
    <div>
          <label><?php echo $strings['Correcto Alumno']; ?></label>
        <select  id="correctoA" name ="correctoA" onblur="comprobarVacio(this)" >
        <option value ="0" <?php if($valores['CorrectoA']==0) echo 'selected' ?> >0</option>
        <option value ="1" <?php if($valores['CorrectoA']==1) echo 'selected' ?> >1</option></select>
        <div class="error" id="correctoAE"></div></div>

    <div>
        <label><?php echo $strings['Comentario Alumno']; ?>:</label>
        <textarea name="comenIncorrectoA" id="comenIncorrectoA"  onblur="comprobarVacio(this) && comprobarTexto(this,300)"
         rows="4" cols="50" maxlength="300"><?php echo $valores[5]?></textarea>
                <div class="error" id="comenIncorrectoAE"></div></div>

<?php
if(tienePermisoFuncAcc($_SESSION['login'], 10, 0)){
?>


    <div>
         <label><?php echo $strings['Correcto Profesor']; ?></label>
        <select  id="correctoP" name ="correctoP" onblur="comprobarVacio(this)" >
        <option value ="0" <?php if($valores['CorrectoP']==0) echo 'selected' ?> >0</option>
        <option value ="1" <?php if($valores['CorrectoP']==1) echo 'selected' ?> >1</option></select>
        <div class="error" id="correctoPE"></div></div>

     <div>
        <label><?php echo $strings['Comentario Profesor']; ?>:</label>
        <textarea name="comenIncorrectoP" id="comenIncorrectoP"  onblur="comprobarVacio(this) && comprobarTexto(this,300)"
         rows="4" cols="50" maxlength="300"><?php echo $valores[7]?></textarea>
                <div class="error" id="comenIncorrectoPE"></div></div>

     <div>
         <label><?php echo $strings['Ok']; ?></label>
        <select  id="ok" name ="ok" onblur="comprobarVacio(this)" >
        <option value ="0" <?php if($valores['OK']==0) echo 'selected' ?> >0</option>
        <option value ="1" <?php if($valores['OK']==1) echo 'selected' ?> >1</option></select>
        <div class="error" id="okE"></div></div>

      <div>


<?php
}
?>


        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/QAS_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>

        <?php
              include '../Views/Footer.php'; //header necesita los string

  }
}


?>
