
<?php
/*  Fichero para la vista de add de las evaluaciones de los qas
    Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class EVALUATIONQAS_ADD {

    //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct(){
        $this->Render();

    }

    function Render(){
      include '../Views/Header.php'; //header necesita los string
        ?>
            <h1><?php echo $strings['Add']; ?></h1>

    <div id = "formulario"> <?php //Definimos el formulario para añadir ua EVALUACIÓN con todos sus campos?>
            <form method="post"  name="formAdd" action='../Controllers/EvaluationQAS_Controller.php?action=ADD' autocomplete="off" onsubmit="return validar_EvaluationQAS(idtrabajo, loginEvaluador, aliasEvaluado, idhistoria, correctoA, comenIncorrectoA, correctoP, comenIncorrectoP, ok)">
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id="idtrabajo" name="idtrabajo" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,6) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Login Evaluador']; ?>:</label>
        <input type="text" id='loginEvaluador' name="loginEvaluador" maxlength="9" size="9" onblur=" comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
        <div class="error" id="loginEvaluadorE"></div></div>
    <div>
        <label><?php echo $strings['Alias Evaluado']; ?>:</label>
        <input type="text" id="aliasEvaluado" name="aliasEvaluado" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="aliasEvaluadoE"></div></div>
    <div>
        <label><?php echo $strings['ID Historia']; ?>:</label>
        <input type="text" id="idhistoria" name="idhistoria" size="2" maxlength="2" onblur="comprobarVacio(this) && comprobarEntero(this,0,99) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="idhistoriaE"></div></div>
    <div>
        <label><?php echo $strings['Correcto Alumno']; ?>:</label>
        <input type="text" id="correctoA" name="correctoA" maxlength="1" size="1" onblur="comprobarEntero(this,0,1)" />
                <div class="error" id="correctoAE"></div></div>
    <div>
        <label><?php echo $strings['Comentario Alumno']; ?>:</label>
         <textarea name="comenIncorrectoA" id="comenIncorrectoA"  onblur="comprobarTexto(this,300)" placeholder="<?php echo   $strings['Escribe aqui tus comentarios'];?>" size="300"   rows="5" cols="40" maxlength="300"  
         ></textarea> 
                <div class="error" id="comenIncorrectoAE"></div></div>
    <div>
        <label><?php echo $strings['Correcto Profesor']; ?>:</label>
        <input type="text" id="correctoP" name="correctoP" maxlength="1" size="1" onblur="comprobarEntero(this,0,1)" />
                <div class="error" id="correctoPE"></div></div>
     <div>
        <label><?php echo $strings['Comentario Profesor']; ?>:</label>
        <textarea name="comentIncorrectoP" id="comenIncorrectoP"  onblur="comprobarTexto(this,300)"  placeholder="<?php echo  $strings['Escribe aqui tus comentarios'];?>"  size="300"  
         rows="5" cols="40" maxlength="300"  ></textarea> 
                <div class="error" id="comenIncorrectoPE"></div></div>
     <div>
        <label><?php echo $strings['Ok']; ?>:</label>
        <input type="text" id="ok" name="ok" maxlength="1" size="1" onblur="comprobarVacio(this)" />
                <div class="error" id="okE"></div></div>


        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <button type="reset" name="limpiar"><img src="../Views/icons/Undo.png"></button><a href="../Controllers/EvaluationQAS_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>

        <?php
              include '../Views/Footer.php'; //header necesita los string
  }
}


?>
