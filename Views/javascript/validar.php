<script type="text/javascript">
/* Archivo javascript, contiene las funciones para validar cada campo de los formularios.
Autor: lbdp
  Fecha: 27/11/2017*/


//Comprueba que los datos introducidos en cada campo no sobrepasen el tamaño máximo de los mismos
  function comprobarTexto(campo,size) { //param: campo: campo en el que se aplica la funcion, size: tamaño máximo permitido
    var error = document.getElementById(campo.id + "E");
    if (campo.value.length>size) { //en caso de que el dato tenga un tamaño mayor del admitido, salta la alerta
        error.innerHTML = '<?php echo $strings['longitud']; ?>'+ size;
        campo.style.borderColor = 'red';
            return false;
    }
    else{
        campo.style.borderColor = 'grey';
        error.innerHTML = '';
        return true;
    }
}

//Comprueba que el campo no esté vacío
    function comprobarVacio(campo){ //param: campo: campo en el que se aplica la funcion
      if ((campo.value == null) || (campo.value.length == 0)){ //en caso de que el campo sea nulo (no se haya llegado a activar) o que no se haya escrito nada en él, salta la alerta
        var error = document.getElementById(campo.id + "E");
        error.innerHTML = '<?php echo $strings['vacío']; ?>';
        campo.style.borderColor = 'red';
            return false;
        }
        else{
        var error = document.getElementById(campo.id + "E");
        campo.style.borderColor = 'grey';
        error.innerHTML = '';
            return true;
        }
    }


//Comprueba que en el campo sólo haya letras
    function comprobarAlfabetico(campo, size){ //param: campo: campo en el que se aplica la funcion, size: tamaño máximo permitido
        if(comprobarTexto(campo,size)){ //comprueba que no sobrepase el máximo de tamaño
            var texto = /^[a-zñáéíóú ]*$/i; //expresión regular con todo el alfabeto, las vocales acentuadas y espacio
            if(!texto.test(campo.value)){ //en caso de que haya caracteres no admitidos, salta la alerta
                var error = document.getElementById(campo.id + "E");
                error.innerHTML = '<?php echo $strings['formato']; ?>';
                campo.style.borderColor = 'red';
                    return false;
            }
            else{
                var error = document.getElementById(campo.id + "E");
              campo.style.borderColor = 'grey';
              error.innerHTML = '';
              return true;
            }
        }
}


//Comprueba que un DNI introducido tenga el formato correcto y sea válido
    function comprobarDni(campo){ //param: campo: campo en el que se aplica la funcion
        var valor = campo.value; //almacena el valor introducido
        var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T']; //almacena todas las letras posibles de DNI
        var num = /^\d{8}[A-Z]$/; //almacena el formato de DNI, 8 numeros y una letra
        var error = document.getElementById(campo.id + "E");
            if( !(num.test(valor))){ //comprueba el formato 
                error.innerHTML = '<?php echo $strings['formato']; ?>';
                campo.style.borderColor = 'red';
                return false;
            }
            if(valor.charAt(8) != letras[(valor.substring(0, 8))%23]) { //comprueba que la letra sea la correspondiente al número introducido
                error.innerHTML = '<?php echo $strings['formato']; ?>';
                campo.style.borderColor = 'red';
                return false;
                }
        else{
          campo.style.borderColor = 'grey';
          error.innerHTML = '';
          return true;
        }

     }



//Comprueba que un número de telefono tenga un formato válido
    function comprobarTelf(campo){ //param: campo: campo en el que se aplica la funcion
        var valor = campo.value; //almacena el dato introducido
        var exptel= /^(34)?([0-9]){9}$/ //almacena el formato del telefono, tanto nacional como internacional
        var error = document.getElementById(campo.id + "E");
        if( !(exptel.test(valor))){ //en caso de que el número introducido no cumpla el formato, salta la alarma
                error.innerHTML = '<?php echo $strings['formato']; ?>';
                campo.style.borderColor = 'red';
                return false;
        }
        else{
          campo.style.borderColor = 'grey';
          error.innerHTML = '';
          return true;
        }
}

//Comprueba que un email introducido tenga el formato correcto
    function comprobarEmail(campo,size){ //param: campo: campo en el que se aplica la funcion, size: tamaño máximo permitido
        if(comprobarTexto(campo,size)){ //comprueba que no sobrepase el tamaño introducido
            var error = document.getElementById(campo.id + "E");
            var caract = /^[\w.%+]{1,25}@(?:[A-Z0-9-]{1,15}\.){1,3}[A-Z]{2,4}$/i; //alamacena la estructura completa posible de un email
            var valor = campo.value; //almacena el dato introducido
            if (!caract.test(valor)){ //en caso de que el email no cumpla el formato, salta la alarma
                error.innerHTML = '<?php echo $strings['formato']; ?>';
                campo.style.borderColor = 'red';
                return false;
        }
        else{
          campo.style.borderColor = 'grey';
          error.innerHTML = '';
          return true;
        }
    }
}

/**--------------------------------------------------------------------
* Comprueba que el numero dentro del campo no supere el máximo ni que esté
*   por debajo del mínimo
* @param {string} campo es el campo al que se hace referencia dentro de un formulario(elemento que se desea comprobar)
* @param {int} valormenor valor mínimo que puede ser introducido en el campo
* @param {int} valormayor valor máximo que puede ser introducido en el campo
*/
function comprobarEntero(campo, valormenor, valormayor){
    var expr = /^[0-9]+$/i; // Almacena expr regular que define solo números.
    var error = document.getElementById(campo.id + "E"); // Almacena el nombre id del div error en cuestión, loginE, ApellidosE...

    if(!expr.test(campo.value)){ // Comprueba si el valor es un numero entero
        campo.style.borderColor = 'red';
        error.innerHTML = '<?php echo $strings['numeros']; ?>';
    return false;
    }
    else if(campo.value < valormenor){ // Comprueba si el valor del campo pasado como parametro(campo) es menor que la variable (valormenor), si se cumple entra en el if
        campo.style.borderColor = 'red';
        error.innerHTML = '<?php echo $strings['minimo']; ?>: '+ valormenor;
    return false;
    }else if(campo.value > valormayor){ // Comprueba si el valor del campo pasado como parametro(campo) es mayor que la variable (valormayor), si se cumple entra en el if
        campo.style.borderColor = 'red';
        error.innerHTML = '<?php echo $strings['maximo']; ?>: '+ valormayor;
    return false;
    }else{ // Si cumple con la expresión regular y está entre el número menor y mayor predefinido
        campo.style.borderColor = 'grey';
        error.innerHTML = "";
    return true;
    }
}


/**--------------------------------------------------------------------
* Recorta un numero que le llegue como valor del campo al número de decimales que le pasemos a la funcion, tambien comprueba que este entre un minimo
*   y un máximo establecido por parametro
* @param {string} campo es el campo al que se hace referencia dentro de un formulario(elemento que se desea comprobar)
* @param {int} numero_decimales número de decimales máximo que puede tener el número del campo
* @param {int} valormenor valor mínimo que puede ser introducido en el campo
* @param {valormayor} valormayor valor máximo que puede ser introducido en el campo
*/

function comprobarReal(campo, numero_decimales, valormenor, valormayor){
    var error = document.getElementById(campo.id + "E"); // Almacena el nombre id del div error en cuestión, loginE, ApellidosE...
    var match = (''+campo.value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/); // Almacena el numero que obtenemos del campo con solo la parte decimal.
    var dec = Math.max(0,(match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0)); // Almacena el numero de decimales que tiene el numero del campo al que hacemos referencia
    var expr = /^[0-9.-]+$/i; // Almacena la expresion regular que serían los números y el punto y el símbolo menos

    if(!expr.test(campo.value)){ // Comprueba si el valor es un numero entero
        campo.style.borderColor = 'red';
    error.innerHTML = '<?php echo $strings['numeros decimales']; ?>';
    return false;
    }
    else if(campo.value < valormenor){ // Comprueba si el valor del campo pasado como parametro(campo) es menor que la variable (valormenor), si se cumple entra en el if
        campo.style.borderColor = 'red';
        error.innerHTML = '<?php echo $strings['minimo']; ?>: '+ valormenor;
    return false;
    }
    else if(campo.value > valormayor){ // Comprueba si el valor del campo pasado como parametro(campo) es mayor que la variable (valormayor), si se cumple entra en el if
        campo.style.borderColor = 'red';
        error.innerHTML = '<?php echo $strings['maximo']; ?>: '+ valormayor;
    return false;
    }
    else if(dec > numero_decimales){ // Comprueba si el numero de decimales supera al pasado por parametro, si se cumple entra en el if
        campo.style.borderColor = 'red';
        error.innerHTML = '<?php echo $strings['decimales']; ?>: '+ numero_decimales;
    return false;
    }
    else{ // Si cumple con la expresión regular, está entre el valor menor y mayor predefinido y tiene menos del número de decimales máximo
        campo.style.borderColor = 'grey';
        error.innerHTML = "";
    return true;
    }
}


/** Comprueba si el campo del formulario sobre el que se aplique sólo tiene espacios en blanco
* @param {string} campo es el campo al que se hace referencia dentro de un formulario(elemento que se desea comprobar)
*/
  function comprobarEspaciosEnBlanco(campo){
    var error = document.getElementById(campo.id + 'E'); // Almacena el nombre id del div error en cuestión, loginE, ApellidosE...
    if(campo.value.trim() == ""){ // Recorta los espacios en blanco laterales del valor del campo, si solo hay espacios en blanco entra en el if
      campo.style.borderColor = 'red';
      error.innerHTML = '<?php echo $strings['blanco']; ?>';
      return false;
    }
    else{ // Si no hay solo espacios en blanco en un campo
      campo.style.borderColor = 'grey';
      error.innerHTML = "";
      return true;
    }
}
    //Encripta mediante md5 el campo pasado por parámetro
     function encriptar(){
          document.getElementById('password').value = hex_md5(document.getElementById('password').value); //cambia el valor del campo a su encriptación mediante md5
          return true;
        }

    //Encripta mediante md5 el campo pasado por parámetro
    function encriptar(campo){ //param: password: campo al que se le aplicará la encriptación
          campo.value = hex_md5(campo.value); // cambia el valor del campo a su encriptación mediante md5
          return true;
        }

      /**--------------------------------------------------------------------
      * Aplica una u otras funciones según el tipo de campo que se pase por parámetro
      * @param campo el formulario al que se hace referencia(insertar, buscar o editar)
      * @param formulario el formulario al que se hace referencia(insertar, buscar o editar)
      */
      function comprobarCampoAñadir(campo,formulario){
            /*Este switch utiliza el nombre de los inputs del formulario y según el input que sea entra en el case correspondiente
                para aplicar las comprobaciones necesarias llamando a las funciones correctas*/
            switch (campo.id){
                case 'login': // Login
                if(formulario.name != 'formSearch')
                        comprobarVacio(campo) && comprobarTexto(campo,9) && comprobarEspaciosEnBlanco(campo);
                      break;
                case 'password': // Password
                if(formulario.name != 'formSearch')
                        comprobarVacio(campo) && comprobarTexto(campo,20) && comprobarEspaciosEnBlanco(campo);
                      break;
                case 'nombre': // Nombre
                if(formulario.name != 'formSearch')
                        comprobarVacio(campo) && comprobarEspaciosEnBlanco(campo) && comprobarAlfabetico(campo, 30) ;
                      break;
                case 'apellidos': // Apellidos
                if(formulario.name != 'formSearch')
                        comprobarVacio(campo) && comprobarEspaciosEnBlanco(campo) && comprobarAlfabetico(campo, 50);
                      break;
                case 'direccion': // direccion
                if(formulario.name != 'formSearch')
                        comprobarVacio(campo) && comprobarTexto(campo,60) && comprobarEspaciosEnBlanco(campo);
                      break;
                case 'correo': // correo
                if(formulario.name != 'formSearch')
                        comprobarVacio(campo) && comprobarEmail(campo) && comprobarTexto(campo,40);
                      break;
                case 'dni': // DNI
                if(formulario.name != 'formSearch')
                        comprobarVacio(campo) && comprobarDni(campo);
                      break;
                case 'telefono': // Telefono
                if(formulario.name != 'formSearch')
                        comprobarVacio(campo) && comprobarTelf(campo);
                      break;
            case 'idtrabajo':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,6) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'loginEvaluador':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,9) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'loginEvaluado':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,9) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'aliasEvaluado':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,9) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'alias':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,9) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'horas':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarEntero(campo, 0, 99);
                break;
            case 'ruta':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo);
                break;
            case 'idgrupo':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo, 6);
                break;
            case 'idfuncionalidad':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo, 6) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'nombrefuncionalidad':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,60) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'descripcion':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,100) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'idaccion':
                if(formulario.name != 'formSearch')
                  comprobarVacio(campo) && comprobarTexto(campo, 6) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'nombreaccion':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,60) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'idhistoria':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarEntero(campo,0,99) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'textohistoria':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,300) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'correctoA':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo);
                break;
            case 'comenIncorrectoA':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,300);
                break;
            case 'correctoP':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo);
                break;
            case 'comenIncorrectoP':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo,300);
                break;
            case 'ok':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo);
                break;
            case 'notaTrabajo':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarReal(campo, 2, 0, 10);
                break;
            case 'nomtrabajo':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarTexto(campo, 60) && comprobarEspaciosEnBlanco(campo);
                break;
            case 'porcentajenota':
                if(formulario.name != 'formSearch')
                comprobarVacio(campo) && comprobarEspaciosEnBlanco(campo) && comprobarReal(campo, 0, 0, 99);
                break;
            case 'fecini':
                if(formulario.name != 'formSearch')
                comprobarVacio();
                break;
            case 'fecfin':
                if(formulario.name != 'formSearch')
                comprobarVacio();
                break;
            }
      }



function validar_User(login,password,dni,nombre,apellidos,correo, direccion, telefono){

    if(comprobarVacio(login)==false || comprobarEspaciosEnBlanco(login)==false || comprobarTexto(login,9)==false){
        return false;
    }
    else if(comprobarVacio(password)==false || comprobarTexto(password,20)==false){
        return false;
    }
    else if(comprobarVacio(dni)==false || !comprobarDni(dni)){
        return false;
    }
    else if(comprobarVacio(nombre)==false || comprobarEspaciosEnBlanco(nombre)==false || comprobarTexto(nombre,30)==false){
        return false;
    }
    else if(comprobarVacio(apellidos)==false || comprobarEspaciosEnBlanco(apellidos)==false || comprobarTexto(apellidos,50)==false){
        return false;
    }
    else if(comprobarVacio(correo)==false || comprobarEspaciosEnBlanco(correo)==false || comprobarEmail(correo,40)==false){
        return false;
    }
    else if(comprobarVacio(direccion)==false || comprobarEspaciosEnBlanco(direccion)==false || comprobarTexto(direccion,60)==false){
        return false;
    }
    else if(comprobarVacio(telefono)==false || comprobarTelf(telefono)==false){
        return false;
    }
    else{
        return true;
    }
}

function validar_User_Login(login,password){
    if(comprobarVacio(login)==false || comprobarEspaciosEnBlanco(login)==false || comprobarTexto(login,9)==false){
        return false;
    }
    else if(comprobarVacio(password)==false || comprobarTexto(password,20)==false){
        return false;
    }
    else{
        return true;
    }

}

function validar_Stories(idtrabajo, idhistoria, textohistoria){
    if(comprobarVacio(idtrabajo)==false || comprobarEspaciosEnBlanco(idtrabajo)==false || comprobarTexto(idtrabajo,6)==false){
        return false;
    }
    else if(comprobarVacio(idhistoria)==false || comprobarEspaciosEnBlanco(idhistoria)==false || comprobarEntero(idhistoria,0,99)==false){
        return false;
    }
    else if(comprobarVacio(textohistoria)==false || comprobarEspaciosEnBlanco(textohistoria)==false || comprobarTexto(textohistoria,300)==false){
        return false;
    }
    else{
        return true;
    }
}

function validar_QAS(idtrabajo, loginEvaluador, loginEvaluado, aliasEvaluado){
    if(comprobarVacio(idtrabajo)==false || comprobarTexto(idtrabajo,6)==false || comprobarEspaciosEnBlanco(idtrabajo)==false){
        return false;
    }
    else if(comprobarVacio(loginEvaluador)==false || comprobarTexto(loginEvaluador,9)==false || comprobarEspaciosEnBlanco(loginEvaluador)==false){
        return false;
    }
    else if(comprobarVacio(loginEvaluado)==false || comprobarTexto(loginEvaluado,9)==false || comprobarEspaciosEnBlanco(loginEvaluado)==false){
        return false;
    }
    else if(comprobarVacio(aliasEvaluado)==false || comprobarTexto(aliasEvaluado,9)==false || comprobarEspaciosEnBlanco(aliasEvaluado)==false){
        return false;
    }
    else{
        return true;
    }

}

function validar_Marks(login, idtrabajo, notaTrabajo){
   if(comprobarVacio(login)==false || comprobarEspaciosEnBlanco(login)==false || comprobarTexto(login,9)==false){
        return false;
    }
    else if(comprobarVacio(idtrabajo)==false || comprobarTexto(idtrabajo,6)==false || comprobarEspaciosEnBlanco(idtrabajo)==false){
        return false;
    }
    else if(comprobarVacio(notaTrabajo)==false || comprobarReal(notaTrabajo, 2, 0, 10)==false){
        return false;
    }
    else{
        return true;
    }
}


function validar_Jobs(idtrabajo, nombreTrabajo, nombTrabajo, fecini, fecfin, porcentajenota){
    if(comprobarVacio(idtrabajo)==false || comprobarTexto(idtrabajo,6)==false || comprobarEspaciosEnBlanco(idtrabajo)==false){
        return false;
    }
    else if(comprobarVacio(nombreTrabajo)==false){
        return false;
    }
    else if(comprobarVacio(nombTrabajo)==false || comprobarTexto(nombTrabajo,60)==false || comprobarEspaciosEnBlanco(nombTrabajo)==false){
        return false;
    }
    else if(comprobarVacio(fecini)==false){
        return false;
    }
    else if(comprobarVacio(fecfin)==false){
        return false;
    }
    else if(comprobarVacio(porcentajenota)==false || comprobarEspaciosEnBlanco(porcentajenota)==false || comprobarReal(porcentajenota, 0, 0, 99)==false){
        return false;
    }
    else{
        return true;
    }
    
}

function validar_Groups(idgrupo, nombregrupo, descripcion){
    if(comprobarVacio(idgrupo)==false || comprobarTexto(idgrupo, 6)==false){
        return false;
    }
    else if(comprobarVacio(nombregrupo)==false || comprobarEspaciosEnBlanco(nombregrupo)==false || comprobarTexto(nombregrupo,60)==false){
        return false;
    }
    else if(comprobarVacio(descripcion)==false || comprobarEspaciosEnBlanco(descripcion)==false || comprobarTexto(descripcion,100)==false){
        return false;
    }
    else{
        return true;
    }
}

function validar_Functionalities(idfuncionalidad, nombrefuncionalidad, descripcion){
    if(comprobarVacio(idfuncionalidad)==false || comprobarTexto(idfuncionalidad, 6)==false || comprobarEspaciosEnBlanco(idfuncionalidad)==false){
        return false;
    }
    else if(comprobarVacio(nombrefuncionalidad)==false || comprobarTexto(nombrefuncionalidad,60)==false || comprobarEspaciosEnBlanco(nombrefuncionalidad)==false){
        return false;
    }
   else if(comprobarVacio(descripcion)==false || comprobarEspaciosEnBlanco(descripcion)==false || comprobarTexto(descripcion,100)==false){
        return false;
    }
    else{
        return true;
    }
}

function validar_EvaluationQAS(idtrabajo, loginEvaluador, aliasEvaluado, idhistoria, correctoA, comenIncorrectoA, correctoP, comenIncorrectoP, ok){
    if(comprobarVacio(idtrabajo)==false || comprobarTexto(idtrabajo,6)==false || comprobarEspaciosEnBlanco(idtrabajo)==false){
        return false;
    }
    else if(comprobarVacio(loginEvaluador)==false || comprobarTexto(loginEvaluador,9)==false || comprobarEspaciosEnBlanco(loginEvaluador)==false){
        return false;
    }
    else if(comprobarVacio(aliasEvaluado)==false || comprobarTexto(aliasEvaluado,9)==false || comprobarEspaciosEnBlanco(aliasEvaluado)==false){
        return false;
    }
    else if(comprobarVacio(idhistoria)==false || comprobarEspaciosEnBlanco(idhistoria)==false || comprobarTexto(idhistoria,2)==false){
        return false;
    }
    else if(comprobarVacio(correctoA)==false){
        return false;
    }
    else if(comprobarTexto(comenIncorrectoA,300)==false){
        return false;
    }
    else if(comprobarVacio(correctoP)==false){
        return false;
    }
    else if( comprobarTexto(comenIncorrectoP,300)==false){
        return false;
    }
    else if(comprobarEntero(ok, 0, 2)==false){
        return false;
    }
    else{
        return true;
    }
}

function validar_Deliverables(login, idtrabajo, aliasEvaluado, horas, ruta){
    if(comprobarVacio(login)==false || comprobarEspaciosEnBlanco(login)==false || comprobarTexto(login,9)==false){
        return false;
    }
    else if(comprobarVacio(idtrabajo)==false || comprobarTexto(idtrabajo,6)==false || comprobarEspaciosEnBlanco(idtrabajo)==false){
        return false;
    }
    else if(comprobarVacio(aliasEvaluado)==false || comprobarTexto(aliasEvaluado,9)==false || comprobarEspaciosEnBlanco(aliasEvaluado)==false){
        return false;
    }
    else if(comprobarVacio(horas)==false || comprobarEntero(horas, 0, 99)==false){
        return false;
    }
    else if( comprobarVacio(ruta)==false){
        return false;
    }
    else{
        return true;
    }
}

function validar_Action(idaccion, nombreaccion, descripcion){
    if( comprobarVacio(idaccion)==false || comprobarTexto(idaccion,6)==false || comprobarEspaciosEnBlanco(idaccion)==false){
        return false;
    }
    else if(comprobarVacio(nombreaccion)==false || comprobarTexto(nombreaccion,60)==false || comprobarEspaciosEnBlanco(nombreaccion)==false){
        return false;
    }
    else if(comprobarVacio(descripcion)==false || comprobarTexto(descripcion,100)==false || comprobarEspaciosEnBlanco(descripcion)==false){
        return false;
    }
    else{
        return true;
    }
    
}


</script>
