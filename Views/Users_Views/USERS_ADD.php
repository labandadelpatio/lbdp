
<?php
/*  Fichero para la vista de add de usuarios
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class USERS_ADD {


     //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct(){
        $this->Render();

    }


        //funcion Render()
    //A través de está función crearemos la vista

    function Render(){
        
    include '../Views/Header.php'; //header necesita los string
?>


    <h1><?php echo $strings['Add']; ?></h1>

    <div id = "formulario">
            <form method="post"  name="formAdd" action='../Controllers/Users_Controller.php?action=ADD' autocomplete="off" onsubmit="return validar_User(login,password,dni,nombre,apellidos,correo, direccion, telefono) && encriptar(password);">
    <div>
        <label><?php echo $strings['Login']; ?>:</label>
        <input type="text" id="login" name="login" maxlength="9" size="9" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="loginE"></div></div>
    <div>
        <label><?php echo $strings['Password']; ?>:</label>
        <input type="password" id='password' name="password" maxlength="20" size="20" onblur=" comprobarVacio(this) && comprobarTexto(this,128)" />
                <div class="error" id="passwordE"></div></div>
    <div>
        <label><?php echo $strings['DNI']; ?>:</label>
        <input type="text" id="dni" name="DNI" maxlength="9" size="9" onblur="comprobarVacio(this) && comprobarDni(this)" />
                <div class="error" id="dniE"></div></div>
    <div>
        <label><?php echo $strings['Nombre']; ?>:</label>
        <input type="text" id="nombre" name="Nombre" size="30" maxlength="30" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarAlfabetico(this, 30)" />
                <div class="error" id="nombreE"></div></div>
    <div>
        <label><?php echo $strings['Apellidos']; ?>:</label>
        <input type="text" id="apellidos" name="Apellidos" maxlength="50" size="50" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarAlfabetico(this, 50)" />
                <div class="error" id="apellidosE"></div></div>

     <div>
        <label><?php echo $strings['Correo']; ?>:</label>
        <input type="text" id="correo" name="Correo" maxlength="40" size="40" onblur="comprobarVacio(this) && comprobarEmail(this) && comprobarTexto(this,40)" />
                <div class="error" id="correoE"></div></div>

     <div>
        <label><?php echo $strings['Direccion']; ?>:</label>
        <input type="text" id="direccion" name="Direccion" maxlength="60" size="60" onblur="comprobarVacio(this) && comprobarTexto(this,60) && comprobarEspaciosEnBlanco(this)"/>
                <div class="error" id="direccionE"></div></div>
        

    <div>
        <label><?php echo $strings['Telefono']; ?>:</label>
        <input type="text" id="telefono" name="Telefono" maxlength="11" size="11" onblur="comprobarVacio(this) && comprobarTelf(this)" />
        <div class="error" id="telefonoE"></div></div>

    <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <button type="reset" name="limpiar"><img src="../Views/icons/Undo.png"></button><a href="../Controllers/Users_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>



        <?php
         include '../Views/Footer.php'; //header necesita los string



?>
<?php

  }
}


?>
