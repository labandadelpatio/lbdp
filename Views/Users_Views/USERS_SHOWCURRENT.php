
<?php
/*  Fichero para la vista del showcurrent de usuarios
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class USERS_SHOWCURRENT {


     //function __construct() 
    //Mediante esta función declararemos el constructor de la vista
	
	function __construct($valores){
		$this->Render($valores);

	}

    //funcion Render()
    //A través de está función crearemos la vista
    
	function Render($valores){
    include '../Views/Header.php'; //header necesita los string
    
?>

	
<h1><?php echo $strings['current']; ?></h1>
<div id="current"><table>
        <tr>
            <th><?php echo $strings['Login']; ?></th>
            <td><?php echo $valores[0]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Password']; ?></th>
            <td><?php echo $valores[1]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['DNI']; ?></th>
            <td><?php echo $valores[2]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Nombre']; ?></th>
            <td><?php echo $valores[3]; ?></td>
        </tr>
        <tr>
            <th><?php echo $strings['Apellidos']; ?></th>
            <td><?php echo $valores[4]; ?></td>
        </tr>

        <tr>
            <th><?php echo $strings['Correo']; ?></th>
            <td><?php echo $valores[5]; ?></td>
        </tr>
        
        <tr>
            <th><?php echo $strings['Direccion']; ?></th>
            <td><?php echo $valores[6]; ?></td>
         </tr>  
         
        <tr>
            <th><?php echo $strings['Telefono']; ?></th>
            <td><?php echo $valores[7]; ?></td>
        </tr>

     
        </table>
        <br>
        <div id="imgtableshw">
            <a href="../Controllers/Users_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        </div>


	
 

<?php
include '../Views/Footer.php';

?>
<?php

  } 
}


?>
