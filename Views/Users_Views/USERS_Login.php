
<?php
/*  Fichero para la vista de login de un usuario
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
	class Login{


     //function __construct() 
    //Mediante esta función declararemos el constructor de la vista


		function __construct(){	
			$this->render();
		}
    //funcion Render()
    //A través de está función crearemos la vista
		
		function render(){

			include '../Views/Header.php'; //header necesita los strings
		    

?>		

			
    <div id = "loginform">
			<form name = "formlogin" action='../Controllers/Login_Controller.php' method="post" autocomplete="off" onsubmit="return validar_User_Login(login,password) && encriptar(password);">
    <div>
        <label><?php echo $strings['Login']; ?>:</label>
        <input type="text" id="login" name="login" maxlength="9" size="9" onblur="comprobarVacio(this);" /><div class="error" id="loginE"></div></div>
    <div>
        <label><?php echo $strings['Password']; ?>:</label>
        <input type="password" id='password' name="password" maxlength="20" size="20" onblur="comprobarVacio(this);" />
        <div class="error" id="passwordE"></div></div>  
        
            <div id="imgtable"><button type='submit' name='logeo' id='logeo'><img src="../Views/icons/OK.png"></button></div>

			</form>
    </div>	
					


<?php
include '../Views/Footer.php';




	}	
}


?>