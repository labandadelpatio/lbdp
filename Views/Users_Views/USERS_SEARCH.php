
<?php
/*  Fichero para la vista de search de usuarios
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class USERS_SEARCH {


     //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

	function __construct(){
		$this->Render();

	}
    //funcion Render()
    //A través de está función crearemos la vista

	function Render(){
    include '../Views/Header.php'; //header necesita los string
   
?>

 	<h1><?php echo $strings['Search']; ?></h1>

    <div id = "searchform">
			<form method="post"  name="formadd" action='../Controllers/Users_Controller.php?action=SEARCH' autocomplete="off" onsubmit="">
    <div>
        <label><?php echo $strings['Login']; ?>:</label>
        <input type="text" id="login" name="login" maxlength="9" size="9" onblur="" /><div class="error" id="loginE"></div></div>
    <div>
        <label><?php echo $strings['Password']; ?>:</label>
        <input type="password" id='password' name="password" maxlength="20" size="20" onblur="" />
        <div class="error" id="password"></div></div>   
    <div>
        <label><?php echo $strings['DNI']; ?>:</label>
        <input type="text" id="DNI" name="DNI" maxlength="9" size="9" onblur="" /><div class="error" id="dniE"></div></div>
    <div>
        <label><?php echo $strings['Nombre']; ?>:</label>
        <input type="text" id="Nombre" name="Nombre" size="30" maxlength="30" onblur="" /><div class="error" id="nameE"></div></div>
    <div>
        <label><?php echo $strings['Apellidos']; ?>:</label>
        <input type="text" id="Apellidos" name="Apellidos" maxlength="50" size="50" onblur="" /><div class="error" id="surnameE"></div></div>

     <div>
        <label><?php echo $strings['Correo']; ?>:</label>
        <input type="text" id="Correo" name="Correo" maxlength="40" size="40" onblur="" /><div class="error" id="emailE"></div></div>

     <div>
        <label><?php echo $strings['Direccion']; ?>:</label>
        <input type="text" id="Direccion" name="Direccion" maxlength="60" size="60" /></div>
        	
    <div>
        <label><?php echo $strings['Telefono']; ?>:</label>
        <input type="text" id="Telefono" name="Telefono" maxlength="11" size="11" onblur="" /><div class="error" id="telefonoE"></div></div>
    
   <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Users_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>

 

<?php
 include '../Views/Footer.php'; //header necesita los string

?>
<?php

  } 
}


?>
