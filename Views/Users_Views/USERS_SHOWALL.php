
<?php
/*  Fichero para la vista del showall de usuarios
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class USERS_SHOWALL {


     //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

	function __construct($fila, $resultado){
		$this->Render($fila, $resultado);

	}

    //funcion Render()
    //A través de está función crearemos la vista

	function Render($fila, $resultado){
    include '../Views/Header.php'; //header necesita los string
?>

	<div id="showall">
        <div id="imgtable">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 3, 1)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/Users_Controller.php?action=ADD"><img src="../Views/icons/Create.png"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 3, 2)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
					<a href="../Controllers/Users_Controller.php?action=SEARCH"><img src="../Views/icons/Find.png"/></a>
<?php }
?>
					<a href="../Controllers/Users_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        <table>
        <tr>
            <th><?php echo $strings['Login']; ?></th>
            <th><?php echo $strings['DNI']; ?></th>
            <th><?php echo $strings['Nombre']; ?></th>
            <th><?php echo $strings['Correo']; ?></th>
            <th><?php echo $strings['opciones']; ?></th>
        </tr>

<?php
if( tienePermisoFuncAcc($_SESSION['login'], 3, 0)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
        while($fila = $resultado->fetch_assoc())
        {
            echo "<tr>";
            echo "<td>".$fila["login"]."</td>";
            echo "<td>".$fila["dni"]."</td>";
            echo "<td>".$fila["nombre"]."</td>";
            echo "<td>".$fila["correo"]."</td>";
?>
            <td> 
                <div id="imagsall">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 3, 5)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
                    <a href="../Controllers/Users_Controller.php?action=SHOWCURRENT&login=<?php echo $fila['login'] ?>"><img src="../Views/icons/How-to.png" width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 3, 3)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Users_Controller.php?action=EDIT&login=<?php echo $fila['login'] ?>"><img src="../Views/icons/Modify.png", width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 3, 6)){
?>
										<a href="../Controllers/UsersGroup_Controller.php?action=ASSIGN&login=<?php echo $fila['login'] ?>"><img src="../Views/icons/asignar.png", width="16"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 3, 4)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Users_Controller.php?action=DELETE&login=<?php echo $fila['login'] ?>"><img src="../Views/icons/Erase.png" width="14"/></a></div></td>
<?php
}
                echo "</tr>";
        }
			}

?>
        </table>
</div>


<?php
 include '../Views/Footer.php'; //header necesita los string

?>
<?php

  }
}


?>
