

<?php
/*  Fichero para la vista de index de un usuario
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class Index {


     //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

	function __construct(){
		$this->render();
	}

	  //funcion render()
    //A través de está función crearemos la vista

	function render(){
		include '../Views/Header.php'; //header necesita los strings
		include '../Views/Footer.php';
	}

}

?>