
<?php
/*  Fichero para la vista de añadir grupos a los usuarios
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class USERS_ASSIGN {


     //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct($valores, $lista, $resultado){
        $this->Render($valores, $lista, $resultado);

    }

      //funcion Render()
     //A través de está función crearemos la vista

    function Render($valores, $lista, $resultado){
    include '../Views/Header.php'; //header necesita los string
    include_once '../Functions/TieneUser.php'; //incluimos el fichero que contiene la funcion TieneUse
?>
    <h1><?php echo $strings['Asignar']; ?></h1>

   <div id = "editar">
       <div id="asignar"><p><strong style="font-size:15px;"><?php echo $valores['login']; ?>:</strong></p></div>
              <?php echo $strings['Grupos de usuarios disponibles'];?>:<br><br>

            <?php
            while( $lista = $resultado->fetch_assoc() ){ //mientras haya entregas
            if(!tieneUser($lista['idgrupo'],$valores[0])){
?>
            <a href="../Controllers/UsersGroup_Controller.php?action=ASSIGN&login=<?php echo $valores['login']; ?>&idgrupo=<?php echo $lista['idgrupo']; ?>" id='idgrupo' name='idgrupo'><img  src="../Views/icons/noCheck.png" /></a>
<?php
            }
            else{
?>
            <a href="../Controllers/UsersGroup_Controller.php?action=DESASSIGN&login=<?php echo $valores['login']; ?>&idgrupo=<?php echo $lista['idgrupo']; ?>" id='idgrupo' name='idgrupo'><img  src="../Views/icons/check.png" /></a>
<?php
            }
?>
            <label><strong><?php echo $lista['idgrupo'] ?></strong></label></br>

<?php
            }
?>



        <div><div id="imgtable"><!--<button type="submit" name="submit" id="submit"><img src="../Views/icons/OK.png"></button>-->
        <a href="../Controllers/Users_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form></div>
<?php
include '../Views/Footer.php'; //incluimos la vista del Pie

?>
<?php

  }
}


?>
