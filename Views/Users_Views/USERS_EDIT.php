
<?php
/*  Fichero para la vista de edit de un usuario
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class USERS_EDIT {


     //function __construct() 
    //Mediante esta función declararemos el constructor de la vista

    function __construct($valores){
        $this->Render($valores);

    }

        //funcion Render()
    //A través de está función crearemos la vista

    function Render($valores){
    include '../Views/Header.php'; //header necesita los string
    
?>
    <h1><?php echo $strings['Edit']; ?></h1>

    <div id = "editar">
            <form method="post" enctype="multipart/form-data" name="formEdit" action='../Controllers/Users_Controller.php?action=EDIT' autocomplete="off" onsubmit="return validar_User(login,password,dni,nombre,apellidos,correo, direccion, telefono)">
    <div>
        <label><?php echo $strings['Login']; ?>:</label>
        <input type="text" id="login" name="login" maxlength="9" size="9" readonly onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" value="<?php echo $valores[0]; ?>" />
                <div class="error" id="loginE"></div></div>
    <div>
        <label><?php echo $strings['Password']; ?>:</label>
        <input type="password" id='password' name="password" maxlength="20" size="20" onblur="comprobarVacio(this) && comprobarTexto(this,20)" value="<?php echo $valores[1]?>"  />
        <div class="error" id="passwordE"></div></div>
    <div>
        <label><?php echo $strings['DNI']; ?>:</label>
        <input type="text" id="dni" name="DNI" maxlength="9" size="9" onblur="comprobarVacio(this) && comprobarDni(this)" value="<?php echo $valores[2]?>"  />
                <div class="error" id="dniE"></div></div>
    <div>
        <label><?php echo $strings['Nombre']; ?>:</label>
        <input type="text" id="nombre" name="Nombre" size="30" maxlength="30" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarAlfabetico(this, 30)" value="<?php echo $valores[3]?>" />
                <div class="error" id="nombreE"></div></div>
    <div>
        <label><?php echo $strings['Apellidos']; ?>:</label>
        <input type="text" id="apellidos" name="Apellidos" maxlength="50" size="50" onblur="comprobarVacio(this) && comprobarEspaciosEnBlanco(this) && comprobarAlfabetico(this, 50)" value="<?php echo $valores[4]?>" />
                <div class="error" id="apellidosE"></div></div>

     <div>
        <label><?php echo $strings['Correo']; ?>:</label>
        <input type="text" id="correo" name="Correo" maxlength="40" size="40" onblur="comprobarVacio(this) && comprobarEmail(this) && comprobarTexto(this,40)" value="<?php echo $valores[5]?>"  />
                <div class="error" id="correoE"></div></div>

     <div>
        <label><?php echo $strings['Direccion']; ?>:</label>
        <input type="text" id="direccion" name="Direccion" maxlength="60" size="60" onblur="comprobarVacio(this) && comprobarTexto(this,60) && comprobarEspaciosEnBlanco(this)" value="<?php echo $valores[6]?>"  />
                <div class="error" id="direccionE"></div></div>

    <div>
        <label><?php echo $strings['Telefono']; ?>:</label>
        <input type="text" id="telefono" name="Telefono" maxlength="11" size="11" onblur="comprobarVacio(this) && comprobarTelf(this)" value="<?php echo $valores[7]?>"/>
        <div class="error" id="telefonoE"></div></div>
    <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Users_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>



        <?php
             include '../Views/Footer.php'; //header necesita los string


?>
<?php

  }
}


?>
