<?php

/*Archivo php, menu lateral, contiene el html (estructura y elementos) del menú lateral, visible una vez nos hemos logueado en la aplicación.
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/

?>
<aside>
	<nav>
		<ul class="nav">
     <li><a href="">QAS</a></li>
     <li><a href="../Controllers/Deliverables_Controller.php">Entregas</a></li>   
     </ul>
	</nav>
 </aside>