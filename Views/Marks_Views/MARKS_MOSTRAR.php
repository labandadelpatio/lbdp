
<?php
/*  Fichero para la vista del showall de calificaciones
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class MARKS_MOSTRAR {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista


	function __construct($fila, $resultado){
		$this->Render($fila, $resultado);

	}
    //funcion Render()
    //A través de está función crearemos la vista

	function Render($fila, $resultado){
      include '../Views/Header.php';


?>
    <div id="notas">
        <div id="imgtable">
            <a href="../Controllers/Marks_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
        <table border = 1>
<?php
            $cont = 0;
            $cuadrados = 0;
            $cambiar = true;
            $comentarios = 0;
            $coment = array('','','','','');
            $comentprof = "";
        while($fila = $resultado->fetch_array()){
            
            
            if($cont == 0){
                
                
                echo "<tr><td colspan=\"6\" bgcolor='beige' style=\"font-size: 15px;\">".$fila[0]."-. ".$fila[5]."</td></tr><tr>";
                
                if($fila[1] == 1){
                    
                    echo "<td bgcolor = 'limegreen'>".$fila[1]."</td>";
                        $cuadrados = $cuadrados + 1;
                    
                }else{
                    
                    echo "<td bgcolor = 'firebrick'>".$fila[1]."</td>"; 
                        $coment[$cuadrados] = "$fila[3]";
                        $cuadrados = $cuadrados + 1;
                }
                $cont = $cont + 1;
                
            }else if($cont == 1){
                
                   
                while($cuadrados < 5 && $cambiar){
                    if($fila[1] == 1){
                       
                        echo "<td bgcolor = 'limegreen'>".$fila[1]."</td>";
                            $cuadrados = $cuadrados + 1;
                            $cambiar = false;
                    }else{
                        
                        echo "<td bgcolor = 'firebrick'>".$fila[1]."</td>"; 
                            $coment[$cuadrados] = "$fila[3]";
                            $cuadrados = $cuadrados + 1;
                            $cambiar = false;
                    }
                }
                $cambiar = true;
                if( $cuadrados >= 5){
                    if($fila[2] == 1){
                        
                        echo "<td bgcolor = 'limegreen'><strong style=\"font-size: 13px;\">".$fila[2]."</strong></td>";
                            
                    }else{
                        
                        echo "<td bgcolor = 'firebrick'><strong style=\"font-size: 13px;\">".$fila[2]."</strong></td>"; 
                            $comentprof = "$fila[4]";
                    }
                    echo "</tr><tr><td colspan=\"6\">";
                         
                
                    while( $comentarios < 5 ){
                    if($coment[$comentarios] <> ''){
                        
                        echo $coment[$comentarios]."</br>";
                            $comentarios = $comentarios+1;
                    }else{
                        
                            $comentarios = $comentarios+1;
                    }
                }
                if($comentarios >= 4){
                if($comentprof <> ''){
                       
                        echo "<strong style>".$comentprof."</strong></br>";
                            
                }
                    echo "</td></tr>";
                    $cont = 0;
                    $cuadrados = 0;
                    $cambiar = true;
                    $comentarios = 0;
                    $coment = array('','','','','');
                    $comentprof = "";
                
                }
                }
                
                
                
            }else{
                echo "esto no debería salir nunca";
                
                
            }
            
        
        }
        
        
?>
            </table>
       
</div>
            
<?php
			
                
       include '../Views/Footer.php'; //header necesita los string
  }

}

?>
