
<?php
/*  Fichero para la vista de edit de calificaciones
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class MARKS_EDIT {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista


    function __construct($valores){
        $this->Render($valores);

    }
    //funcion Render()
    //A través de está función crearemos la vista

    function Render($valores){
    include '../Views/Header.php'; //header necesita los string
        ?>
            <h1><?php echo $strings['Edit']; ?></h1>

    <div id = "editar"> <?php //Definimos el formulario que nos permitirá editar una nota?>
            <form method="post"  name="formEdit" action='../Controllers/Marks_Controller.php?action=EDIT' autocomplete="off" onsubmit="return validar_Marks(login, idtrabajo, notaTrabajo)">
    <div>
        <label><?php echo $strings['Login']; ?>:</label>
        <input type="text" id="login" name="login" maxlength="9" size="9" readonly value="<?php echo $valores[0] ?>" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="loginE"></div></div>
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id='idtrabajo' name="idtrabajo" maxlength="6" readonly  value="<?php echo $valores[1] ?>" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,6) && comprobarEspaciosEnBlanco(this)" />
        <div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Calificacion']; ?>:</label>
        <input type="text" id="notaTrabajo" name="notaTrabajo"  size="7" maxlength="7" value="<?php echo $valores[2] ?>" onblur="comprobarVacio(this) && comprobarReal(this, 2, 0, 10)" />
                <div class="error" id="notaTrabajoE"></div></div>

        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Marks_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>

        <?php
             include '../Views/Footer.php'; //header necesita los string

  }
}


?>
