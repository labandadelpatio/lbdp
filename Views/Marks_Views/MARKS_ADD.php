
<?php
/*  Fichero para la vista de add de calificaciones
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class MARKS_ADD {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista


    function __construct(){
        $this->Render();

    }
    //funcion Render()
    //A través de está función crearemos la vista

    function Render(){
      include '../Views/Header.php';
        ?>
            <h1><?php echo $strings['Add']; ?></h1>

    <div id = "formulario"> <?php //Definimos el formulario que nos permitirá añadir una nota?>
            <form method="post"  name="formAdd" action='../Controllers/Marks_Controller.php?action=ADD' autocomplete="off" onsubmit="return validar_Marks(login, idtrabajo, notaTrabajo)">
    <div>
        <label><?php echo $strings['Login']; ?>:</label>
        <input type="text" id="login" name="login" maxlength="9" size="9" onblur="comprobarVacio(this) && comprobarTexto(this,9) && comprobarEspaciosEnBlanco(this)" />
                <div class="error" id="loginE"></div></div>
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id='idtrabajo' name="idtrabajo" maxlength="6" size="6" onblur="comprobarVacio(this) && comprobarTexto(this,6) && comprobarEspaciosEnBlanco(this)" />
        <div class="error" id="idtrabajoE"></div></div>
    <div>
        <label><?php echo $strings['Calificacion']; ?>:</label>
        <input type="text" id="notaTrabajo" name="notaTrabajo"  size="7" maxlength="7" onblur="comprobarVacio(this) && comprobarReal(this, 2, 0, 10)" />
        <div class="error" id="notaTrabajoE"></div></div>

        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <button type="reset" name="limpiar"><img src="../Views/icons/Undo.png"></button><a href="../Controllers/Marks_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>

        <?php
             include '../Views/Footer.php'; //header necesita los string

  }
}


?>
