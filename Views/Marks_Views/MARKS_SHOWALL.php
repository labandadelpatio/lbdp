
<?php
/*  Fichero para la vista del showall de calificaciones
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class MARKS_SHOWALL {


    //function __construct() 
//Mediante esta función declararemos el constructor de la vista

	function __construct($fila, $resultado){
		$this->Render($fila, $resultado);

	}
    //funcion Render()
    //A través de está función crearemos la vista

	function Render($fila, $resultado){
      include '../Views/Header.php';


?>
    <div id="showall">
        <div id="imgtable">


<?php
if( tienePermisoFuncAcc($_SESSION['login'], 2, 1)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
            <a href="../Controllers/Marks_Controller.php?action=ADD"><img src="../Views/icons/Create.png"/></a>

<?php
}
if( tienePermisoFuncAcc($_SESSION['login'], 2, 2)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
            <a href="../Controllers/Marks_Controller.php?action=SEARCH"><img src="../Views/icons/Find.png"/></a><a href="../Controllers/Marks_Controller.php"><img src="../Views/icons/Exit.png"/></a>
<?php }
?>
        </div>
        <table>
        <tr>
            <th><?php echo $strings['Login']; ?></th>
            <th><?php echo $strings['ID Trabajo']; ?></th>
            <th><?php echo $strings['Nombre Trabajo']; ?></th>
            <th><?php echo $strings['Calificacion']; ?></th>
            <th><?php echo $strings['opciones']; ?></th>
        </tr>

<?php
if( tienePermisoFuncAcc($_SESSION['login'], 2, 0)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
        while($fila = $resultado->fetch_assoc())
        {

        	if(isUser($fila['login']) || isAdmin($_SESSION['login'])){
            echo "<tr>";
            echo "<td>".$fila["login"]."</td>";
            echo "<td>".$fila["IdTrabajo"]."</td>";
            echo "<td>".$fila["NombreTrabajo"]."</td>";
            echo "<td>".$fila["NotaTrabajo"]."</td>";
        
?>
            <td>
                <div id="imagsall">
<?php
if( tienePermisoFuncAcc($_SESSION['login'], 2, 5)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
    
    if(substr($fila['NombreTrabajo'],0,2)=="ET"){ //Se moestrara para aquellos que el nombre empiece por ET
        ?>
                    <a href="../Controllers/Marks_Controller.php?action=MOSTRAR&login=<?php echo $fila['login']?>&idtrabajo=<?php echo $fila['IdTrabajo']?>"><img src="../Views/icons/How-to.png" width="14"/></a>
<?php
        }else{
        ?>
                    <a href="../Controllers/Marks_Controller.php?action=MOSTRARQA&login=<?php echo $fila['login']?>&idtrabajo=<?php echo $fila['IdTrabajo']?>"><img src="../Views/icons/How-to.png" width="14"/></a>
        
   <?php     
    }                                            
                                                    
                                                    }
if( tienePermisoFuncAcc($_SESSION['login'], 2, 3)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Marks_Controller.php?action=EDIT&login=<?php echo $fila['login']?>&idtrabajo=<?php echo $fila['IdTrabajo']?>"><img src="../Views/icons/Modify.png" width="14"/></a>
<?php }
if( tienePermisoFuncAcc($_SESSION['login'], 2, 4)){ //según los permisos, funcionalidades y acciones que tenga el usuario se le mostrará o no el siguiente icono
?>
										<a href="../Controllers/Marks_Controller.php?action=DELETE&login=<?php echo $fila['login']?>&idtrabajo=<?php echo $fila['IdTrabajo'] ?>"><img src="../Views/icons/Erase.png" width="14"/></a></div></td>
<?php
			}
                echo "</tr>";
    }
            }
}
?>
        </table>
</div>


<?php
       include '../Views/Footer.php'; //header necesita los string
  }
}


?>
