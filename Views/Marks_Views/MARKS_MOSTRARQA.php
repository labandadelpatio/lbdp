
<?php
/*  Fichero para la vista del showall de calificaciones
  Autor: lbdp  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas  Fecha: 27/11/2017*/
class MARKS_MOSTRARQA {

//function __construct() 
//Mediante esta función declararemos el constructor de la vista


	function __construct($fila, $resultado){
		$this->Render($fila, $resultado);

	}
    //funcion Render()
    //A través de está función crearemos la vista

	function Render($fila, $resultado){
      include '../Views/Header.php';


?>
    <div id="notasqa">
        <div id="imgtable">
            <a href="../Controllers/Marks_Controller.php"><img src="../Views/icons/Exit.png"/></a>
        </div>
       <table border = 1>
        
<?php
            $cont = 0;
            $aux = 0;
            $nombres = 0;
            $cuadrados = 0;
            $cambiar = true;
            $comentarios = 0;
            $coment = array('','','','','');
            $cuad = array('','','','','','','','','','');
        while($fila = $resultado->fetch_array()){ //recorremos de una en una las tuplas del array
            
            if($cont == 0){ //si el contador es 0
                echo "<tr><td colspan=\"10\" bgcolor='beige' style=\"font-size: 15px;\">".$fila[0]."-. ".$fila[1]."</td></tr><tr>"; //se escribe la primera historia
                
                echo "<td colspan=\"2\"><strong>".$fila[4]."</strong></td>"; //abrimos la columna
                    $cuad[$aux] = $fila[5];
                    $coment[$nombres] = $fila[3];
                    $aux = $aux + 1;
                    $cuad[$aux] = $fila[2];
                    $aux = $aux + 1;
                    $nombres = $nombres + 1;
                    
                
                $cont = $cont+1; //incrementamos contador
                
            }else if($cont == 1){ //si contador es igual a 1
                while($nombres < 5 && $cambiar){ //y no se han completado los 5 recorridos y cambiar = true
                    
                    echo "<td colspan=\"2\"><strong>".$fila[4]."</strong></td>";
                    $cuad[$aux] = $fila[5];
                    $coment[$nombres] = $fila[3];
                    $aux = $aux + 1;
                    $cuad[$aux] = $fila[2];
                    $aux = $aux + 1;
                    $nombres = $nombres + 1;
                    $cambiar = false;
                   
                }
                $cambiar = true;
                
                if($nombres >= 5){
                echo "</tr><tr>";
                    
                   
                while($cuadrados < 10){
                    if($cuad[$cuadrados] == 1){
                        echo "<td bgcolor = 'limegreen'></td>";
                           $cuadrados = $cuadrados + 1;
                        
                        if($cuad[$cuadrados] == 1){
                        echo "<td bgcolor = 'limegreen'>".$cuad[$cuadrados]."</td>";
                            $cuadrados = $cuadrados + 1;
                            
                    }else{
                        echo "<td bgcolor = 'firebrick'>".$cuad[$cuadrados]."</td>"; 
                            $cuadrados = $cuadrados + 1;
                            
                    }
                        
                        
                            
                    }else{
                        echo "<td bgcolor = 'firebrick'></td>"; 
                            $cuadrados = $cuadrados + 1;
                        
                        if($cuad[$cuadrados] == 1){
                        echo "<td bgcolor = 'limegreen'>".$cuad[$cuadrados]."</td>";
                            $cuadrados = $cuadrados + 1;
                            
                    }else{
                        echo "<td bgcolor = 'firebrick'>".$cuad[$cuadrados]."</td>"; 
                            $cuadrados = $cuadrados + 1;
                            
                    }
                            
                    }
                    
                }
                
                
                if( $cuadrados >= 10){
                    echo "</tr><tr><td colspan=\"10\">";
                        $cont = $cont+1; 
                        $cuadrados = 0;
                    
                    if($coment[$comentarios] <> ''){
                        echo $coment[$comentarios]."</br>";
                            $comentarios = $comentarios+1;
                    }else{
                        echo "Se cumple la historia</br>";
                            $comentarios = $comentarios+1;
                    }
                
                    while( $comentarios < 5 ){
                    if($coment[$comentarios] <> ''){
                        echo $coment[$comentarios]."</br>";
                            $comentarios = $comentarios+1;
                    }else{
                        echo "Se cumple la historia</br>";
                            $comentarios = $comentarios+1;
                    }
                
                }
                
                if($comentarios >= 4){
                    echo "</td></tr>";
                    $cont = 0;
                    $aux = 0;
                    $nombres = 0;
                    $cuadrados = 0;
                    $cambiar = true;
                    $comentarios = 0;
                    $coment = array('','','','','');
                    $cuad = array('','','','','','','','','','');
                    
                
                }
                }
                    }
             }else{
                echo "esto no debería salir nunca";
                
            }
        
        }
        
?>
            </table>
            
        

          
</div>
            
<?php
			
                
       include '../Views/Footer.php'; //header necesita los string
  }

}

?>