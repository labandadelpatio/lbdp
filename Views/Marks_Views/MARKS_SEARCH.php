
<?php
/*  Fichero para la vista de search de calificaciones
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/
class MARKS_SEARCH {
	
    //function __construct() 
   //Mediante esta función declararemos el constructor de la vista

	function __construct(){
		$this->Render();

	}

        //funcion Render()
    //A través de está función crearemos la vista


	function Render(){
   include '../Views/Header.php';
		?>
			<h1><?php echo $strings['Search']; ?></h1>

    <div id = "searchform">
			<form method="post" name="formadd" action='../Controllers/Marks_Controller.php?action=SEARCH' autocomplete="off" onsubmit="">
    <?php
    if(isAdmin($_SESSION['login'])){
    ?>
    <div>
        <label><?php echo $strings['Login']; ?>:</label>
        <input type="text" id="login" name="login" maxlength="9" size="9" onblur="" />
        <div class="error" id="loginE"></div></div>
                <?php
    }
        ?>
    <div>
        <label><?php echo $strings['ID Trabajo']; ?>:</label>
        <input type="text" id='idtrabajo' name="idtrabajo" maxlength="6" size="6" onblur="" />
        <div class="error" id="idTrabajoE"></div></div>   
    <div>
        <label><?php echo $strings['Calificacion']; ?>:</label>
        <input type="text" id="notaTrabajo" name="notaTrabajo"  size="6" onblur="" />
        <div class="error" id="notaTrabajoE"></div></div>
    
        <div>
        <div id="imgtable"><button type="submit" name="submit" id="submit" ><img src="../Views/icons/OK.png"></button>
        <a href="../Controllers/Marks_Controller.php"><img src="../Views/icons/Exit.png"/></a></div></div>
        </form>
    </div>
		
		<?php
			 include '../Views/Footer.php'; //header necesita los string


  } 
}


?>
