<?php

/*Archivo php, menu lateral, contiene el html (estructura y elementos) del menú lateral, visible una vez nos hemos logueado en la aplicación.
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017*/

include '../Functions/TienePermisoFunc.php';//inlcuimos el archivo que contiene la funcion TienePermisoFunc

//Cada categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y FUncionalidad para ello

?>
<aside>
	<nav>
	<ul class="nav">
<?php if( TienePermisoFunc( $_SESSION['login'], 0 ) ) { ?>  <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/Deliverables_Controller.php"><?php echo $strings['Entregas'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 1 ) ) { ?> <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/QAS_Controller.php"><?php echo $strings['QAS'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 2 ) ) { ?> <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/Marks_Controller.php"><?php echo $strings['Calificaciones'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 3 ) ) { ?> <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/Users_Controller.php"><?php echo $strings['Usuarios'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 4 ) ) { ?><?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/Groups_Controller.php"><?php echo $strings['Grupos de Usuario'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 5 ) ) { ?> <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/Functionalities_Controller.php"><?php echo $strings['Funcionalidades'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 6 ) ) { ?> <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/Action_Controller.php"><?php echo $strings['Acciones'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 7 ) ) { ?> <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/Permissions_Controller.php"><?php echo $strings['Permisos'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 8 ) ) { ?> <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/Jobs_Controller.php"><?php echo $strings['Trabajos'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 9 ) ) { ?> <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/Stories_Controller.php"><?php echo $strings['Historias de Usuario'] ?></a></li>
<?php }
			if( TienePermisoFunc( $_SESSION['login'], 10 ) ) { ?> <?php //categoría del menú se mostrará sólo si el usuario conectado tiene Permiso y Funcionalidad para ello?>
     <li><a href="../Controllers/EvaluationQAS_Controller.php?action=SHOW"><?php echo $strings['Evaluacion QAS'] ?></a></li>
<?php }
?>
    </ul>
	</nav>
 </aside>
