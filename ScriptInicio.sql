/* Script que se utiliza una vez creada la base de datos para rellenar los datos
*   de usuarios,grupos, acciones y funcionalidades básicas para poder utilizar
*   la app
*   Autor: lbdp
  Fecha: 15/12/2017
*/

-- El usuario administrador del sistema para poder entrar y gestionar todo, cuando esta por defecto, CONTRASEÑA:ADMIN
INSERT INTO USUARIO(login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono)
  VALUES ('admin', '21232f297a57a5a743894a0e4a801fc3', '38609989G', 'Juan', 'González González' ,'juan@gmail.com', 'Av. De Habana', '612345678');


/******************************************************************************/
-- El grupo de Alumnnos donde de asignaran todos los usuarios que se registren en la página
INSERT INTO GRUPO(IdGrupo, NombreGrupo, DescripGrupo)
  VALUES ('0', 'Alumnnos', 'Grupo donde se encuentran los Alumnnos');

-- El grupo de Administradores, con todos los permisos por defecto
INSERT INTO GRUPO(IdGrupo, NombreGrupo, DescripGrupo)
  VALUES ('1', 'Administrador', 'Grupo donde se encuentran los Administradores');


/******************************************************************************/
-- Se asigna el administrador creado anteriormente al grupo 1(Administrador)
INSERT INTO USU_GRUPO(login, IdGrupo)
  VALUES ('admin', '1');


/******************************************************************************/
-- Funcionalidades:
-- Entregas
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('0', 'Entregas', 'Funcionalidad en donde los alumnos subiran las entregas');

-- QAS
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('1', 'QAS', 'Funcionalidad en donde estarán las QAS que los alumnos tienen que corregir');

-- Calificaciones
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('2', 'Calificaciones', 'Funcionalidad donde se podrán ver las calificaciones de las entregas y QAS');

-- Usuarios
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('3', 'Usuarios', 'Funcionalidad donde se podrán registrar nuevos usuarios, además de
                            gestionar los ya existentes');

-- Grupos de Usuarios
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('4', 'Grupos de Usuarios', 'Funcionalidad donde se podrán crear nuevos grupos de usuarios,
                                        además de gestionar los ya existentes');

-- Funcionalidades
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('5', 'Funcionalidades', 'Funcionalidad donde se podrán crear nuevas funcionalidades, además de
                                    gestionar las ya existentes');

-- Acciones
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('6', 'Acciones', 'Funcionalidad donde se podrán crear nuevas acciones, además de gestionar las ya existentes');

-- Permisos
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('7', 'Permisos', 'Funcionalidad donde se podrán ver los permisos para acceder a unas funcionalidades y
                              realizar acciones que tienen los distintos grupos');
-- Trabajos
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('8', 'Trabajos', 'Funcionalidad donde se podrán crear trabajos, además de gestionar los ya existentes');

-- Historias de Usuario
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('9', 'Historias de Usuario', 'Funcionalidad donde se podrán crear nuevas historias de usuario para las QAS,
                                          además de gestionar las ya existentes');

-- Evaluacion QAS
INSERT INTO FUNCIONALIDAD(IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad)
  VALUES ('10', 'Evaluacion QAS', 'Funcionalidad donde el profesor/administrador podrá evaluar las historias de usuarios
                                    de un determinado trabajo y las QAS de los alumnos a estas historias');


/******************************************************************************/
-- Acciones:
-- Show All
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('0', 'Show all', 'Acción que permite ver todas tuplas de la tabla de la funcionalidad en cuestión');

-- Añadir
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('1', 'Añadir', 'Acción que permite añadir nuevas tuplas a la tabla de la funcionalidad en cuestión');

-- Buscar
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('2', 'Buscar', 'Acción que permite buscar tuplas en la tabla de la funcionalidad en cuestión');

-- Editar
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('3', 'Editar', 'Acción que permite editar tuplas de la tabla de la funcionalidad en cuestión');

-- Borrar
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('4', 'Borrar', 'Acción que permite borrar tuplas de la tabla de la funcionalidad en cuestión');

-- Show Current
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('5', 'Show Current', 'Acción que permite ver una tupla en detalle de la tabla de la funcionalidad en cuestión');

-- Administrar Usuario a grupos
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('6', 'Administrar Usuarios a grupo', 'Acción que permite añadir o borrar un usuario de un grupo');

-- Administrar Permisos de los grupos
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('7', 'Permisos Grupos', 'Acción que permite gestionar los permisos que tienen los grupos para
                                    realizar ciertas funcionalidades y acciones');

-- Asignar acciones a funcionalidades
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('8', 'Acciones a Funcionalidades', 'Acción que permite asignar acciones a las funcionalidades');

-- Acceder
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('9', 'Acceder', 'Acción que permite acceder a la entrega de un trabajo');

-- Gererar asignaciond e QAS
INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('10', 'Asignacion QAS', 'Acción con la cual se asignaran automaticamente 5 entregas a todos los
                                      usuarios que hayan hecho la entrega para corregirla a otras personas');

INSERT INTO ACCION(IdAccion, NombreAccion, DescripAccion)
  VALUES ('11', 'Generar Notas', 'Acción con la cual se generaran las notas para las entregas que se realizaran');


  /*ET1*/

INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','1','Puede ejecutarse y funciona la aplicación al abrir el directorio que la contiene desde el navegador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','2','El diseño mantiene coherencia visual entre los elementos de la página O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','3','El diseño de los formularios es coherente entre los mismos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','4','El diseño de las tablas de muestra de datos es coherente entre las mismas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','5','La página mantiene la estructura de la presentación ante un redimensionamiento del navegador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','6','La página mantiene coherencia entre opciones (tiene siempre la misma opción para la misma acción)');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','7','Los campos de formulario tienen el tamaño de control correcto para el atributo de la tabla que solicitan');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','8','Los campos del formulario tiene el tamaño del dato solicitado correcto para el atributo de la tabla que solicitan');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','9','Todas las acciones están representadas por iconos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','10','Los SHOWALL son claros y visualmente correctos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','11','Las confirmaciones de DELETE son claras y visualmente correctas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','12','Existe una validación correcta por campo de formulario O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','13','Existe una validación por submit que comprueba la validez de todos los campos del formulario antes de enviar al servidor O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','14','Los formularios de SEARCH permiten buscar por todos los campos que se desee');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','15','Los formularios de SEARCH permiten colocar valores parciales en cada campo del formulario (p.e. una parte del dni)');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','16','Los formularios de SEARCH permiten buscar solo por parte de los campos del formulario');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','17','Los formularios de EDIT no permiten modificar los campos clave de la tabla');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','18','En los formularios de ADD son obligatorios los campos NOT NULL de la tabla');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','19','En los formularios de EDIT son obligatorios los campos NOT NULL de la tabla');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','20','Las funciones de código tienen comentario con una descripción antes de su comienzo O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','21','En el código están todas las variables definidas O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','22','En el código están las variables comentadas en su definición O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','23','En el código están comentadas todas las estructuras de control O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','24','Los ficheros del trabajo tiene todos al principio del fichero comentada su función, autor y fecha O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','25','Los ficheros tienen el nombre indicado en la definición de la entrega O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','26','El directorio a entregar existe y tiene el nombre indicado en la entrega O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','27','El alumno evaluado ha indicado el número de horas utilizado en la entrega O');


/*ET2*/



INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','1','Puede ejecutarse y funciona la aplicación al abrir el directorio que la contiene desde el navegador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','2','El diseño mantiene coherencia visual entre los elementos de la página O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','3','El diseño de los formularios es coherente entre los mismos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','4','El diseño de las tablas de muestra de datos es coherente entre las mismas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','5','La página mantiene la estructura de la presentación ante un redimensionamiento del navegador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','6','La página mantiene coherencia entre opciones (tiene siempre la misma opción para la misma acción)');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','7','Los campos de formulario tienen el tamaño de control correcto para el atributo de la tabla que solicitan');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','8','Los campos del formulario tiene el tamaño del dato solicitado correcto para el atributo de la tabla que solicitan');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','9','Todas las acciones están representadas por iconos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','10','Los SHOWALL son claros y visualmente correctos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','11','Las confirmaciones de DELETE son claras y visualmente correctas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','12','Existe una validación correcta por campo de formulario O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','13','Existe una validación por submit que comprueba la validez de todos los campos del formulario antes de enviar al servidor O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','14','Los formularios de SEARCH permiten buscar por todos los campos que se desee');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','15','Los formularios de SEARCH permiten colocar valores parciales en cada campo del formulario (p.e. una parte del dni)');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','16','Los formularios de SEARCH permiten buscar solo por parte de los campos del formulario');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','17','Los formularios de EDIT no permiten modificar los campos clave de la tabla');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','18','En los formularios de ADD son obligatorios los campos NOT NULL de la tabla');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','19','En los formularios de EDIT son obligatorios los campos NOT NULL de la tabla');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','20','Las funciones de código tienen comentario con una descripción antes de su comienzo O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','21','En el código están todas las variables definidas O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','22','En el código están las variables comentadas en su definición O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','23','En el código están comentadas todas las estructuras de control O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','24','Los ficheros del trabajo tiene todos al principio del fichero comentada su función, autor y fecha O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','25','Los ficheros tienen el nombre indicado en la definición de la entrega O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','26','El directorio a entregar existe y tiene el nombre indicado en la entrega O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','27','El alumno evaluado ha indicado el número de horas utilizado en la entrega O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','28','Existen todas las vistas solicitadas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','29','Las vistas son clases');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','30','El modelo de datos es una clase');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','31','El controlador es un script php');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','32','No se accede a la BD desde una vista');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','33','No se envía información al navegador desde el modelo de datos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','34','No se accede a la BD desde el controlador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','35','No se envía información al navegador desde el controlador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','36','La autenticación funciona correctamente');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','37','Realiza correctamente todas las inserciones en la bd');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','38','Realiza correctamente todos los borrados en la bd');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','39','Realiza correctamente todas las modificación en la bd');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','40','Visualiza correctamente todas las vistas en detalle de las entidades de la bd');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','41','Realiza correctamente el registro de un usuario');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','42','Muestra correctamente el resultado de cualquier búsqueda usando una vista SHOWALL');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','43','Realiza correctamente el cambio de idioma de la interfaz');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','44','Estás definidos correctamente todos los strings de idioma para español, ingles y gallego.');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','45','Se visualiza correctamente toda la interfaz de la aplicación en cualquiera de los tres idiomas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','46','Utiliza las acciones definidas para altas, bajas, modificaciones y consultas.');


/*ET3*/

INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','1','Puede ejecutarse y funciona la aplicación al abrir el directorio que la contiene desde el navegador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','2','El diseño mantiene coherencia visual entre los elementos de la página O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','3','El diseño de los formularios es coherente entre los mismos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','4','El diseño de las tablas de muestra de datos es coherente entre las mismas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','5','La página mantiene la estructura de la presentación ante un redimensionamiento del navegador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','6','La página mantiene coherencia entre opciones (tiene siempre la misma opción para la misma acción)');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','7','Los campos de formulario tienen el tamaño de control correcto para el atributo de la tabla que solicitan');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','8','Los campos del formulario tiene el tamaño del dato solicitado correcto para el atributo de la tabla que solicitan');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','9','Todas las acciones están representadas por iconos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','10','Los SHOWALL son claros y visualmente correctos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','11','Las confirmaciones de DELETE son claras y visualmente correctas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','12','Existe una validación correcta por campo de formulario O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','13','Existe una validación por submit que comprueba la validez de todos los campos del formulario antes de enviar al servidor O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','14','Los formularios de SEARCH permiten buscar por todos los campos que se desee');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','15','Los formularios de SEARCH permiten colocar valores parciales en cada campo del formulario (p.e. una parte del dni)');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','16','Los formularios de SEARCH permiten buscar solo por parte de los campos del formulario');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','17','Los formularios de EDIT no permiten modificar los campos clave de la tabla');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','18','En los formularios de ADD son obligatorios los campos NOT NULL de la tabla');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','19','En los formularios de EDIT son obligatorios los campos NOT NULL de la tabla');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','20','Las funciones de código tienen comentario con una descripción antes de su comienzo O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','21','En el código están todas las variables definidas O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','22','En el código están las variables comentadas en su definición O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','23','En el código están comentadas todas las estructuras de control O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','24','Los ficheros del trabajo tiene todos al principio del fichero comentada su función, autor y fecha O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','25','Los ficheros tienen el nombre indicado en la definición de la entrega O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','26','El directorio a entregar existe y tiene el nombre indicado en la entrega O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','27','El alumno evaluado ha indicado el número de horas utilizado en la entrega O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','28','Existen todas las vistas solicitadas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','29','Las vistas son clases');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','30','El modelo de datos es una clase');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','31','El controlador es un script php');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','32','No se accede a la BD desde una vista');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','33','No se envía información al navegador desde el modelo de datos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','34','No se accede a la BD desde el controlador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','35','No se envía información al navegador desde el controlador');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','36','La autenticación funciona correctamente');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','37','Realiza correctamente todas las inserciones en la bd');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','38','Realiza correctamente todos los borrados en la bd');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','39','Realiza correctamente todas las modificación en la bd');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','40','Visualiza correctamente todas las vistas en detalle de las entidades de la bd');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','41','Realiza correctamente el registro de un usuario');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','42','Muestra correctamente el resultado de cualquier búsqueda usando una vista SHOWALL');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','43','Realiza correctamente el cambio de idioma de la interfaz');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','44','Estás definidos correctamente todos los strings de idioma para español, ingles y gallego.');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','45','Se visualiza correctamente toda la interfaz de la aplicación en cualquiera de los tres idiomas');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','46','Utiliza las acciones definidas para altas, bajas, modificaciones y consultas.');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','47','Permite realizar todas los casos de usuario previstos en la definición de la entrega. O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','48','Solo se pueden introducir Ids (códigos Id) de entidades si estas son fuertes y solo en el ADD y en el SEARCH O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','49','Han de aparecer siempre los nombres correspondientes a los Ids que se muestren en cualquier vista (aunque después se envíen al controlador los IDs) para que el usuario no tenga que recordar Ids O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','50','Un usuario no puede ver las acciones sobre las que no tiene permisos en el interfaz');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','51','Un usuario no puede ejecutar las acciones sobre las cuales no tiene permisos en el controlador.');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','52','Puede crearse un grupo nuevo con permisos nuevos y asignarse usuarios en ese grupo.');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','53','Un usuario en el grupo nuevo puede acceder a las acciones de funcionalidades que se le asignen como permisos');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','54','Existe un manual de usuario del proyecto O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','55','Cada caso de uso del proyecto existe una entrada en el manual de usuario O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','56','Puede accederse a la aplicación con un usuario y password proporcionado por los creadores del proyecto O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','57','Existe un documento para cada reunión de seguimiento O');
INSERT INTO HISTORIA(IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','58','En las reuniones de seguimiento está reflejado el estado del proyecto, el balance entre planificado y proyecto para cada semana en cuanto al coste, el alcance y el tiempo O');

/******************************************************************************/
-- Acciones asignadas a Funcionalidades:
-- Sobre Entregas
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('0', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('0', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('0', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('0', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('0', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('0', '5');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('0', '9');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('0', '10');


-- Sobre QAS
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('1', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('1', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('1', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('1', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('1', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('1', '5');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('1', '9');


-- Sobre Calificaciones
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('2', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('2', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('2', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('2', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('2', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('2', '5');


-- Sobre Usuarios
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('3', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('3', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('3', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('3', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('3', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('3', '5');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('3', '6');


-- Sobre Grupos de Usuario
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('4', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('4', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('4', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('4', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('4', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('4', '5');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('4', '7');

-- Sobre Funcionalidades
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('5', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('5', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('5', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('5', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('5', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('5', '5');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('5', '8');


-- Sobre Acciones
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('6', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('6', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('6', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('6', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('6', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('6', '5');


-- Sobre Permisos
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('7', '2');

-- Sobre Trabajos
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('8', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('8', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('8', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('8', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('8', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('8', '5');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('8', '11');


-- Sobre Historias de usuario
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('9', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('9', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('9', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('9', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('9', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('9', '5');


-- Sobre Evaluacion QAS
INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('10', '0');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('10', '1');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('10', '2');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('10', '3');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('10', '4');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('10', '5');

INSERT INTO FUNC_ACCION(IdFuncionalidad, IdAccion)
  VALUES ('10', '10');


/******************************************************************************/
-- Permisos de los grupos para acceder a unas funcionalidades y realizar determinadas acciones
-- Grupo Admin (Por defecto en la creación, tiene permisos para todo)
-- Sobre Entregas
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '0', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '0', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '0', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '0', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '0', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '0', '5');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '0', '9');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '0', '10');


-- Sobre QAS
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '1', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '1', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '1', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '1', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '1', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '1', '5');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '1', '9');


-- Sobre Calificaciones
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '2', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '2', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '2', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '2', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '2', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '2', '5');


-- Sobre Usuarios
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '3', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '3', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '3', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '3', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '3', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '3', '5');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '3', '6');


-- Sobre Grupos de Usuario
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '4', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '4', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '4', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '4', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '4', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '4', '5');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '4', '7');

-- Sobre Funcionalidades
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '5', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '5', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '5', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '5', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '5', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '5', '5');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '5', '8');


-- Sobre Acciones
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '6', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '6', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '6', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '6', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '6', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '6', '5');


-- Sobre Permisos
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '7', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '7', '2');

-- Sobre Trabajos
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '8', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '8', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '8', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '8', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '8', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '8', '5');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '8', '11');


-- Sobre Historias de usuario
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '9', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '9', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '9', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '9', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '9', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '9', '5');


-- Sobre Evaluacion QAS
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '10', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '10', '1');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '10', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '10', '3');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '10', '4');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '10', '5');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('1', '10', '10');


-- Grupo Alumnnos
-- Sobre Entregas
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '0', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '0', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '0', '5');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '0', '9');


-- Sobre QAS
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '1', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '1', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '1', '5');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '1', '9');


-- Sobre Calificaciones
INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '2', '0');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '2', '2');

INSERT INTO PERMISO(IdGrupo, IdFuncionalidad, IdAccion)
  VALUES ('0', '2', '5');
