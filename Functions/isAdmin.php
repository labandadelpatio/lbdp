<?php
/* Fichero que contiene la funcion isAdmin()
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017
*/

/*  function isAdmin()
*     Esta función comprueba si el login que está conectado a la aplicación
*     es un Administrador o un Usuario, si es un Administrador devuelve true
*     y si es usuario devuelve false
*/
function isAdmin($login){

  include_once '../Models/USERSGROUP_MODEL.php';

  $control = new USERSGROUP_MODEL(0, $login); //creamos un objeto del modelo de datos
  $resultado = $control->comprobarControlAccesoAdmin(); //y le aplicmaos la funcion comprobarControlAcceso Admin

  if ( $resultado == true ){ // Si el usuario conectado tiene permisos de Administrador
      return true;
  }
  else{ // Si el usuario conectado no tiene permisos de Administrador
      return false;
  }
}
?>
