<?php
/* Fichero que contiene una funcion la cual comprueba si una funcionalidad puede
*   realizar alguna de las acciones que agregamos a la bd.
*  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017
*/

/*  tieneAccion()
*   Devuelve true si la funcionalidad a la que accedemos puede realiar una accion
*   en concreto
*/
function tieneAccion($idfuncionalidad, $idaccion){

    include_once '../Models/FUNCACC_MODEL.php';

    $FUNCACTION = new FUNCACC_MODEL($idfuncionalidad, $idaccion); //creamos un objeto del modelo de dtaos
    $resultado = $FUNCACTION->rAccion(); //le aplicamos la función rAccion()

    return $resultado; //devolvemos el resultado de la aplicación de la funcion anterior sobre la variable FUNACTION

}

?>
