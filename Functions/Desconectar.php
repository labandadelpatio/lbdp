<!--
Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017
En este fichero se establecen las sentencias para desconectarse de la sesión iniciada por el usuario
-->

<?php

session_start(); //iniciamos la sesion
session_destroy(); //finalizamos la sesion
header('Location:../index.php');

?>
