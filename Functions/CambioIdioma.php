<!--
Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017
En este fichero se utiliza para el cambio de idioma
-->

<?php
session_start(); //solicitamos trabajar con la sesión
$idioma = $_POST['idioma']; //almacenamos el idioma seleccionado
$_SESSION['idioma'] = $idioma; //el idioma almacenado será el de la sesion
header('Location:' . $_SERVER["HTTP_REFERER"]);  //envía el idioma al navegador
?>