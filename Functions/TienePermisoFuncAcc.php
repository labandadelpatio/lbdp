<?php
/* Fichero que contiene la funcion tienePermisoFuncAcc
*   Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
   Fecha: 11/12/2017
*/

/*  function tienePermisoFuncAcc()
*     Esta función comprueba a que grupo pertenece el usuario conectado y si
*     ese grupo tiene permisos para una determinada funcionalidad.
*/
function TienePermisoFuncAcc($login, $idfuncionalidad, $idaccion){

  include_once '../Models/USERSGROUP_MODEL.php';
  $control = new USERSGROUP_MODEL( 0, $login ); // Si hay un 0 es que no necesitamos ese dato en la funcion
  $resultado = $control->comprobarGrupo();

  while($almacena = $resultado->fetch_array()){ // Si el usuario está en varios grupos este bucle los recorre
    $idgrupo = $almacena[1];
    include_once '../Models/PERMISSIONS_MODEL.php';
    $PERMISSION = new PERMISSIONS_MODEL( $idgrupo, $idfuncionalidad, $idaccion ); // Si hay un 0 es que no necesitamos ese dato en la funcion
    $resultado2 = $PERMISSION->comprobarPermisosFuncAcc();
    if($resultado2){ // Si tiene permiso en alguno de los grupos a los que pertenece
      return true;
    }
  }
  return false; // Si sale del bucle es por que no tiene permiso
}
?>
