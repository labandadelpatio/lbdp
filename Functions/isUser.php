<?php

/*  Fichero para comprobar si el usuario no es administrador
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/

//mediante sta funcion comprobaremos si el usurio conectado es o no el Admin

function isUser($login){


	if($login == $_SESSION['login']){ //almacenamos el login del usuario que tiene inicida la sesión
		return true; 
	}


	return false;
}
?>