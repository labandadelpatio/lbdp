<?php
/* Fichero que contiene una funcion la cual comprueba si una USUARIO pertenece
*   a alguno de los grupos que agregamos a la bd.
*  Fecha de creacion 06/12/2017
* Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
*/

/*  tieneUser()
*   Devuelve true si el usuario pertenece a ese grupo en concreto
*   en concreto
*/
function tieneUser($idgrupo, $login){

    include_once '../Models/USERSGROUP_MODEL.php';

    $USERSGROUP = new USERSGROUP_MODEL($idgrupo, $login); //creamos un nuevo objeto del modelo de dtaos
    $resultado = $USERSGROUP->rUser(); //e aplicamos a función rUser

    return $resultado; //almacenamos el resulado 

}

?>
