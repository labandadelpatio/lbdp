<?php
/* Fichero que contiene una funcion la cual comprueba obtiene la ruta de un trabajo
*  pasandole el id de la entrega, el login y alias
* Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017
*/

/*  obtenerRuta()
*   Obtiene la ruta para un trabajo
*   en concreto
*/
function obtenerRuta($identrega, $alias){

    include_once '../Models/DELIVERABLES_MODEL.php';

    $ENTREGA = new DELIVERABLES_MODEL($identrega, '', $alias,'',''); //creamos un nuevo objeto del modelo de dtaos
    $resultado = $ENTREGA->SEARCHRUTA(); //e aplicamos a función rUser

    return $resultado; //almacenamos el resulado

}

?>
