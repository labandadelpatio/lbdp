<?php
/* Fichero que contiene una funcion la cual comprueba si una funcionalidad puede
*   realizar alguna de las acciones que agregamos a la bd.
*  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 11/12/2017
*/

/*  tieneAccion()
*   Devuelve true si la funcionalidad a la que accedemos puede realiar una accion
*   en concreto
*/
function tienePermiso($idgrupo, $idfuncionalidad, $idaccion){
    include_once '../Models/PERMISSIONS_MODEL.php';
    $FUNCACTION = new PERMISSIONS_MODEL($idgrupo, $idfuncionalidad, $idaccion);
    $resultado = $FUNCACTION->rPermiso();

    return $resultado;

}

?>
