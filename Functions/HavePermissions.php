<?php
/* Fichero que realiza la funciones de comprobación de permisos
*  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017
*/

/*
*   function HavePermissions()
*   Función que devuelve true si el usuario que está conectado puede acceder a
*   una determinada acción de una funcionalidad.
*
*/
function HavePermissions( $login, $funcionalidad, $accion ){

  $login = $_SESSION['login']; // Obtenemos el login.
  /* Opcion 1, pasando por controlador
  $resultado = header("AccesControlList_Controller.php?funcionalidad=" + $funcionalidad + "&&accion=" + $accion );
  */
  include '../Models/PERMISSIONS_MODEL.php';
  $control = new PERMISSIONS_MODEL();
  $resultado = $control->comprobarControlAccesoAcciones( $login, $funcionalidad, $accion );

  if($resultado == true){ // Si tiene acceso a una acción de una funcionalidad
      return true;
  }
  else{
      return false; // Si tiene acceso a una acción de una funcionalidad
  }
}
?>
