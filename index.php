<!--
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 27/11/2017
En este fichero está el index. Si los datos en la autenticación son correctos te envía a la vista priniciapl, en caso contrario pasas por el controlador de login( el cual te mostrará un mensaje de error)
-->

<?php
//entrada a la aplicacion

//se va usar la session de la conexion

session_start(); 

//funcion de autenticacion
include './Functions/Authentication.php';

//si no ha pasado por el login de forma correcta
if (!IsAuthenticated()){
	header('Location:./Controllers/Login_Controller.php');
}
//si ha pasado por el login de forma correcta 
else{
	header('Location:./Controllers/Index_Controller.php');
}


?>
