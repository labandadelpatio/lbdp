<?php
/*  Fichero para el controlador de los grupos de usuarios
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/
session_start(); //solicito trabajar con la session

if (!isset($_REQUEST['action'])){  //comprobamos que la acción no sea nula y esté definida
	$_REQUEST['action'] = '';
	$_REQUEST['action'] = '';
}

//incluimos todos los ficheros que vamos a usar

include '../Views/Groups_Views/GROUPS_SHOWALL.php';
include '../Views/Groups_Views/GROUPS_ADD.php';
include '../Views/Groups_Views/GROUPS_SEARCH.php';
include '../Views/Groups_Views/GROUPS_SHOWCURRENT.php';
include '../Views/Groups_Views/GROUPS_DELETE.php';
include '../Views/Groups_Views/GROUPS_EDIT.php';
include '../Views/Groups_Views/GROUPS_ASSIGN.php';
include '../Views/MESSAGE.php';
include '../Models/GROUPS_MODEL.php';
include '../Models/USERS_MODEL.php';

//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $GROUPS 

	function get_data_form(){

		$idgrupo = $_REQUEST['idgrupo']; //mediante la variable $idgrupo almacenamos el dato de que viene del post del campo idgrupo
		$nombregrupo = $_REQUEST['nombregrupo']; //mediante la variable $nombregrupo almacenamos el dato de que viene del post del campo nombregrupo
		$descripgrupo = $_REQUEST['descripgrupo']; //mediante la variable $descrpgrupo almacenamos el dato de que viene del post del campo descripgrupo

		$GROUPS = new GROUPS_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$idgrupo,
			$nombregrupo,
			$descripgrupo
		   );

		return $GROUPS;
	}


//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo

Switch ($_REQUEST['action']){  //Comprobamos que accion viene por el POST

		case 'ADD': //si la accion es ADD

			if (!$_POST){//si no viene del POST

				new GROUPS_ADD(); //se creará una nueva vista de ADD de GROUPS

			}

			else //si vienen del POST
			{

				$modelo = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form


				$respuesta = $modelo->ADD(); //se aplicará la función de ADD en dichos datos, almacenando el resultado en la variable
				new MESSAGE($respuesta, './Groups_Controller.php'); //se moestrará un mensaje en función del resultado de la inserción
			

			}

			break;



		case 'DELETE'://si la acción recibida es DELETE

			if (!$_POST){ //los datos no vienen del POST

				$GROUPS = new GROUPS_MODEL($_REQUEST['idgrupo'], '', ''); //creamos un objeto del modelo de datos que recoja los valores

				$valores = $GROUPS->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
				new GROUPS_DELETE($valores);  //se mostrará una vista de delete con los valores recogido

			}

			else{

	     		$GROUPS = new GROUPS_MODEL($_REQUEST['idgrupo'], '', ''); //creamos un objeto del modelo de datos que recoja los valores
				$respuesta = $GROUPS->DELETE(); //y les aplicamos la función de DELETE almacenanado el resultado en esta variable
				new MESSAGE($respuesta, './Groups_Controller.php'); 
			}
			break;




		case 'EDIT': //si la acción recibida es DELETE	

			if (!$_POST){//si no viene del POST


				$GROUPS = new GROUPS_MODEL($_REQUEST['idgrupo'], '', ''); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $GROUPS->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
				new GROUPS_EDIT($valores); //se mostrará una vista de edit con los valores recogidos
			}
			else{

				$GROUPS = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form

				$respuesta = $GROUPS->EDIT(); //y les aplicamos la función edit
				new MESSAGE($respuesta, './Groups_Controller.php'); //se mostrará un mensaje en función del resultado de la edición

			}

			break;


		case 'SEARCH': //si la acción recibida es SEARCH
			if (!$_POST){ //si los datos no vienen del POST
				new GROUPS_SEARCH();  //se creará una nueva vista de SEARCH
			}
			else{
				$GROUPS = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form

				$datos = $GROUPS->SEARCH();  //y les aplicamos la función search
				$lista = array('','',''); //creamos un array que se guardará en esta variable
				new GROUPS_SHOWALL($lista, $datos, '../index.php'); //se mostrará una nueva vista de SHOWALL con los resultado de la busqueda
			}

			break;




		case 'SHOWCURRENT': //si la acción recibida es SHOWCURRENT

			$GROUPS = new GROUPS_MODEL($_REQUEST['idgrupo'], '', ''); //creamos un objeto del modelo de datos que recoja los valores
			$valores = $GROUPS->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
			new GROUPS_SHOWCURRENT($valores); //se creará una nueva vista con los valores pertenecientes a la tuple seleccionada
			break;

        

		default: //la acción por defecto será la vista de SHOWALL

			if (!$_POST){ //si no viene del POST

				$GROUPS = new GROUPS_MODEL('','', ''); //creamos un objeto del modelo de datos que recoja los valores
			}
			else{

				$GROUPS = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form

			}

			$datos = $GROUPS->SEARCH(); //y les aplicamos la función search
			$lista = array('ID Grupo','Nombre Grupo','Descripcion Grupo'); //creamos un array que se guardará en esta variable
			new GROUPS_SHOWALL($lista, $datos);

}

 ?>
