<!--
Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
Fecha: 23/12/2017
En este fichero esta el controlador de index, da paso al usuario a la vista princiapl si los datos con los que se logea son correctos
-->

<?php
//session
session_start();
//incluir funcion autenticacion
include '../Functions/Authentication.php';
//si no esta autenticado
if (!IsAuthenticated()){
	header('Location: ../index.php');
}
//esta autenticado
else{
	include '../Views/Users_Views/USERS_Index.php';
	new Index();
}

?>