
<?php

/* Fichero para el controlador de asignar acciones a funcionalidades
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/
session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){
	$_REQUEST['action'] = '';
}

//incluimos todos los ficheros que vamos a usar

include '../Views/Functionalities_Views/FUNCTIONALITIES_ASSIGN.php';
include '../Views/MESSAGE.php';
include_once '../Models/FUNCACC_MODEL.php';
include '../Models/ACTION_MODEL.php';
include '../Models/FUNCTIONALITIES_MODEL.php';

//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $FUNCACC
	function get_data_form(){

		$idfuncionalidad = $_REQUEST['idfuncionalidad']; //variable que recoge el valor que viene del POST de idfuncionalidad
		$idaccion = $_REQUEST['idaccion']; //variable que recoge el valor que viene del POST de idaccion

		$FUNCACC = new FUNCACC_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$idfuncionalidad,
			$idaccion
		 );

		return $FUNCACC;
	}

//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo

Switch ($_REQUEST['action']){ //Comprobamos que accion viene por el POST


		case 'ASSIGN': //si la accion es assign

			if (!isset($_REQUEST['idaccion'])){ //comprobamos que la accion este definida y no sea nula

				$FUNCTIONALITIES = new FUNCTIONALITIES_MODEL($_REQUEST['idfuncionalidad'], '', ''); //creamos un nuevo obejto del modelo de datos
				$valores = $FUNCTIONALITIES->RellenaDatos(); //obtenemos los datos y los almacenamos en esta variable

				$ACCIONES = new ACTION_MODEL('','',''); //creamos un nuevo objeto del modelo de datos
        		$resultado = $ACCIONES->SEARCH(); //almacenamos las acciones en la variable resultado

				$lista = array('IdAccion','nombreaccion','descriptAcion'); //creamos un array que almacene estos campos

			  new FUNCTIONALITIES_ASSIGN($valores, $lista, $resultado); //se creará un nevo obejeto funcionalities_Assing
			}
			else{ 

				$idfun = $_REQUEST['idfuncionalidad']; //cogemos el valor de la funcionalidad que viene del POST
				$idacc = $_REQUEST['idaccion']; //cogemos el valor de la accion que viene del POST
				$FUNCACTION = new FUNCACC_MODEL($idfun, $idacc); //creamos un nuevo objeto del modelo de datos
				$FUNCACTION->ADD(); //y le aplicamos la función ADD

				header("Location:../Controllers/FuncAcc_Controller.php?action=ASSIGN&idfuncionalidad=$idfun");
			}
			break;

			case 'DESASSIGN': //si la accion es dessasing
			$idfun = $_REQUEST['idfuncionalidad']; //almacenamos los datos que viene del post del campo idfuncionalidad
			$idacc = $_REQUEST['idaccion']; //almacenamos los datos que viene del post del campo idaccion
			$FUNCACTION = new FUNCACC_MODEL($idfun, $idacc); //se creará un nuevo objeto del modelo de datos 
			$FUNCACTION->DELETE(); //a tal objeto le aplicamos la función de delete

			header("Location:../Controllers/FuncAcc_Controller.php?action=ASSIGN&idfuncionalidad=$idfun");
			break;

		default: //el caso por defecto es

			if (!$_POST){ //si la accion no viene del POST
				$FUNCACC = new FUNCACC_MODEL('',''); //se moestrara un nuevo objeto del modelo de datos
			}
			else{
				$FUNCACC = get_data_form(); //recogemos los datos si viene del POST
			}
			$datos = $FUNCACC->SEARCH(); //almacenamos todas los datos de la tabla FUNACC
			$lista = array('idfuncionalidad','idaccion'); //esta variale almacenará un array con el idfuncionalidad y el idaccion
			new FUNCACC_SHOWALL($lista, $datos); //se mostrará un nueva vista de showall

}






 ?>
