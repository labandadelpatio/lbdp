<?php 
/*  Fichero para el controlador de permisos
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/

session_start(); //solicito trabajar con la session

 

if (!isset($_REQUEST['action'])){ //comprobamos que la acción no sea nula y esté definida
	$_REQUEST['action'] = '';
}

//incluimos todos los ficheros que vamos a usar

include '../Views/Permissions_Views/PERMISSIONS_SHOWALL.php';
include '../Views/Permissions_Views/PERMISSIONS_SEARCH.php';
include '../Views/Groups_Views/GROUPS_ASSIGN.php';
include '../Views/MESSAGE.php';
include '../Models/PERMISSIONS_MODEL.php';
include '../Models/GROUPS_MODEL.php';
include '../Models/FUNCTIONALITIES_MODEL.php';
include '../Models/ACTION_MODEL.php';
include '../Models/FUNCACC_MODEL.php';

//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $PERMISSIONS

	function get_data_form(){

		$idfuncionalidad = $_REQUEST['IdFuncionalidad']; //mediante la variable $idduncionalidad almacenamos el dato de que viene del post del campo idfuncionalidad
		$idaccion = $_REQUEST['IdAccion']; //mediante la variable $idaccion almacenamos el dato de que viene del post del campo idaccion
		$idgrupo = $_REQUEST['IdGrupo'];//mediante la variable $idgrupo almacenamos el dato de que viene del post del campo IdGrupo
		$action = $_REQUEST['action']; //mediante la variable $action almacenamos el dato de que viene del post del campo action

		$PERMISSIONS = new PERMISSIONS_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$idfuncionalidad, 
		    $idaccion,
			$idgrupo );

		return $PERMISSIONS;
	}


//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo

Switch ($_REQUEST['action']){ //Comprobamos que accion viene por el POST


		case 'ASSIGN':
			if (!(isset($_REQUEST['idfuncionalidad']) && isset($_REQUEST['idaccion']))){

				$GRUPO = new GROUPS_MODEL($_REQUEST['idgrupo'], '', '');
				$valores = $GRUPO->RellenaDatos();

				$FUNCACC = new FUNCACC_MODEL('','');
        		$resultado = $FUNCACC->RellenaDatosNombres();

				$lista = array('idfuncionalidad','idaccion','nombrefuncionalidad','nombreaccion');

			  new GROUPS_ASSIGN($valores, $lista, $resultado);
			}
			else{
				$idgrup = $_REQUEST['idgrupo'];
				$idfun = $_REQUEST['idfuncionalidad'];
				$idacc = $_REQUEST['idaccion'];
				$permiso = new PERMISSIONS_MODEL($idgrup, $idfun, $idacc);
				$permiso->ADD();

				header("Location:../Controllers/Permissions_Controller.php?action=ASSIGN&idgrupo=$idgrup");
			}
			break;

			case 'DESASSIGN':
			$idgrup = $_REQUEST['idgrupo'];
			$idfun = $_REQUEST['idfuncionalidad'];
			$idacc = $_REQUEST['idaccion'];
			$permiso = new PERMISSIONS_MODEL($idgrup, $idfun, $idacc);
			$permiso->DELETE();

			header("Location:../Controllers/Permissions_Controller.php?action=ASSIGN&idgrupo=$idgrup");
			break;


		case 'SEARCH':
			if (!$_POST){
				new PERMISSIONS_SEARCH();
			}
			else{
				$PERMISSIONS = get_data_form();
				$datos = $PERMISSIONS->RellenaDatosNombres();
				$lista = array('','','','','','');
				new PERMISSIONS_SHOWALL($lista, $datos, '../index.php');
			}
			break;





		default:
			if (!$_POST){
				$PERMISSIONS = new PERMISSIONS_MODEL('','','');
			}
			else{
				$PERMISSIONS = get_data_form();
			}
			$datos = $PERMISSIONS->RellenaDatosNombres();
			$lista = array('IdGrupo', 'NombreGrupo', 'IdFuncionalidad', 'NombreFuncionalidad', 'IdAccion','NombreAccion');
			new PERMISSIONS_SHOWALL($lista, $datos);		

}






 ?>