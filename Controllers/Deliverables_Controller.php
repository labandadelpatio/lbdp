<?php
/*  Fichero para el controlador de las entregas
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/


session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){
	$_REQUEST['action'] = '';
}


//incluimos los distintos ficheros que vamos a utilizar

include '../Views/Deliverables_Views/DELIVERABLES_SHOWALL.php';
include '../Views/Deliverables_Views/DELIVERABLES_ADD.php';
include '../Views/Deliverables_Views/DELIVERABLES_SEARCH.php';
include '../Views/Deliverables_Views/DELIVERABLES_SHOWCURRENT.php';
include '../Views/Deliverables_Views/DELIVERABLES_DELETE.php';
include '../Views/Deliverables_Views/DELIVERABLES_EDIT.php';
include '../Views/MESSAGE.php';
include '../Models/DELIVERABLES_MODEL.php';
include '../Models/JOB_MODEL.php';
include '../Views/Jobs_Views/JOB_SHOWALL.php';
include '../Views/Jobs_Views/JOB_SHOWDELIVERABLES.php';

//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $DELIVERABLE

	function get_data_form(){

		$meus = $_FILES['ruta']['name'];  //en esta variable amacenaremos el nombre original del fichero en la maquina cliente

		 if (!(strpos($meus, "sh")) && !(strpos($meus, "php"))){ //comprobamoms que el archivo subido sea solo rar o zip
		$nombre = $_FILES['ruta']['name']; //en esta variable almacenamos el nombre de la ruta
		$original = $_FILES['ruta']['tmp_name']; //en esta variable almacenamos el nombre temporal del fichero en la maquina srvidos
		$directorio = "../Files/"; //en esta variable almacenamos la ruta del directorio en el que queremos guardar el fichero
		$uploadfile = $directorio . $nombre; //en esta variable almacenamos la ruta final del fichero una vez subido al servidor
}

		$idtrabajo = $_REQUEST['idtrabajo']; //mediante la variable $idtrabajo almacenamos el dato de que viene del post del campo idtrabajo
		$login = $_REQUEST['login']; //mediante la variable $login almacenamos el dato de que viene del post del campo login
		$alias = $_REQUEST['alias']; //mediante la variable $alias almacenamos el dato de que viene del post del campo alias
		$horas = $_REQUEST['horas']; //mediante la variable $horas almacenamos el dato de que viene del post del campo horas
		$action = $_REQUEST['action']; //mediante la variable $action almacenamos el dato de que viene del post del campo actiom

		$DELIVERABLE = new DELIVERABLES_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$idtrabajo,
			$login,
			$alias,
			$horas,
			$uploadfile,
			$action);

		return $DELIVERABLE;
	}


//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo


switch ($_REQUEST['action']){ //Comprobamos que accion viene por el POST

	case 'ADD': //si la accion es ADD

			if (!$_POST){ //si no viene del POST

				new DELIVERABLES_ADD(); //se creará una nueva vista de ADD de DELIVERABLES

			}
			else{//si vienen del POST

				$DELIVERABLE = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form
				$respuesta = $DELIVERABLE->ADD(); //aplicamos a esos datos la funcion de ADD

        if($respuesta == 'Inserción realizada con éxito'){ //si la inserción se ha realizado correctamente

						$nombre = $_FILES['ruta']['name']; //en esta variable amacenaremos el nombre original del fichero en la maquina cliente
						$original = $_FILES['ruta']['tmp_name']; //en esta variable almacenamos el nombre temporal del fichero en la maquina srvidos
						$directorio = "../Files/"; //en esta variable almacenamos la ruta del directorio en el que queremos guardar el fichero
						$uploadfile = $directorio . $nombre; //en esta variable almacenamos la ruta final del fichero una vez subido al servidor
						move_uploaded_file($original, $uploadfile); //cambiamos la ruta de nuestro archivo subido
				}

				new MESSAGE($respuesta, './Deliverables_Controller.php'); //se mostrará un mensaje en función del resultado de la inserción

			}
			break;

		case 'DELETE'://si la acción recibida es DELETE

			if (!$_POST){ //los datos no vienen del POST

				$DELIVERABLE = new DELIVERABLES_MODEL($_REQUEST['idtrabajo'], $_REQUEST['login'], '','','');  //creamos un objeto del modelo de datos que recoja los valores
				$valores = $DELIVERABLE->RellenaDatos();  //le aplicamos la función RellenaDatos a estos valores
				new DELIVERABLES_DELETE($valores); //se creará una vista de delete con los valores previamente recogidos
			}
			else{
				$DELIVERABLE = new DELIVERABLES_MODEL($_REQUEST['idtrabajo'],  $_REQUEST['login'], '','','');  //creamos un objeto del modelo de datos que recoja los valores
				$respuesta = $DELIVERABLE->DELETE(); //y les aplicamos la función de DELETE almacenanado el resultado en esta variable
				new MESSAGE($respuesta, './Deliverables_Controller.php'); //se mostrará una vista de delete con los valores recogidos
			}
			break;




		case 'EDIT': //si la acción recibida es EDIT

			if (!$_POST){//si no viene del POST

				include_once '../Functions/generarAlias.php';

				$DELIVERABLE = new DELIVERABLES_MODEL($_REQUEST['idtrabajo'], $_SESSION['login'],getRandomCode('alias'),'',''); //creamos un objeto del modelo de datos que recoja los valores

				if ($DELIVERABLE->existe()){ //Comrpueba si este usuario ha accedido ya  a la entrega para asignarle un alias o no, si le asignamos el alias

					$valores = $DELIVERABLE->RellenaDatos();  //le aplicamos la función RellenaDatos a estos valores
				}
				else{ //sino

					$valores= $DELIVERABLE->ADD(); //se aplicará la función de ADD en dichos datos, almacenando el resultado en la variable
					$valores = $DELIVERABLE->RellenaDatos();  //le aplicamos la función RellenaDatos a estos valores
				}

				new DELIVERABLES_EDIT($valores); //se mostrará una nueva vista de edit con los valores almacenados
			}

			else{ //si viene del POST

				$DELIVERABLE = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form
				$respuesta = $DELIVERABLE->EDIT(); //y les aplicamos la función edit
				new MESSAGE($respuesta, './Deliverables_Controller.php'); //se mostrará un mensaje en función del resultado de la edición

				if($respuesta == 'Modificado correctamente'){ //si no ha habido fallos en la edición

						$nombre = $_FILES['ruta']['name']; //en esta variable amacenaremos el nombre original del fichero en la maquina cliente
						$original = $_FILES['ruta']['tmp_name']; //en esta variable almacenamos el nombre temporal del fichero en la maquina srvidos
						$directorio = "../Files/"; //en esta variable almacenamos la ruta del directorio en el que queremos guardar el fichero
						$uploadfile = $directorio . $nombre; //en esta variable almacenamos la ruta final del fichero una vez subido al servidor
						move_uploaded_file($original, $uploadfile); //cambiamos la ruta de nuestro archivo subido

				}

			}

			break;

		case 'SEARCH': //si la acción recibida es SEARCH

			if (!$_POST){ //si los datos no vienen del POST

				new DELIVERABLES_SEARCH(); //se creará una nueva vista de SEARCH
			}

			else{
				$DELIVERABLE = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form
				$datos = $DELIVERABLE->SEARCH();  //y les aplicamos la función search
				$lista = array('ID Trabajo',' Login','Alias', 'Horas','Ruta'); //creamos un array que se guardará en esta variable
				new DELIVERABLES_SHOWALL($lista, $datos, '../index.php'); //se mostrará una nueva vista de SHOWALL con los resultado de la busqueda

			}

			break;



			case 'SHOWCURRENT': //si la acción recibida es SHOWCURRENT

			$DELIVERABLE = new DELIVERABLES_MODEL($_REQUEST['idtrabajo'],  $_REQUEST['login'], '','','');  //creamos un objeto del modelo de datos que recoja los valores
			$valores = $DELIVERABLE->RellenaDatos();  //le aplicamos la función RellenaDatos a estos valores
			new DELIVERABLES_SHOWCURRENT($valores);
			break;



			case 'SHOWALL': //si la accion recibida es SHOWALL

				if (!$_POST){ //y los datos no vienen del POST
				$JOB = new DELIVERABLES_MODEL('','', '','','');  //creamos un objeto del modelo de datos que recoja los valores
			}
			else{
				$JOB = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form
			}
			$datos = $JOB->SEARCH(); //y les aplicamos la función search
			$lista = array('Login','ID Trabajo','Nombre Trabajo','Login Evaluado','Horas','Ruta'); //creamos un array que se guardará en esta variable
			new DELIVERABLES_SHOWALL($lista, $datos); //se creará una nueva vista con los valores pertenecientes a la tuple seleccionada
	

			break;


		default:
				if (!$_POST){ //la acción por defecto será la vista de SHOWALl de JOBS

				$JOB = new JOB_MODEL('','', '','','');  //creamos un objeto del modelo de datos que recoja los valores
			}
			else{
				$JOB = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form
			}
			$datos = $JOB->SEARCHDELIVERABLES(); //le aplicamos la funcion SEARCHDELIVERABLES para que muestre solo las entregas
			$lista = array('ID Trabajo','Nombre Trabajo','Fecha Inicio','Fecha Fin','Porcentaje Nota'); //creamos un array que se guardará en esta variable
			new JOB_SHOWDELIVERABLE($lista, $datos); //se nos mostra una tabla con todas las DELIVERABLES.

}






 ?>
