
<?php
/*  Fichero para el controlador de la evaluación de qas
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/
session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){ //comprobamos que la acción no sea nula y esté definida
	$_REQUEST['action'] = '';
}

//incluimos todos los ficheros que vamos a usar
include '../Views/EvaluationQAS_Views/EVALUATION_SHOWALL.php';
include '../Views/EvaluationQAS_Views/EVALUATIONQAS_SHOWALL.php';
include '../Views/EvaluationQAS_Views/EVALUATIONQAS_ADD.php';
include '../Views/EvaluationQAS_Views/EVALUATIONQAS_SEARCH.php';
include '../Views/EvaluationQAS_Views/EVALUATIONQAS_SHOWCURRENT.php';
include '../Views/EvaluationQAS_Views/EVALUATIONQAS_DELETE.php';
include '../Views/EvaluationQAS_Views/EVALUATIONQAS_EDIT.php';
include '../Views/MESSAGE.php';
include '../Models/EVALUATION_MODEL.php';
include '../Models/QAASIGNMENT_MODEL.php';
//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $EVALUATION

	function get_data_form(){

		$idtrabajo = $_REQUEST['idtrabajo'];  //mediante la variable $idtrabajo almacenamos el dato de que viene del post del campo idtrabajo
		$loginEvaluador = $_REQUEST['loginEvaluador']; //mediante la variable $loginEvaluador almacenamos el dato de que viene del post del campo loginEvaluador
		$aliasEvaluado = $_REQUEST['aliasEvaluado']; //mediante la variable $aliasEvaluado almacenamos el dato de que viene del post del campo aliasEvaluado
		$idhistoria = $_REQUEST['idhistoria']; //mediante la variable $idhistoria almacenamos el dato de que viene del post del campo idhistoria
		if(!isset($_REQUEST['correctoA'])) $correctoA = '';
		else $correctoA = $_REQUEST['correctoA']; //mediante la variable $correctoA almacenamos el dato de que viene del post del campo correctoA
		if(!isset($_REQUEST['comenIncorrectoA'])) $comenIncorrectoA = '';
    else $comenIncorrectoA = $_REQUEST['comenIncorrectoA']; //mediante la variable $cometIncorrectoA almacenamos el dato de que viene del post del campo comentIncorrectoA
		if(!isset($_REQUEST['correctoP'])) $correctoP = '';
		else $correctoP = $_REQUEST['correctoP']; //mediante la variable $correctoP almacenamos el dato de que viene del post del campo correctoP
		if(!isset($_REQUEST['comenIncorrectoP'])) $comentIncorrectoP = '';
		else $comentIncorrectoP = $_REQUEST['comenIncorrectoP']; //mediante la variable $cometIncorrectoP almacenamos el dato de que viene del post del campo comentIncorrectoP
		if(!isset($_REQUEST['ok'])) $Ok = '';
		else $Ok = $_REQUEST['ok']; //mediante la variable $ok almacenamos el dato de que viene del post del campo ok

		$action = $_REQUEST['action']; //mediante la variable $action almacenamos el dato de que viene del post del campo action

		$EVALUATION = new EVALUATION_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$idtrabajo,
			$loginEvaluador,
			$aliasEvaluado,
			$idhistoria,
			$correctoA,
			$comenIncorrectoA,
			$correctoP,
			$comentIncorrectoP,
			$Ok,
			$action
		   );

		return $EVALUATION;
	}


//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo

Switch ($_REQUEST['action']){ //Comprobamos que accion viene por el POST

	case 'ADD': //si la accion es ADD
			if (!$_POST){//si no viene del POST
				new EVALUATIONQAS_ADD(); //se creará una nueva vista de ADD de EVALUATIONSQA
			}
			else{
				$modelo = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form


				$respuesta = $modelo->ADD(); //se aplicará la función de ADD en dichos datos, almacenando el resultado en la variable
				new MESSAGE($respuesta, './EvaluationQAS_Controller.php'); //se moestrará un mensaje en función del resultado de la inserción

			}
			break;



		case 'DELETE'://si la acción recibida es DELETE

			if (!$_POST){ //los datos no vienen del POST

				$EVALUATION= new EVALUATION_MODEL($_REQUEST['idtrabajo'],$_REQUEST['loginEvaluador'],$_REQUEST['aliasEvaluado'],$_REQUEST['idhistoria'],'','','','',''); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $EVALUATION->RellenaDatos();//le aplicamos la función RellenaDatos a estos valores
				new EVALUATIONQAS_DELETE($valores); //se mostrará una vista de delete con los valores recogido
			}
			else{ //si vienen del POST
				$EVALUATION= new EVALUATION_MODEL($_REQUEST['idtrabajo'],$_REQUEST['loginEvaluador'],$_REQUEST['aliasEvaluado'],$_REQUEST['idhistoria'],'','','','',''); //creamos un objeto del modelo de dtaos que recoja esos valores
				$respuesta = $EVALUATION->DELETE(); //y les aplicamos la función de DELETE almacenanado el resultado en esta variable
				new MESSAGE($respuesta, './EvaluationQAS_Controller.php'); //se mostrá un mensaje en función del resultado del borrado
			}
			break;




		case 'EDIT': //si la acción recibida es EDIT

			if (!$_POST){//si no viene del POST

				$EVALUATION= new EVALUATION_MODEL($_REQUEST['idtrabajo'],$_REQUEST['loginEvaluador'],$_REQUEST['aliasEvaluado'],$_REQUEST['idhistoria'],'','','','','');  //creamos un objeto del modelo de datos que recoja los valores
				$valores = $EVALUATION->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
				new EVALUATIONQAS_EDIT($valores); //se mostrará una vista de edit con los valores recogidos
			}
			else{ //si vienen del POST

				$EVALUATION=get_data_form(); //almacenamos estos datos por medio de la función get_data_form
				$idT = $_REQUEST['idtrabajo'];
				$loginE = $_REQUEST['loginEvaluador'];
				$aliasE = $_REQUEST['aliasEvaluado'];
				$respuesta = $EVALUATION->EDIT(); //y les aplicamos la función edit
				$direc = './EvaluationQAS_Controller.php?idtrabajo='.$_REQUEST['idtrabajo'].'&loginEvaluador='.$_REQUEST['loginEvaluador'].'&aliasEvaluado='.$_REQUEST['aliasEvaluado'].'';
				new MESSAGE($respuesta,$direc); //se mostrará un mensaje en función del resultado de la edición
			}

			break;


		case 'SEARCH': //si la acción recibida es SEARCH

			if (!$_POST){ //si los datos no vienen del POST

				new EVALUATIONQAS_SEARCH(); //se creará una nueva vista de SEARCH
			}
			else{

				$EVALUATION= get_data_form(); //almacenamos estos datos por medio de la función get_data_form
				$datos = $EVALUATION->SEARCH(); //y les aplicamos la función search
				$lista = array('','','','','','','','',''); //creamos un array que se guardará en esta variable
				new EVALUATIONQAS_SHOWALL($lista, $datos, '../index.php'); //se mostrará una nueva vista de SHOWALL con los resultado de la busqueda
			}
			break;




		case 'SHOWCURRENT'://si la acción recibida es SHOWCURRENT

			$EVALUATION= new EVALUATION_MODEL($_REQUEST['idtrabajo'],$_REQUEST['loginEvaluador'],$_REQUEST['aliasEvaluado'],$_REQUEST['idhistoria'],'','','','','');  //creamos un objeto del modelo de datos que recoja los valores
			$valores = $EVALUATION->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
			new EVALUATIONQAS_SHOWCURRENT($valores); //se creará una nueva vista con los valores pertenecientes a la tuple seleccionada
			break;


			case 'SHOW':
				if (!$_POST){ //si no viene del POST
				$EVALUATION =new EVALUATION_MODEL('','','','','','','','','');
			}

			else{//si viene del POST

				$EVALUATION = get_data_form(); //almacenamos estos datos por medio de la función get_data_form

			}

			$datos=$EVALUATION->SEARCH();
			$lista = array('ID Trabajo','Evaluador','Evaluado','ID Historia','Correcto Alumno','Incorrecto Alumno','Correcto Profesor','Incorrecto Profesor', 'Ok');  //creamos un array que se guardará en esta variable
			new EVALUATION_SHOWALL($lista, $datos);	//se mostrará una nueva vista de SHOWALL con todos los datos de la tabla

			break;






		default://la acción por defecto será la vista de SHOWALL

			if (!$_POST){ //si no viene del POST
				$EVALUATION = new QAASIGNMENT_MODEL($_REQUEST['idtrabajo'],$_REQUEST['loginEvaluador'],'', $_REQUEST['aliasEvaluado']);  //creamos un objeto del modelo de datos que recoja los valores
			}

			else{//si viene del POST

				$EVALUATION = get_data_form(); //almacenamos estos datos por medio de la función get_data_form

			}
			$HISTORIAS = new STORY_MODEL($_REQUEST['idtrabajo'],'','');
			$datos2 = $HISTORIAS->SEARCH();
			$datos = $EVALUATION->SEARCH();  //y les aplicamos la función search
			$datosLista = $datos->fetch_assoc();
			while($historiasLista = $datos2->fetch_assoc()){
				$EVALHISTORIA = new EVALUATION_MODEL($datosLista['IdTrabajo'],$datosLista['LoginEvaluador'],$datosLista['AliasEvaluado'],$historiasLista['IdHistoria'],
																								'','','','','');
				if(!$EVALHISTORIA->EXISTE()){
					$EVALHISTORIA->ADD();

				}
			}

			$DEVOLVERHISTORIAS = new EVALUATION_MODEL($datosLista['IdTrabajo'],$datosLista['LoginEvaluador'],$datosLista['AliasEvaluado'],'','','','','','');
			$devolverLista = $DEVOLVERHISTORIAS->SEARCH();

			$lista = array('ID Trabajo','Evaluador','Evaluado','ID Historia','Correcto Alumno','Incorrecto Alumno','Correcto Profesor','Incorrecto Profesor', 'Ok');  //creamos un array que se guardará en esta variable
			new EVALUATIONQAS_SHOWALL($lista, $devolverLista);	//se mostrará una nueva vista de SHOWALL con todos los datos de la tabla

}






 ?>
