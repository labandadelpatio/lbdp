<?php 
/*  Fichero para el controlador de calificaciones
Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/
session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){ //comprobamos que la acción no sea nula y esté definida
	$_REQUEST['action'] = '';
}

//incluimos todos los ficheros que vamos a usar

include '../Views/Marks_Views/MARKS_SHOWALL.php';
include '../Views/Marks_Views/MARKS_ADD.php';
include '../Views/Marks_Views/MARKS_SEARCH.php';
include '../Views/Marks_Views/MARKS_SHOWCURRENT.php';
include '../Views/Marks_Views/MARKS_DELETE.php';
include '../Views/Marks_Views/MARKS_EDIT.php';
include '../Views/Marks_Views/MARKS_MOSTRAR.php';
include '../Views/Marks_Views/MARKS_MOSTRARQA.php';
include '../Views/MESSAGE.php';
include '../Models/JOBMARK_MODEL.php';

//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $MARKS

	function get_data_form(){

		$login = $_REQUEST['login']; //mediante la variable $login almacenamos el dato de que viene del post del campo login
		$idtrabajo = $_REQUEST['idtrabajo']; //mediante la variable $idtrabajo almacenamos el dato de que viene del post del campo idtrabajo
		$notaTrabajo = $_REQUEST['notaTrabajo']; //mediante la variable $notaTrabajo almacenamos el dato de que viene del post del campo notaTrabajo
		$action = $_REQUEST['action']; //mediante la variable $action almacenamos el dato de que viene del post del campo action

		$MARKS = new JOBMARK_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$login, 
			$idtrabajo,
			$notaTrabajo
		   );

		return $MARKS;
	}

//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo


switch ($_REQUEST['action']){ //Comprobamos que accion viene por el POST

	case 'ADD': //si la accion es ADD

			if (!$_POST){ //si no viene del POST

				new MARKS_ADD(); //se creará una nueva vista de ADD de MARKS
			}

			else{ //si vienen del POST

				$modelo = new JOBMARK_MODEL($_REQUEST['login'],$_REQUEST['idtrabajo'], $_REQUEST['notaTrabajo']);  //creamos un objeto del modelo de datos que recoja los valores

			
				$respuesta = $modelo->ADD(); //se aplicará la función de ADD en dichos datos, almacenando el resultado en la variable
				new MESSAGE($respuesta, './Marks_Controller.php'); //se mostrará un mensaje en función del resultado de la inserción
			
			}
			break;



		case 'DELETE'://si la acción recibida es DELETE

			if (!$_POST){ //los datos no vienen del POST
				$MARKS = new JOBMARK_MODEL($_REQUEST['login'], $_REQUEST['idtrabajo'],''); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $MARKS->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
				new MARKS_DELETE($valores); //se creará una vista de delete con los valores previamente recogidos
			}
			else{
				$MARKS = new JOBMARK_MODEL($_REQUEST['login'], $_REQUEST['idtrabajo'],''); //creamos un objeto del modelo de datos que recoja los valores
				$respuesta = $MARKS->DELETE(); //y les aplicamos la función de DELETE almacenanado el resultado en esta variable
				new MESSAGE($respuesta, './Marks_Controller.php'); //se mostrará una vista de delete con los valores recogidos
			}

			break;




			case 'EDIT': //si la acción recibida es EDIT

			if (!$_POST){//si no viene del POST


				$MARKS = new JOBMARK_MODEL($_REQUEST['login'], $_REQUEST['idtrabajo'],''); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $MARKS->RellenaDatos();//le aplicamos la función RellenaDatos a estos valores
				new MARKS_EDIT($valores); //se mostrará una vista de edit con los valores recogidos
			}
			else{
				
				$MARKS = new JOBMARK_MODEL($_REQUEST['login'],$_REQUEST['idtrabajo'], $_REQUEST['notaTrabajo']); //creamos un objeto del modelo de datos que recoja los valores


				$respuesta = $MARKS->EDIT(); //y les aplicamos la función edit
				new MESSAGE($respuesta, './Marks_Controller.php'); //se mostrará un mensaje en función del resultado de la edición
			
			}
			
			break;


			case 'SEARCH': //si la acción recibida es SEARCH

			if (!$_POST){ //si los datos no vienen del POST

				new MARKS_SEARCH();  //se creará una nueva vista de SEARCH
			}
			else{
				$MARKS = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form
				$datos = $MARKS->RellenaDatosNombre();  //y les aplicamos la función search
				$lista = array('Login','ID Trabajo','Nombre Trabajo','Calificacion');
				new MARKS_SHOWALL($lista, $datos, '../index.php'); //se mostrará una nueva vista de SHOWALL con los resultado de la busqueda
			}
			break;
        

      case 'MOSTRAR': //si la accion recibida es MOSTRAR

			$MARKS = new JOBMARK_MODEL($_REQUEST['login'], $_REQUEST['idtrabajo'],''); //creamos un objeto del modelo de datos que recoja los valores
			$datos = $MARKS->RellenaDatosNotas(); //le aplicamos la función RellenaDatosNotas a estos valores
            $lista = array('IdHistoria','CorrectoA','CorrectoP','ComenIncorrectoA','ComentIncorrectoP','TextoHistoria'); //creamos un array que se guardará en esta variable
			new MARKS_MOSTRAR($lista, $datos);
			break;
        
        case 'MOSTRARQA': //si la accion recibida es MOSTRARQA

			$MARKSQA = new JOBMARK_MODEL($_REQUEST['login'], $_REQUEST['idtrabajo'],''); //creamos un objeto del modelo de datos que recoja los valores
			$datos = $MARKSQA->RellenaDatosNotasQA(); //le aplicamos la función RellenaDatosNotasQA a estos valores
            $lista = array('IdHistoria','TextoHistoria','ok','ComentIncorrectoP','LoginEvaluado','CorrectoA'); //creamos un array que se guardará en esta variable
			new MARKS_MOSTRARQA($lista, $datos);
			break;

	case 'SHOWCURRENT': //si la acción recibida es SHOWCURRENT

			$MARKS = new JOBMARK_MODEL($_REQUEST['login'], $_REQUEST['idtrabajo'],''); //creamos un objeto del modelo de datos que recoja los valores
			$valores = $MARKS->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
			new MARKS_SHOWCURRENT($valores); //se creará una nueva vista con los valores pertenecientes a la tuple seleccionada
			break;
		
    default: //la acción por defecto será la vista de SHOWALL

			if (!$_POST){//si no viene del POST

				$MARKS = new JOBMARK_MODEL('','',''); //creamos un objeto del modelo de datos que recoja los valores
			}
			else{//si viene del POST

				$MARKS = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form
			}
			$datos = $MARKS->RellenaDatosNombre(); //le aplicamos la función RellenaDatosNombre a estos valores
			$lista = array('Login','ID Trabajo','Nombre Trabajo','Calificacion'); //creamos un array que se guardará en esta variable
			new MARKS_SHOWALL($lista, $datos); //se mostrará una nueva vista de SHOWALL con todos los datos de la tabla		 

}






 ?>