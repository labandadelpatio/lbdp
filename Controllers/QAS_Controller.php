
<?php

/*  Fichero para el controlador de los qas
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/


session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){ //comprobamos que la acción no es nula
	$_REQUEST['action'] = '';
}



//incluimos los distintos ficheros que vamos a utilizar

include '../Views/QAS_Views/QAS_SHOWALL.php';
include '../Views/QAS_Views/QAS_ADD.php';
include '../Views/QAS_Views/QAS_SEARCH.php';
include '../Views/QAS_Views/QAS_SHOWCURRENT.php';
include '../Views/QAS_Views/QAS_DELETE.php';
include '../Views/QAS_Views/QAS_EDIT.php';
include '../Views/MESSAGE.php';
include '../Models/QAASIGNMENT_MODEL.php';
include '../Models/JOB_MODEL.php';
include '../Views/Jobs_Views/JOB_SHOWQA.php';
include_once '../Models/DELIVERABLES_MODEL.php';
include_once '../Models/USERS_MODEL.php';


//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $QAS

	function get_data_form(){

		$idtrabajo = $_REQUEST['idtrabajo']; //mediante la variable $idtrabajo almacenamos el dato de que viene del post del campo idtrabajo
		$loginEvaluador = $_REQUEST['loginEvaluador']; //mediante la variable $loginEvaluador almacenamos el dato de que viene del post del campo loginEvaluador
		$loginEvaluado = $_REQUEST['loginEvaluado']; //mediante la variable $loginEvaluado almacenamos el dato de que viene del post del campo loginEvaluado
		$aliasEvaluado = $_REQUEST['aliasEvaluado']; //mediante la variable $aliasEvaluado almacenamos el dato de que viene del post del campo aliasEvaluado
		$action = $_REQUEST['action']; //mediante la variable $action almacenamos el dato de que viene del post del campo action

		$QAS = new QAASIGNMENT_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$idtrabajo,
			$loginEvaluador,
			$loginEvaluado,
			$aliasEvaluado,
			$action
		   );

		return $QAS;
	}


//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo

Switch ($_REQUEST['action']){ //Comprobamos que accion viene por el POST

	case 'ADD': //si la accion recibida es ADD

			if (!$_POST){ //si no viene del POST
				new QAS_ADD(); //se creará una nueva vista de ADD de QAS
			}
			else{
				$modelo = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form

				$respuesta = $modelo->ADD(); //se aplicará la función de ADD en dichos datos, almacenando el resultado en la variable
				new MESSAGE($respuesta, './QAS_Controller.php'); //se moestrará un mensaje en función del resultado de la inserción

			}
			break;



		case 'DELETE': //si la acción recibida es DELETE
			if (!$_POST){ //si no viene del POST
				$QAS= new QAASIGNMENT_MODEL($_REQUEST['idtrabajo'],$_REQUEST['loginEvaluador'],'',$_REQUEST['aliasEvaluado']); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $QAS->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
				new QAS_DELETE($valores); //se moestrá una vista de edit con los valores recogidos
			}
			else{//si vienen del POST
				$QAS= new QAASIGNMENT_MODEL($_REQUEST['idtrabajo'],$_REQUEST['loginEvaluador'],'',$_REQUEST['aliasEvaluado']); //creamos un objeto del modelo de dtaos que recoja esos valores
				$respuesta = $QAS->DELETE(); //y les aplicamos la función de DELETE almacenanado el resultado en esta variable
				new MESSAGE($respuesta, './QAS_Controller.php'); //se mostrá un mensaje en función del resultado del borrado
			}
			break;




		case 'EDIT': //si la acción recibida es EDIT

			if (!$_POST){ //y los datos no vienen del POST

				$QAS = new  QAASIGNMENT_MODEL($_REQUEST['idtrabajo'],$_REQUEST['loginEvaluador'],'',$_REQUEST['aliasEvaluado']); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $QAS->RellenaDatos(); //y les aplicamos la función RellenaDatos, almacenanado el resultado en esta variable
				new QAS_EDIT($valores); //se creará una nueva vista de edit con los valores previamente recogidos
			}
			else{ //si los datos vienen del POST
				$QAS = get_data_form(); //almacenamos estos datos por medio de la función get_data_form

				$respuesta = $QAS->EDIT(); //y les aplicamos la función edit
				new MESSAGE($respuesta, './QAS_Controller.php');//se mostrará un mensaje en función del resultado de la edición

			}

			break;
		case 'SEARCH': //si la acción recibida es SEARCH

			if (!$_POST){ //si los datos no vienen del POST

				new QAS_SEARCH(); //se creará una nueva vista de SEARCH
			}
			else{ //si los datos vienen del POST

				$QAS= get_data_form(); //almacenamos estos datos por medio de la función get_data_form
				$datos = $QAS->SEARCH(); //y les aplicamos la función search
				$lista = array('ID Trabajo','Login Evaluador','LoginEvaluado','Alias Evaluado'); //creamos un array que se guardará en esta variable
				new QAS_SHOWALL($lista, $datos, '../index.php'); //se mostrará una nueva vista de SHOWALL con los resultado de la busqueda
			}
			break;




		case 'SHOWCURRENT': //si la acción recibida es SHOWCURRENT

			$QAS= new QAASIGNMENT_MODEL($_REQUEST['idtrabajo'],$_REQUEST['loginEvaluador'],'',$_REQUEST['aliasEvaluado']); //creamos un objeto del modelo de datos que recoja los valores
			$valores = $QAS->RellenaDatos(); //y les aplicamos la función RellenaDatos, almacenanado el resultado en esta variable
			new QAS_SHOWCURRENT($valores); //se creará una nueva vista con los valores pertenecientes a la tuple seleccionada
			break;


		case 'SHOWALL': //si la acción es SHOWALL

			if (!$_POST){ //si los datos no vienen del POST

				$QAS= new QAASIGNMENT_MODEL('','','','');  //creamos un objeto del modelo de datos
			}

			else{

				$QAS= get_data_form(); //almacenamos estos datos por medio de la función get_data_form
			}


			$datos = $QAS->SEARCH();  //y les aplicamos la función search
			$lista = array('ID Trabajo','Login Evaluador','LoginEvaluado','Alias Evaluado');   //creamos un array que se guardará en esta variable
			new QAS_SHOWALL($lista, $datos); //se mostrará una nueva vista de SHOWALL con todos los datos de la tabla
			break;



			case 'GENERAR':
				$DELIVERABLES = new DELIVERABLES_MODEL($_REQUEST['idtrabajo'],'','','','');
				$entregas = $DELIVERABLES->SEARCHUSUARIOSENVIOTRABAJOS();

				$cont = 0;
				$entregaActual = 0;
				$arrayUsuarios = array();
				$arrayEntregas = array();

				while($entregasArray = $entregas->fetch_assoc() ) // Recorre todas las entregas cuyo campo ruta no es null
				{
					$arrayUsuarios[] = $entregasArray;
					$arrayEntregas[] = $entregasArray;
					$cont++;
				}
				$numeroUsuarios = $cont;
				$j = 0;
				// Recorre todos los usuarios que realizaron una entrega para ese trabajo
				for($i = 0; $i < $numeroUsuarios; $i++)
				{
						$j=0;
						while($j < 5) // Recorremos 5 veces este bucle para asignar 5 trabajos a cada usuario
						{
							if($entregaActual == $numeroUsuarios){
								$entregaActual = 0; // Si no quedan mas enregas en el indice del array bidimensional vuelve al inicio
								$cont = $numeroUsuarios;
							}
						 	if($arrayEntregas[$entregaActual]['login'] == $arrayUsuarios[$i]['login']) // Si la entrega coincide con el usuario al que le estamos asignado entregas
						 	{
								$j--;
								$entregaActual++;
						 	}
						 	else // si el login no coinciden
						 	{
						 		$QAASIGNMENT = new QAASIGNMENT_MODEL($_REQUEST['idtrabajo'],$arrayUsuarios[$i]['login'],
						 												$arrayEntregas[$entregaActual]['login'],$arrayEntregas[$entregaActual]['Alias']);
						 		// Asignamos el trabajo a un usuario para que realice el QA
						 		$QAASIGNMENT->ADD();
						 		$cont--;
						 		$entregaActual++;
								$j++;
							}
						}
					}
		new MESSAGE('Generación de QAS Completada', '../index.php');
		break;

		default: //la accion por defecto que se ejecutará sera un SHOWALL de JOBS

			if (!$_POST){ //si los datos no vienen del POST

				$QAS= new JOB_MODEL('','','','',''); //creamos un objeto del modelo de datos
			}

			else{ //si los datos vienen del POST

				$QAS= get_data_form(); //almacenamos estos datos por medio de la función get_data_form

			}

			$datos = $QAS->SEARCHQAS(); //y les aplicamos la función searcH
			$lista = array('ID Trabajo','Nombre Trabajo','Fecha Inicio','Fecha Fin','Porcentaje Nota');
			new JOB_SHOWQA($lista, $datos); //se mostrará una nueva vista de SHOWALL con todos los datos de la tabla

}






 ?>
