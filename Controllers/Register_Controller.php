
<?php
/*  Fichero para el controlador del registro de usuarios
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017 */

session_start(); //solicto trabajar con la sesión


include_once '../Locales/Strings_'.$_SESSION['idioma'].'.php'; //incluimos el fichero con los strings de idiomas

if(!isset($_POST['login'])){ //si la variable login está definida y no es null

	include '../Views/Users_Views/USERS_REGISTER.php';
	
	$register = new Register(); //creamos una nueva vista de registro
}
else{
		//incluimos los ficheros que vamos a utilizar
	
			include '../Models/USERS_MODEL.php';
			include_once '../Models/USERSGROUP_MODEL.php';


			$modelo = new USERS_MODEL($_REQUEST['login'],$_REQUEST['password'],$_REQUEST['dni'],$_REQUEST['nombre'],$_REQUEST['apellidos'],$_REQUEST['correo'],$_REQUEST['telefono'],$_REQUEST['direccion']); // creamos un nuevo objeto del modelo de datos, con los datos que vienen del POST
	   		$respuesta = $modelo->Register(); //llamaremos al método Register para ver si el usuario ya existe en la BD

			if ( $respuesta == true ) { //no existe
				$respuesta = $modelo->registrar();
				$control = new USERSGROUP_MODEL(0, $_REQUEST['login']);
				$control->ADD(); //añidaremos el nuevo usuario
				include '../Views/MESSAGE.php';
				new MESSAGE($respuesta, './Login_Controller.php');
											}
			else{ //si existe mostraremos un mensaje, diciendo que ya está registrado
				include '../Views/MESSAGE.php';
				new MESSAGE($respuesta, './Login_Controller.php');
			}
}

?>
