
<?php 

/*  Fichero para el controlador de historias de usuario
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/


session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){ //comprobamos que la acción esté definida y no sea null
	$_REQUEST['action'] = ''; 
}


//incluimos los distintos ficheros que vamos a utilizar

include '../Views/Stories_Views/STORIES_SHOWALL.php';
include '../Views/Stories_Views/STORIES_ADD.php';
include '../Views/Stories_Views/STORIES_SEARCH.php';
include '../Views/Stories_Views/STORIES_SHOWCURRENT.php';
include '../Views/Stories_Views/STORIES_DELETE.php';
include '../Views/Stories_Views/STORIES_EDIT.php';
include '../Views/MESSAGE.php';
include '../Models/STORY_MODEL.php';


//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $STORY
	
	function get_data_form(){

		$idtrabajo = $_REQUEST['idtrabajo']; //mediante la variable $idtrabajo almacenamos el dato de que viene del post del campo idtrabajo
		$idhistoria = $_REQUEST['idhistoria']; //mediante la variable $idhistoria almacenamos el dato de que viene del post del campo idhistoria
		$textohistoria = $_REQUEST['textohistoria']; //mediante la variable $textohistoria almacenamos el dato de que viene del post del campo textohisoria
		$action = $_REQUEST['action']; //mediante la variable $action almacenamos el dato de que viene del post del campo action

		$STORY = new STORY_MODEL(  //creamos un objeto del modelo de datos, con los datos obtenidos
			$idtrabajo, 
			$idhistoria,
			$textohistoria
		   );

		return $STORY;
	}


//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo

Switch ($_REQUEST['action']){ //Comprobamos que accion viene por el POST

	case 'ADD':  //si la accion es añadir
			if (!$_POST){ //y los datos no vienen del POST 

				new STORIES_ADD(); //se mostrará la vista de ADD de stories
			}

			else{ //si los datos vienen del POST

				$modelo = get_data_form(); //recogemos los datos con la función previamnete definida

			
				$respuesta = $modelo->ADD(); //le aplicamos la función de añadir y lo añadimos a nuestra BD
				new MESSAGE($respuesta, './Stories_Controller.php');//se mostrará una respuesta en función del resultado de la inserción
			
			}
			break;



		case 'DELETE': //si la accion es borrar

			if (!$_POST){ //y los datos no vienen del POST

				$STORY = new STORY_MODEL($_REQUEST['idtrabajo'],$_REQUEST['idhistoria'], ''); //creamos un objeto del Modelo de Datos  y lo almacenamos en la variable $STORY
				$valores = $STORY->RellenaDatos(); //apicamos sobre ella el metodo rellenaDatos
				new STORIES_DELETE($valores); //creamos una vista de previsualización del borrado de una tupla
			}

			else{ //si vienen del POST

				$STORY = new STORY_MODEL($_REQUEST['idtrabajo'],$_REQUEST['idhistoria'], ''); //creamos un nuevo objeto del modelo almacenado en la variable $STORY
				$respuesta = $STORY->DELETE(); //aplicamos sobre ella el metodo de DELETE
				new MESSAGE($respuesta, './Stories_Controller.php'); //se mostrará una respuesta en función del resultado del borrado
			}
			break;




		case 'EDIT':	//si la accion es editar

			if (!$_POST){ //y los datos no vienen del POST

				$STORY = new STORY_MODEL($_REQUEST['idtrabajo'], $_REQUEST['idhistoria'], ''); //creamos un objeto del Modelo de Datos  y lo almacenamos en la variable $STORY
				$valores = $STORY->RellenaDatos();//apicamos sobre ella el metodo rellenaDatos
				new STORIES_EDIT($valores); //se creará una vista de edit con los datos recogidos previamente
			}

			else{ //si vienen del POST
				
				$STORY = get_data_form();//recogemos los datos con la función previamnete definida


				$respuesta = $STORY->EDIT();//le aplicamos la función de añadir y lo añadimos a nuestra BD
				new MESSAGE($respuesta, './Stories_Controller.php'); //se mostará una un mensaje según sea el resultado de la edición
			
			}
			
			break;


		case 'SEARCH': //si la acción es buscar

			if (!$_POST){ //y los datos no vienen del POST

				new STORIES_SEARCH(); //se mostrará la vista de SEARCH de stories
			}

			else{//si los datos vienen del POST

				$STORY = get_data_form(); //recogemos los datos que provienen del POST
				$datos = $STORY->SEARCH();//aplicamos el metodo de SEARCH sobre la variable que almacena los datos
				$lista = array('ID Trabajo',' ID Historia','Texto Historia');
				new STORIES_SHOWALL($lista, $datos, '../index.php'); //se mostrará una vista SHOWALL
			}
			break;




		case 'SHOWCURRENT'://si la acción es mostrar en detalle


			$STORY = new STORY_MODEL($_REQUEST['idtrabajo'], $_REQUEST['idhistoria'], ''); //creamos un objeto del Modelo de Datos  y lo almacenamos en la variable $STORY
			$valores = $STORY->RellenaDatos(); //apicamos sobre ella el metodo rellenaDatos
			new STORIES_SHOWCURRENT($valores); //se creará una vista de showcurrent con los datos previamente recogidos
			break;


		default: //el caso por defecto que se nos mostrara es el de SHOWALL

			if (!$_POST){ //si no viene del POST

				$STORY = new STORY_MODEL('','', ''); //creamos un objeto del modelo de datos 
			}

			else{

				$STORY = get_data_form(); //recogeremos los datos en la variable $STORY aplicando el metodo get_data_form
			}

			$datos = $STORY->SEARCH(); //aplicamos el metodo de SEARCH a dicha variable
			$lista = array('ID Trabajo',' ID Historia','Texto Historia');
			new STORIES_SHOWALL($lista, $datos);//mostraremos una tabla con todos los datos a través de una vista de SHOWALL
		

}






 ?>