
<?php
/*  Fichero para el controlador de usuarios
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/
session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){ //comprobamos que la acción esté definida y no sea null
	$_REQUEST['action'] = '';
}


//incluimos los distintos ficheros que vamos a utilizar
include '../Views/Users_Views/USERS_SHOWALL.php';
include '../Views/Users_Views/USERS_ADD.php';
include '../Views/Users_Views/USERS_SEARCH.php';
include '../Views/Users_Views/USERS_SHOWCURRENT.php';
include '../Views/Users_Views/USERS_DELETE.php';
include '../Views/Users_Views/USERS_EDIT.php';
include '../Views/Users_Views/USERS_ASSIGN.php';
include '../Views/MESSAGE.php';
include_once '../Models/USERS_MODEL.php';
include_once '../Models/GROUPS_MODEL.php';
include_once '../Models/USERSGROUP_MODEL.php';

//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $USERS
	function get_data_form(){

		$login = $_REQUEST['login']; //mediante la variable $login almacenamos el dato de que viene del post del campo login
		$password = $_REQUEST['password'];//mediante la variable $password almacenamos el dato de que viene del post del campo password
		$dni = $_REQUEST['DNI'];//mediante la variable $dni almacenamos el dato de que viene del post del campo dni
		$nombre = $_REQUEST['Nombre'];//mediante la variable $nombre almacenamos el dato de que viene del post del campo nombre
		$apellidos = $_REQUEST['Apellidos'];//mediante la variable $apellidos almacenamos el dato de que viene del post del campo apellidos
		$correo = $_REQUEST['Correo'];//mediante la variable $correo almacenamos el dato de que viene del post del campo email
		$direccion = $_REQUEST['Direccion'];//mediante la variable $direccion almacenamos el dato de que viene del post del campo direccion
		$telefono = $_REQUEST['Telefono'];//mediante la variable $telefono almacenamos el dato de que viene del post del campo telefono
		$action = $_REQUEST['action'];//mediante la variable $action almacenamos el dato de que viene del post del campo accion

		$USERS = new USERS_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$login,
			$password,
			$dni,
			$nombre,
			$apellidos,
			$correo,
			$direccion,
			$telefono
		   );

		return $USERS;
	}


//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo

Switch ($_REQUEST['action']){//Comprobamos que accion viene por el POST

	case 'ADD': //si la accion es añadir
			if (!$_POST){ //y los datos no vienen del POST
				new USERS_ADD(); //Se mostrará la vista de ADD
			}
			else{//si los datos vienen del POST

				$modelo = get_data_form(); //recogemos los datos con la función previamnete definida

				$control = new USERSGROUP_MODEL(0, $_REQUEST['login']); //creamos un objeto del UserGrup para que el usuario tambiés se añada
				$control->ADD(); //a la tabla de usersgroup

				$respuesta = $modelo->ADD(); //le aplicamos la función de añadir y lo añadimos a nuestra BD
				new MESSAGE($respuesta, './Users_Controller.php'); //se mostrará una respuesta en función del resultado de la inserción

			}
			break;



		case 'DELETE': //si la accion es borrar

			if (!$_POST){ //y los datos NO vienen del POST 
				$USERS = new USERS_MODEL($_REQUEST['login'], '', '', '', '', '', '', ''); //creamos un objeto del Modelo de Datos  y lo almacenamos en la variable $USERS
				$valores = $USERS->RellenaDatos(); //apicamos sobre ella el metodo rellenaDatos
				new USERS_DELETE($valores);//creamos una vista de previsualización del borrado de una tupla
			}
			else{ //si viene del post 

				$USERS = new USERS_MODEL($_REQUEST['login'], '', '', '', '', '', '', ''); //creamos un objeto del Modelo de Datos
				$respuesta = $USERS->DELETE(); //aplicamos sobre el, el metodo de DELETE
				if($respuesta == true){ //si se ha borrado correctamente en la tabla USERS
					$USER_GROUP = new USERSGROUP_MODEL('', $_REQUEST['login']); //También se llevará acabo el borrado en la tabla USERSGROUP
					$respuesta2 = $USER_GROUP->DELETEUSUARIO();
				}
				new MESSAGE($respuesta2, './Users_Controller.php'); //Se moestrará un mensaje en función del resultado del borrado
			}
			break;

		case 'EDIT': //si la acción es editar

			if (!$_POST){ //y los datos NO viene del POST

				$USERS = new USERS_MODEL($_REQUEST['login'], '', '', '', '', '', '', ''); //creamos un objeto del Modelo de Datos  y lo almacenamos en la variable $USERS
				$valores = $USERS->RellenaDatos(); //apicamos sobre ella el metodo rellenaDatos
				new USERS_EDIT($valores); //creamos una vista que nos muestre los datos recogidos de la tupla en un formulario de EDIT
			}
			else{ //si vienen del POST

				$USERS = get_data_form(); //aplicamos el método definido anteriormente y almacenamos los datos que vienen del POST en la variable $users


				$respuesta = $USERS->EDIT(); //aplicamos el método edit sobre la variable $USERS
				new MESSAGE($respuesta, './Users_Controller.php');//Se moestrará un mensaje en función del resultado del edit

			}

			break;


		case 'SEARCH': //si la accion es buscar

			if (!$_POST){ //si los datos no vienen del POST

				new USERS_SEARCH(); //se mostrará una vista de SEARCH
			}

			else{//si los datos vienen del POST

				$USERS = get_data_form(); //recogeremos los datos en la variable $USERS aplicando el metodo get_data_form
				$datos = $USERS->SEARCH(); //aplicaremos el metodo de SEARCH sobre esta variable
				$lista = array('','','','','','','','');
				new USERS_SHOWALL($lista, $datos, '../index.php'); //mostraremos un mensaje en función del resultado del SEARCH
			}

			break;




		case 'SHOWCURRENT': //si la acion es mostrar en detalle (SHOWCURRENT)

			$USERS = new USERS_MODEL($_REQUEST['login'], '', '', '', '', '', '', ''); //crearemos un objeto del modelo de datos
			$valores = $USERS->RellenaDatos(); //recogeremos los datos de la tupla almacenada
			new USERS_SHOWCURRENT($valores); //y los moestraremos en una tabla a través de la vista de SHOWCURRENT
			break;


    default: //el caso por defecto que se nos mostrara es el de SHOWALL

			if (!$_POST){ //si los datos no vienen del POST
				$USERS = new USERS_MODEL('','', '', '', '', '', '', ''); //creamos un objeto del modelo de datos 
			}
			else{ //si vienen del POST

				$USERS = get_data_form(); //recogeremos los datos en la variable $USERS aplicando el metodo get_data_form
			}

			$datos = $USERS->SEARCH(); //aplicamos el metodo de SEARCH a dicha variable 
			$lista = array('Login','Password','DNI','Nombre','Apellidos','Correo','Direccion','Telefono');
			new USERS_SHOWALL($lista, $datos); //mostraremos una tabla con todos los datos a través de una vista de SHOWALL

}






 ?>
