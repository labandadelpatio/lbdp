
<?php
/*  Fichero para el controlador de funcionalidades
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/
session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){ //comprobamos que la acción no sea nula y esté definida
	$_REQUEST['action'] = '';
}

//incluimos todos los ficheros que vamos a usar

include '../Views/Functionalities_Views/FUNCTIONALITIES_SHOWALL.php';
include '../Views/Functionalities_Views/FUNCTIONALITIES_ADD.php';
include '../Views/Functionalities_Views/FUNCTIONALITIES_SEARCH.php';
include '../Views/Functionalities_Views/FUNCTIONALITIES_SHOWCURRENT.php';
include '../Views/Functionalities_Views/FUNCTIONALITIES_DELETE.php';
include '../Views/Functionalities_Views/FUNCTIONALITIES_EDIT.php';
include '../Views/Functionalities_Views/FUNCTIONALITIES_ASSIGN.php';
include '../Views/MESSAGE.php';
include '../Models/FUNCTIONALITIES_MODEL.php';
include '../Models/ACTION_MODEL.php';

//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $FUNCIONALITIES

	function get_data_form(){

		$idfuncionalidad = $_REQUEST['idfuncionalidad']; //mediante la variable $idfuncionalidad almacenamos el dato de que viene del post del campo idfuncionalidad
		$nombrefuncionalidad = $_REQUEST['nombrefuncionalidad']; //mediante la variable $nombrefuncionalidad almacenamos el dato de que viene del post del campo nombrefuncionalidad
		$descripfuncionalidad = $_REQUEST['descripfuncionalidad']; //mediante la variable $descripfuncioalidad almacenamos el dato de que viene del post del campo descripfuncionalidad

		$FUNCTIONALITIES = new FUNCTIONALITIES_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$idfuncionalidad,
			$nombrefuncionalidad,
			$descripfuncionalidad
		   );

		return $FUNCTIONALITIES;
	}


//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo

switch ($_REQUEST['action']){ //Comprobamos que accion viene por el POST

	case 'ADD': //si la accion es ADD

			if (!$_POST){ //si no viene del POST

				new FUNCTIONALITIES_ADD(); //se creará una nueva vista de ADD de FUNCIONALITIES
			}
			else{

				$modelo = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form


				$respuesta = $modelo->ADD(); //se aplicará la función de ADD en dichos datos, almacenando el resultado en la variable
				new MESSAGE($respuesta, './Functionalities_Controller.php'); //se mostrará un mensaje en función del resultado de la inserción

			}
			break;



	case 'DELETE'://si la acción recibida es DELETE

			if (!$_POST){ //los datos no vienen del POST

				$FUNCTIONALITIES = new FUNCTIONALITIES_MODEL($_REQUEST['idfuncionalidad'], '', ''); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $FUNCTIONALITIES->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
				new FUNCTIONALITIES_DELETE($valores); //se creará una vista de delete con los valores previamente recogidos

			}
			else{

	            $FUNCTIONALITIES = new FUNCTIONALITIES_MODEL($_REQUEST['idfuncionalidad'], '', ''); //creamos un objeto del modelo de datos que recoja los valores
				$respuesta = $FUNCTIONALITIES->DELETE(); //y les aplicamos la función de DELETE almacenanado el resultado en esta variable
				new MESSAGE($respuesta, './Functionalities_Controller.php'); //se mostrará una vista de delete con los valores recogidos
			}
			break;




		case 'EDIT': //si la acción recibida es EDIT

			if (!$_POST){//si no viene del POST


				$FUNCTIONALITIES = new FUNCTIONALITIES_MODEL($_REQUEST['idfuncionalidad'], '', ''); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $FUNCTIONALITIES->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
				new FUNCTIONALITIES_EDIT($valores); //se mostrará una vista de edit con los valores recogidos
			}
			else{

			$FUNCTIONALITIES = new FUNCTIONALITIES_MODEL($_REQUEST['idfuncionalidad'], $_REQUEST['nombrefuncionalidad'],$_REQUEST['descripfuncionalidad']); //creamos un objeto del modelo de datos que recoja los valores


				$respuesta = $FUNCTIONALITIES->EDIT(); //y les aplicamos la función edit
				new MESSAGE($respuesta, './Functionalities_Controller.php'); //se mostrará un mensaje en función del resultado de la edición

			}

			break;


		case 'SEARCH': //si la acción recibida es SEARCH

			if (!$_POST){ //si los datos no vienen del POST

				new FUNCTIONALITIES_SEARCH();  //se creará una nueva vista de SEARCH
			}

			else{

				$FUNCTIONALITIES = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form
				$datos = $FUNCTIONALITIES->SEARCH(); //y les aplicamos la función search
				$lista = array('','','');
				new FUNCTIONALITIES_SHOWALL($lista, $datos, '../index.php'); //se mostrará una nueva vista de SHOWALL con los resultado de la busqueda
			}
			break;


		case 'SHOWCURRENT': //si la acción recibida es SHOWCURRENT

			$FUNCTIONALITIES = new FUNCTIONALITIES_MODEL($_REQUEST['idfuncionalidad'], '', ''); //creamos un objeto del modelo de datos que recoja los valores
			$valores = $FUNCTIONALITIES->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
			new FUNCTIONALITIES_SHOWCURRENT($valores); //se creará una nueva vista con los valores pertenecientes a la tuple seleccionada
			break;

		default://la acción por defecto será la vista de SHOWALL

			if (!$_POST){//sino viene del POST

				$FUNCTIONALITIES = new FUNCTIONALITIES_MODEL('','', ''); //creamos un objeto del modelo de datos que recoja los valores
			}
			else{//si viene del POST

				$FUNCTIONALITIES = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form

			}

			$datos = $FUNCTIONALITIES->SEARCH(); //y les aplicamos la función search
			$lista = array('ID Funcionalidad','Nombre Funcionalidad','Descripcion Funcionalidad'); //creamos un array que se guardará en esta variabl
			new FUNCTIONALITIES_SHOWALL($lista, $datos); //se mostrará una nueva vista de SHOWALL con todos los datos de la tabla

}






 ?>
