
<?php 
/*  Fichero para el controlador de acciones
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/
session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){ 
	$_REQUEST['action'] = '';
}

//Mediante estos includes incluimos los ficheros a utilizar en este Controlador.

include '../Views/Action_Views/ACTION_SHOWALL.php';
include '../Views/Action_Views/ACTION_ADD.php';
include '../Views/Action_Views/ACTION_SEARCH.php';
include '../Views/Action_Views/ACTION_SHOWCURRENT.php';
include '../Views/Action_Views/ACTION_DELETE.php';
include '../Views/Action_Views/ACTION_EDIT.php';
include '../Views/MESSAGE.php';
include '../Models/ACTION_MODEL.php';

//Mediante está función capturamos los datos que vienen del POST y creamos un objeto ACTION.
	function get_data_form(){

		$idaccion = $_REQUEST['idaccion']; //mediante esta variable recogeremos el idaccion que viene del POST
		$nombreaccion = $_REQUEST['nombreaccion'];//mediante esta variable recogeremos el nombreaccion que viene del POST
		$descripaccion = $_REQUEST['descripaccion'];//mediante esta variable recogeremos la descripaccion que viene del POST
		$action = $_REQUEST['action'];//mediante esta variable recogeremos la action que viene del POST

		$ACTION = new ACTION_MODEL(
			$idaccion, 
			$nombreaccion,
			$descripaccion,
			$action
		   );

		return $ACTION;
	}



//A través de este switch comprobaremos con que acción ha de ejecutarse
Switch ($_REQUEST['action']){

//Si el usuario escoge la acción de añadir entraremos en este case
	case 'ADD':
			if (!$_POST){ //si los datos NO vienen del POST creamos una nueva vista de ACTION_ADD, es decir, se mostrará el formulario que permite
				new ACTION_ADD(); //añadir una acción
			}
			else{
				$modelo = get_data_form(); //mediante a función capturamos los datos que vienen del POST y los almacenamos en la variable $modelo

			
				$respuesta = $modelo->ADD(); //aplicamos el metodo ADD a los datos almacenados anteriormente, lo que hará que se inserte en la BD
				new MESSAGE($respuesta, './Action_Controller.php'); //Mostrará un mensaje dependiendo del resulyado de la inserción
			
			}
			break;


//Si el usuario escoge la opción de borrar entraremos en este case
		case 'DELETE':
			if (!$_POST){ //si los datos NO vienen del POST 
				$ACTION = new ACTION_MODEL($_REQUEST['idaccion'], '', ''); //creamos un objeto del Modelo de Datos  y lo almacenamos en la variable $ACTION
				$valores = $ACTION->RellenaDatos(); //apicamos sobre ella el metodo rellenaDatos
				new ACTION_DELETE($valores); //creamos una vista de previsualización del borrado de una tupla
			}
			else{

				$ACTION = new ACTION_MODEL($_REQUEST['idaccion'], '', ''); //creamos un objeto del Modelo de Datos
				$respuesta = $ACTION->DELETE(); //aplicamos el método DELETE sobre los datos seleccionados
				new MESSAGE($respuesta, './Action_Controller.php'); //Mostrará un mensaje dependiendo del resultado del borrado
			}
			break;



//Si el usuario escoge la opción de editar entraremos en este case

		case 'EDIT':		
			if (!$_POST){ //Si los datos no vienen del POST

				$ACTION = new ACTION_MODEL($_REQUEST['idaccion'], '', ''); //creamos un nuevo objeto del modelo de datos 
				$valores = $ACTION->RellenaDatos(); //Recogemos los datos de la tupla seleccionada
				new ACTION_EDIT($valores); //Los mostramos a través de un formulario de EDIT
			}
			else{
				
				$ACTION = get_data_form(); //recogemos los datos que vienen del POST


				$respuesta = $ACTION->EDIT(); //Le aplicamos a esos datos la función de EDITAR
				new MESSAGE($respuesta, './Action_Controller.php'); //Se mostrará un mensaje dependiendo del restado de editar
			
			}
			
			break;

//Si el usuario escoge la opción de buscar entraremos en este case

		case 'SEARCH':
			if (!$_POST){//Si los datos no vienen del POST
				new ACTION_SEARCH(); //Se mostrará una vista de SEARCH, es decir un formulario
			}
			else{
				$ACTION = get_data_form(); //recogemos los datos que vienen del POST
				$datos = $ACTION->SEARCH(); //les aplicamos la función de buscar
				$lista = array('ID Accion','Nombre Accion','Descripcion Accion');
				new ACTION_SHOWALL($lista, $datos, '../index.php'); //mostramos una vista de SHOWALL según el resultado
			}
			break;



//Si el usuario escoge la opción de ver en datalle entraremos en este case:

		case 'SHOWCURRENT':
			$ACTION = new ACTION_MODEL($_REQUEST['idaccion'], '', '');
			$valores = $ACTION->RellenaDatos(); //recogemos los datos de la tupla
			new ACTION_SHOWCURRENT($valores);//creamos una nueva vista de SHOWCURRENT con los datos de la tupla
			break;


//el caso por defecto será una vista de SHOWALL			
		default:
			if (!$_POST){ //que si no viene del POST 
				$ACTION = new ACTION_MODEL('','', ''); //creamos un objeto del modelo de datos
			}
			else{
				$ACTION = get_data_form(); //almacenamos los datos que vengan del POST
			}
			$datos = $ACTION->SEARCH(); //aplicamos sobre ellos el metodo SEARCH
			$lista = array('ID Accion','Nombre Accion','Descripcion Accion');
			new ACTION_SHOWALL($lista, $datos);	//se moestrará una tabla con los datos

}






 ?>