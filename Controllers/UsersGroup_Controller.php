
<?php

/*  Fichero para el controlador de asignar grupos a usuarios
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/


session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){ //comprobamos que la acción no sea nula y esté definida
	$_REQUEST['action'] = '';
}

//incluimos todos los ficheros que vamos a usar

include '../Views/Users_Views/USERS_ASSIGN.php';
include '../Views/MESSAGE.php';
include_once '../Models/USERSGROUP_MODEL.php';
include '../Models/USERS_MODEL.php';
include '../Models/GROUPS_MODEL.php';

//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $USERSGROUP
	function get_data_form(){

		$idgrupo = $_REQUEST['idgrupo']; //variable que recoge el valor que viene del POST de idGrupo
		$login = $_REQUEST['login'];//variable que reocge el valor que viene del POST de login
		$action = $_REQUEST['action'];//variable que recoge el valor que viene del post de actiom

		$USERSGROUP = new USERSGROUP_MODEL(
			$idgrupo,
			$login,
			$action
		   );

		return $USERSGROUP;
	}


//Según la acción recibida por POST entrará en el case corrrspondiente

Switch ($_REQUEST['action']){

//Si recibe la accion de ASIGNAR 

		case 'ASSIGN': //si la accion es assign

			if (!isset($_REQUEST['idgrupo'])){ //determina si la variable idgrupo está definida y no es nula

				$USUARIO = new USERS_MODEL($_REQUEST['login'], '', '', '', '', '', '', ''); //creamos un objeto del modelo de datos
				$valores = $USUARIO->RellenaDatos(); //obtenemos los datos a traves de la funcion RellenaDatos y los almacenamos en esta variable


				$GRUPO = new GROUPS_MODEL('','',''); //creamos un nuevo objeto del modelo de datos
                $resultado = $GRUPO->SEARCH(); //y les aplicamos la función search

				$lista = array('ID Grupo','Nombre Grupo','Descripcion Grupo'); //creamos un array que almacene estos campos

			  new USERS_ASSIGN($valores, $lista, $resultado); // se creara una nueva vista de USERS_ASSNG con las nuevas asignacciones
			}
			else{ //si no es así

				$idgr = $_REQUEST['idgrupo']; //creamos una variable que lamace el idgrupo
				$login = $_REQUEST['login']; //creaosb una variable que almacene el login
				$USERGRUPO = new USERSGROUP_MODEL($idgr, $login); //creamos un nuevo objeto del modelo de datos
				$USERGRUPO->ADD(); //y les aplicamos la función de ADD

				header("Location:../Controllers/UsersGroup_Controller.php?action=ASSIGN&login=$login"); //evuamos esta informacion al controller
			}
			break;

			case 'DESASSIGN': //si la accion es DESSASING

			$idgr = $_REQUEST['idgrupo']; //creamos una variable que lamace el idgrupo
			$login = $_REQUEST['login']; //creaosb una variable que almacene el login
			$USERGRUPO = new USERSGROUP_MODEL($idgr, $login); //creamos un nuevo objeto del modelo de datos
			$USERGRUPO->DELETE(); //y a este objeto le aplicamos la función de DELETE

			header("Location:../Controllers/UsersGroup_Controller.php?action=ASSIGN&login=$login");
			break;

		default; //la accion por defecto

			if (!$_POST){//si los datos no viene del POST
				$USERSGROUP = new USERSGROUP_MODEL('',''); //creamos un nuevo objeto del modelo de datos
			}
			else{ //si los datos vienen del POST

				$USERSGROUP = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form
			}
			$datos = $USERSGROUP->SEARCH(); //y les aplicamos la función search
			$lista = array('idgrupo','login'); //creamos un array que almacene estos campos
			new USERSGROUP_SHOWALL($lista, $datos); //se moestraran todos los grupos con sus usuarios

}






 ?>
