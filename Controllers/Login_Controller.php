<!--
Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
 Fecha: 23/12/2017
En este fichero está el controlador del login que determina las acciones y procedimientos posteriores en funcion de los datos introducidos por el usuario( si datos incorrectos crear nueva vista de login tras un feedback al usuario, en caso contrario se envía al usuario al index)
-->


<?php

session_start();//solicto trabajar con la sesión

if(!isset($_REQUEST['login']) && !(isset($_REQUEST['password']))){  //si la variable login está definida y no es null y lo mismo con la variable password

	include '../Views/Users_Views/USERS_Login.php';

	$login = new Login(); //creamos una nueva vista de Login
}

else{ //de no estar definidas o ser nulas cualquiera de las dos variables

	include '../Models/BdAdmin.php';

	include '../Models/USERS_MODEL.php';

	$usuario = new USERS_MODEL($_REQUEST['login'],$_REQUEST['password'],'','','','','',''); //creamos un nuevo objeto del modelo de datos
	$respuesta = $usuario->Login(); //le aplicamos la función de Login y almacenamos su respuesta

	if ($respuesta == 'true'){ //si existe en la base de datos y la contraseña es correcta
		session_start(); //pordrá inciar sesión
		$_SESSION['login'] = $_REQUEST['login']; //el login de usuario que accede será el login que almacenamos en la variable $_SESSION
		header('Location: ../index.php'); 
	}
	else{//de no ser true la respuesta

		include '../Views/MESSAGE.php';

		new MESSAGE($respuesta, './Login_Controller.php'); //se mostrará un mensaje con lo ocurrido, que no esté registrado o que la contraseña sea incorrecta
	} 

}

?>

