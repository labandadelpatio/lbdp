
<?php
/*  Fichero para el controlador de los trabajos
Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 23/12/2017*/
session_start(); //solicito trabajar con la session



if (!isset($_REQUEST['action'])){  //comprobamos que la acción no sea nula y esté definida
	$_REQUEST['action'] = '';
}

//incluimos todos los ficheros que vamos a usar

include '../Views/Jobs_Views/JOB_SHOWALL.php';
include '../Views/Jobs_Views/JOB_ADD.php';
include '../Views/Jobs_Views/JOB_SEARCH.php';
include '../Views/Jobs_Views/JOB_SHOWCURRENT.php';
include '../Views/Jobs_Views/JOB_DELETE.php';
include '../Views/Jobs_Views/JOB_EDIT.php';
include '../Views/MESSAGE.php';
include '../Models/JOB_MODEL.php';
include '../Models/DELIVERABLES_MODEL.php';
include '../Models/EVALUATION_MODEL.php';
include '../Models/JOBMARK_MODEL.php';

//A través de esta funcíon recogeremos los datos que vienen del POST creando un nuevo objeto $JOB

	function get_data_form(){


		$idtrabajo = $_REQUEST['idtrabajo']; //mediante la variable $idtrabajo almacenamos el dato de que viene del post del campo idtrabajo
		$nomtrabajoCompleto= $_REQUEST['nomtrabajo'] . $_REQUEST['nombtrabajo']; //mediante la variable $nombretrabajo almacenamos el dato de que viene del post del campo nombretrabajo

		$idtrabajo = $_REQUEST['idtrabajo'];
		$nomtrabajo= $_REQUEST['nomtrabajo'] ."-". $_REQUEST['nombtrabajo'];


		$fecini = $_REQUEST['fecini']; //mediante la variable $fecini almacenamos el dato de que viene del post del campo fecini
		$fecfin = $_REQUEST['fecfin']; //mediante la variable $fecfin almacenamos el dato de que viene del post del campo fecfin
		$porcentajenota = $_REQUEST['porcentajenota']; //mediante la variable $porcentajenota almacenamos el dato de que viene del post del campo porcentajenota

		$action = $_REQUEST['action']; //mediante la variable $action almacenamos el dato de que viene del post del campo action

		$JOB = new JOB_MODEL( //creamos un objeto del modelo de datos, con los datos obtenidos
			$idtrabajo,
			$nomtrabajoCompleto,
			$fecini,
			$fecfin,
			$porcentajenota
		   );

		return $JOB;
	}

//A través de este switch controlaremos las distintas acciones que puedne llevarse a cabo

Switch ($_REQUEST['action']){  //Comprobamos que accion viene por el POST

	case 'ADD': //si la accion es ADD
			if (!$_POST){//si no viene del POST

				new JOB_ADD(); //se creará una nueva vista de ADD de JOB
			}

			else{

				$modelo = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form



				$respuesta = $modelo->ADD(); //se aplicará la función de ADD en dichos datos, almacenando el resultado en la variable
				new MESSAGE($respuesta, './Jobs_Controller.php'); //se mostrará un mensaje en función del resultado de la inserción


			}
			break;



		case 'DELETE'://si la acción recibida es DELETE

			if (!$_POST){ //los datos no vienen del POST

				$JOB = new JOB_MODEL($_REQUEST['idtrabajo'], '', '','',''); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $JOB->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
				new JOB_DELETE($valores); //se creará una vista de delete con los valores previamente recogidos
			}
			else{
				$JOB = new JOB_MODEL($_REQUEST['idtrabajo'], '', '','','');  //creamos un objeto del modelo de datos que recoja los valores
				$respuesta = $JOB->DELETE(); //y les aplicamos la función de DELETE almacenanado el resultado en esta variable
				new MESSAGE($respuesta, './Jobs_Controller.php');  //se mostrará una vista de delete con los valores recogidos
			}
			break;

case 'GENERARNOTAS':
				$TRABAJO = new JOB_MODEL($_REQUEST['idtrabajo'],'','', '', '');
				$trabajoid = $TRABAJO->SEARCH();
				$porcentaje = $trabajoid->fetch_array(); // Cogemos el porcentaje de la nota de ese trabajo

				$USUARIOS = new DELIVERABLES_MODEL($_REQUEST['idtrabajo'],'','','','');
				$usuariosConEntrega = $USUARIOS->SEARCHUSUARIOSENVIOTRABAJOS();

				while($user = $usuariosConEntrega->fetch_assoc()){ // Recorremos todos los usuarios que realizaron entrega
					$EVALUATION = new EVALUATION_MODEL($_REQUEST['idtrabajo'],$user['login'],'','','','','','','');
					$evaluacionesEntregas = $EVALUATION->SEARCH();
					$numeroQAS = $evaluacionesEntregas->num_rows;
					$contadorOK = 0;
							while($qasUsuario = $evaluacionesEntregas->fetch_assoc()){ // Recorremos todas las QAS que hizo ese usuario
									if($qasUsuario['ok'] == 1) $contadorOK = $contadorOK + 1;
							}
					$resultado = ($porcentaje[4]/100)*$contadorOK / $numeroQAS;
					$NOTAS = new JOBMARK_MODEL($user['login']	,$_REQUEST['idtrabajo'],$resultado);
					$NOTAS->ADD();
				}
				new MESSAGE('Generación de notas realizada con éxito', './Jobs_Controller.php');
				break;


		case 'EDIT': //si la acción recibida es EDIT

			if (!$_POST){//si no viene del POST


				$JOB = new JOB_MODEL($_REQUEST['idtrabajo'], '', '','',''); //creamos un objeto del modelo de datos que recoja los valores
				$valores = $JOB->RellenaDatos(); //le aplicamos la función RellenaDatos a estos valores
				new JOB_EDIT($valores); //se mostrará una vista de edit con los valores recogidos
			}
			else{

				$JOB = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form



				$respuesta = $JOB->EDIT(); //y les aplicamos la función edit
				new MESSAGE($respuesta, './Jobs_Controller.php'); //se mostrará un mensaje en función del resultado de la edición

			}

			break;


		case 'SEARCH': //si la acción recibida es SEARCH

			if (!$_POST){ //si los datos no vienen del POST

				new JOB_SEARCH();  //se creará una nueva vista de SEARCH
			}
			else{
				$JOB = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form

				$datos = $JOB->SEARCH();  //y les aplicamos la función search
				$lista = array('ID Trabajo','Nombre Trabajo','Fecha Inicio','Fecha Fin','Porcentaje Nota'); //creamos un array que se guardará en esta variable
				new JOB_SHOWALL($lista, $datos, '../index.php'); //se mostrará una nueva vista de SHOWALL con los resultado de la busqueda
			}
			break;




		case 'SHOWCURRENT': //si la acción recibida es SHOWCURRENT

			$JOB = new JOB_MODEL($_REQUEST['idtrabajo'], '', '','',''); //creamos un objeto del modelo de datos que recoja los valores
			$valores = $JOB->RellenaDatos();//le aplicamos la función RellenaDatos a estos valores
			new JOB_SHOWCURRENT($valores); //se creará una nueva vista con los valores pertenecientes a la tuple seleccionada
			break;


		default: //la acción por defecto será la vista de SHOWALL

			if (!$_POST){ //si no viene del POST

				$JOB = new JOB_MODEL('','', '','',''); //creamos un objeto del modelo de datos que recoja los valores
			}

			else{

				$JOB = get_data_form(); //se almacenarán los datos recogidos con la función get_data_form

			}

			$datos = $JOB->SEARCH(); //y les aplicamos la función search
			$lista = array('ID Trabajo','Nombre Trabajo','Fecha Inicio','Fecha Fin','Porcentaje Nota'); //creamos un array que se guardará en esta variable
			new JOB_SHOWALL($lista, $datos);//se mostrará una nueva vista de SHOWALL con todos los datos de la tabla

}






 ?>
