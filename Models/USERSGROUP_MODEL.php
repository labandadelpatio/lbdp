
<?php
/*  Fichero para el archivo del modelo de asignar grupos a usuarios
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 10/12/2017*/

 class USERSGROUP_MODEL
 {
 	var $idgrupo; //definimos la variable idgrupo que almacenará el valor perteneciente a dicha variable
 	var $login; //definimos la variable login que almacenará el valor perteneciente a dicha variable
 	var $bd; //definimos la variable bd que almacenará el valor perteneciente a dicha variable

 	function __construct( $idgrupo, $login ){

 		$this->idgrupo = $idgrupo; //asignamos al atributo idgrupo el valor del parametro idfuncionalidad
 		$this->login = $login; //asignamos al atributo login el valor del parametro idfuncionalidad

		include_once '../Models/BdAdmin.php'; // incluimos la función de acceso a la bd
		$this->mysqli = ConectarBD();  //almacenamos la conexión con la bd
	}

  //funcion ADD()
  //Mediante esta función añadiremos un USUARIO a un GRUPO 

	function ADD(){


		if (($this->idgrupo <> '') || ($this->login <> '')){ // si el atributo clave de la entidad no esta vacio

		// construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM USU_GRUPO WHERE (idgrupo = '$this->idgrupo') && (login = '$this->login') ";

		if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ // miramos si el resultado de la consulta es vacio (no existe el login)
				//construimos la sentencia sql de inserción en la bd
				$sql = "INSERT INTO USU_GRUPO (
					idgrupo,
					login
					)
						VALUES (
						'$this->idgrupo',
						'$this->login'
						)";


				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción';
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}

			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje

        return 'Introduzca un valor'; // introduzca un valor para el usuario

	}

	}

  //funcion EDIT()
  //Mediante esta función editaremos los usuarios que pertenecen a un grupo


	function EDIT()
{
	// se construye la sentencia de busqueda de la tupla en la bd
    $sql = "SELECT * FROM  USU_GRUPO  WHERE ((idgrupo = '$this->idgrupo') && (login = '$this->login'))";
    // se ejecuta la query
    $result = $this->mysqli->query($sql);
    // si el numero de filas es igual a uno es que lo encuentra

    if ($result->num_rows == 1)

    {	// se construye la sentencia de modificacion en base a los atributos de la clase

		$sql = "UPDATE USU_GRUPO  SET
						idgrupo = '$this->idgrupo',
						login = '$this->login'


				WHERE ((idgrupo = '$this->idgrupo') && (login = '$this->login'))";


		// si hay un problema con la query se envia un mensaje de error en la modificacion
        if (!($resultado = $this->mysqli->query($sql))){
			return 'Error en la modificación';
		}
		else{ // si no hay problemas con la modificación se indica que se ha modificado
			return 'Modificado correctamente';
		}
    }

    else{ // si no se encuentra la tupla se manda el mensaje de que no existe la tupla

    	return 'No existe en la base de datos';
    }
}


  //funcion SEARCH()
  //Mediante esta funcion buscaremos los datos relativos a un usuario de un grupo

	function SEARCH()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "select idgrupo,
    				login

       	    from  USU_GRUPO where

    				((idgrupo LIKE '%$this->idgrupo%') &&
    				(login LIKE '%$this->login%'))";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
} // fin metodo SEARCH





		// funcion DELETE()
		// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
		// se manda un mensaje de que ese valor de clave no existe
		function DELETE()
		{	// se construye la sentencia sql de busqueda con los atributos de la clase
		    $sql = "SELECT * FROM  USU_GRUPO  WHERE  ((IdGrupo = '$this->idgrupo' ) && (login = '$this->login')) ";
		    // se ejecuta la query
        if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
    			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
    		}
       // si existe una tupla con ese valor de clave
		    if ($result->num_rows == 1)
		    {
		    	// se construye la sentencia sql de borrado
		        $sql = "DELETE FROM  USU_GRUPO  WHERE ((IdGrupo = '$this->idgrupo') && (login = '$this->login'))";
		        // se ejecuta la query
		        $this->mysqli->query($sql);
		        // se devuelve el mensaje de borrado correcto
		    	  return "Borrado correctamente";
		    } // si no existe el login a borrar se devuelve el mensaje de que no existe
		    else
		        return "No existe";
		} // fin metodo DELETE

		// funcion RellenaDatos()
		// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
		// en el atributo de la clase
		function RellenaDatos()
		{	// se construye la sentencia de busqueda de la tupla
		    $sql = "SELECT * FROM USU_GRUPO   WHERE  ((idgrupo = '$this->idgrupo') && (login = '$this->login'))";
		    // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		    if (!($resultado = $this->mysqli->query($sql))){
				return 'No existe en la base de datos'; //
			}
		    else{ // si existe se devuelve la tupla resultado
				$result = $resultado->fetch_array();
				return $result;
			}
		} // fin del metodo RellenaDatos()


     /*  rUser()
    *   Comprueba si existe en la tabla USU_GRUPO de la base de datos un
    *   idgrupo con un login
    */
    function rUser(){
        $sql = "SELECT * FROM USU_GRUPO WHERE ((IdGrupo = '$this->idgrupo') && (login = '$this->login'))";
        if (!($resultado = $this->mysqli->query($sql))){
          return false;
        }
        if ($resultado->num_rows == 1){
          return true;
        }
        else return false;
    }



    /* Metodo comprobarControlAccesoU()
    * Comrpueba si el login que esta conectado tiene permisos para realizar o ver
    * ciertas acciones del sistema(A nivel de usuarios)
    */
   function comprobarControlAccesoAdmin(){

     $sql = "SELECT * FROM USU_GRUPO WHERE ((login = '$this->login') && (IdGrupo = 1))"; //Almacenamos la consulta sql, el grupo al que pertenece ese usuario
     $resultado = $this->mysqli->query($sql);

     if ($resultado->num_rows == 1){ // Si es admin
       return true;
     }
     else{ // Si no es admin
       return false;
     }
     //return $resultado; // Devuelve el idGrupo al que pertenece un login.
   }

   /* Metodo comprobarGrupo()
   * Comrpueba a que grupo pertenece el login que está conectado al sistema
   */
  function comprobarGrupo(){

    $sql = "SELECT * FROM USU_GRUPO WHERE (login = '$this->login')"; //Almacenamos la consulta sql, el grupo al que pertenece ese usuario
    $resultado = $this->mysqli->query($sql);
    //$result = $resultado->fetch_array();
    return $resultado;
  }


  // funcion DELETE()
  // comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
  // se manda un mensaje de que ese valor de clave no existe
  function DELETEUSUARIO()
  {	// se construye la sentencia sql de busqueda con los atributos de la clase
      $sql = "SELECT * FROM  USU_GRUPO  WHERE (login = '$this->login') ";
      // se ejecuta la query
      if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
        return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
      }
     // si existe una tupla con ese valor de clave
      if ($result->num_rows >= 1)
      {
        // se construye la sentencia sql de borrado
          $sql = "DELETE FROM  USU_GRUPO  WHERE  (login = '$this->login')";
          // se ejecuta la query
          $this->mysqli->query($sql);
          // se devuelve el mensaje de borrado correcto
          return "Borrado correctamente";
      } // si no existe el login a borrar se devuelve el mensaje de que no existe
      else
          return "No existe";
  } // fin metodo DELETE

}

?>
