
<?php

/*  Fichero para el archivo del modelo de trabajos
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 10/12/2017*/

 class JOB_MODEL
 {
 	var $idtrabajo; //definimos la variable idtrabajo que almacenará el valor perteneciente a dicha variable
 	var $nomtrabajo; //definimos la variable nomtrabajo que almacenará el valor perteneciente a dicha variable
 	var $fecini; //definimos la variable fecini que almacenará el valor perteneciente a dicha variable
 	var $fecfin; //definimos la variable fecfin que almacenará el valor perteneciente a dicha variable
 	var $porcentajenota; //definimos la variable porcentajenota que almacenará el valor perteneciente a dicha variabl
 	var $bd; //definimos la variable bd que almacenará el valor perteneciente a dicha variable



 	function __construct($idtrabajo,$nomtrabajo,$fecini, $fecfin, $porcentajenota){

 		$this->idtrabajo = $idtrabajo; //asignamos al atributo idtrabajo el valor del parametro idtrabajo
 		$this->nomtrabajo = $nomtrabajo; //asignamos al atributo nombreTrabajo el valor del parametro nombreTrabajo
		$this->fecini = $fecini; //asignamos al atributo fecini el valor del parametro fecini
		$this->fecfin = $fecfin; // asignamos al atributo fecfin el valor del parametro fecfin
	    $this->porcentajenota = $porcentajenota; //asignamos al atributo porcentajenota el valor del parametro porcentajenota


		if ($fecini == ''){ //si la variable fecini viene vacía
		$this->fecini = $fecini; //le asigamos el valor del parametro fecini
		}
		else{ // si no viene vacia le cambiamos el formato para que se adecue al de la bd
		$this->fecini = date_format(date_create($fecini), 'Y-m-d');
		}

		if ($fecfin == ''){ //si la variable fecfin viene vacía
		$this->fecfin = $fecfin; //le asignamos el valor del parametro fecfin
		}
		else{ // si no viene vacia le cambiamos el formato para que se adecue al de la bd
		$this->fecfin = date_format(date_create($fecfin), 'Y-m-d');
		}


		include_once '../Models/BdAdmin.php'; // incluimos la función de acceso a la bd
		$this->mysqli = ConectarBD();  //almacenamos la conexión con la bd
	}

	//funcion ADD()
	//Mediante esta funcion añadiremos un nuevo TRABAJO a la bd


	function ADD(){


		if (($this->idtrabajo <> '')){ // si el atributo clave de la entidad no esta vacio

		// construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM TRABAJO WHERE (	IdTrabajo = '$this->idtrabajo')";

		if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ // miramos si el resultado de la consulta es vacio (no existe el login)
				//construimos la sentencia sql de inserción en la bd
				$sql = "INSERT INTO TRABAJO (
						IdTrabajo,
						NombreTrabajo,
						FechaIniTrabajo,
						FechaFinTrabajo,
						PorcentajeNota 

					)
						VALUES (
						'$this->idtrabajo',
						'$this->nomtrabajo',
						'$this->fecini',
						'$this->fecfin',
						'$this->porcentajenota'
						)";


				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción';
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}

			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje

        return 'Introduzca un valor'; // introduzca un valor para el usuario

	}

	}//fin del metodo ADD

	//funcion EDIT()
	//Mediante esta funcion editaremos un trabajo ya existente en la bd


	function EDIT()
{
	// se construye la sentencia de busqueda de la tupla en la bd
    $sql = "SELECT * FROM TRABAJO  WHERE (IdTrabajo = '$this->idtrabajo') ";
    // se ejecuta la query
    $result = $this->mysqli->query($sql);
    // si el numero de filas es igual a uno es que lo encuentra

    if ($result->num_rows == 1)

    {	// se construye la sentencia de modificacion en base a los atributos de la clase

		$sql = "UPDATE TRABAJO  SET
						IdTrabajo = '$this->idtrabajo',
						NombreTrabajo = '$this->nomtrabajo',
					    FechaIniTrabajo = '$this->fecini',
						FechaFinTrabajo  = '$this->fecfin',
						PorcentajeNota  = '$this->porcentajenota'

				WHERE ( IdTrabajo = '$this->idtrabajo')";


		// si hay un problema con la query se envia un mensaje de error en la modificacion
        if (!($resultado = $this->mysqli->query($sql))){
			return 'Error en la modificación';
		}
		else{ // si no hay problemas con la modificación se indica que se ha modificado
			return 'Modificado correctamente';
		}
    }

    else{ // si no se encuentra la tupla se manda el mensaje de que no existe la tupla

    	return 'No existe en la base de datos';
    }
}


	//funcion SEACRH()
	//Mediante esta funcion buscaremos información sobre un TRABAJO

	function SEARCH()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "select IdTrabajo,
    				        NombreTrabajo,
    				        FechaIniTrabajo,
                    FechaFinTrabajo,
					          PorcentajeNota
       	    from TRABAJO
            where

    				((IdTrabajo LIKE '%$this->idtrabajo%') &&
    				(NombreTrabajo LIKE '%$this->nomtrabajo%') &&
    				(FechaIniTrabajo LIKE '%$this->fecini%') &&
	 				  (FechaFinTrabajo LIKE '%$this->fecfin%') &&
	 				  (PorcentajeNota LIKE '%$this->porcentajenota%'))";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
} // fin metodo SEARCH
	
	//funcion SEARCHDELIVERABLES()
	//Mediante esta función mostraremos todas las ENTREGAS

	function SEARCHDELIVERABLES()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "select IdTrabajo,
    				        NombreTrabajo,
    				        FechaIniTrabajo,
                    FechaFinTrabajo,
					          PorcentajeNota
       	    from TRABAJO
            where

    				((NombreTrabajo LIKE 'ET%') &&
    				(IdTrabajo LIKE '%$this->idtrabajo%') &&
    				(FechaIniTrabajo LIKE '%$this->fecini%') &&
	 				  (FechaFinTrabajo LIKE '%$this->fecfin%') &&
	 				  (PorcentajeNota LIKE '%$this->porcentajenota%'))";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
} // fin metodo SEARCHDELIVERABLES

	//funcion SEARCHQAS()
	//Mediante esta funcion mostraremos todos los QAS
function SEARCHQAS(){
	 $sql = "select IdTrabajo,
    				        NombreTrabajo,
    				        FechaIniTrabajo,
                    FechaFinTrabajo,
					          PorcentajeNota
       	    from TRABAJO
            where

    				((NombreTrabajo LIKE 'QA%') &&
    				(IdTrabajo LIKE '%$this->idtrabajo%') &&
    				(FechaIniTrabajo LIKE '%$this->fecini%') &&
	 				  (FechaFinTrabajo LIKE '%$this->fecfin%') &&
	 				  (PorcentajeNota LIKE '%$this->porcentajenota%'))";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
}





/*FUNCION PARA TRAERSE EL NOMBRE DE TRABAJO A EVALUACION*/

function SEARCHNOMBRE(){


	 $sql = "SELECT NombreTrabajo
       	    FROM TRABAJO WHERE
    				(IdTrabajo LIKE '$this->idtrabajo')";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado

      while($ret = $resultado->fetch_assoc()){
          $segm = $ret['NombreTrabajo'];
          return $segm;
      }



	}
}







		// funcion DELETE()
		// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
		// se manda un mensaje de que ese valor de clave no existe
		function DELETE()
		{	// se construye la sentencia sql de busqueda con los atributos de la clase
		    $sql = "SELECT * FROM  TRABAJO WHERE (IdTrabajo = '$this->idtrabajo') ";
		    // se ejecuta la query
		    $result = $this->mysqli->query($sql);
		    // si existe una tupla con ese valor de clave
		    if ($result->num_rows == 1)
		    {
		    	// se construye la sentencia sql de borrado
		        $sql = "DELETE FROM TRABAJO WHERE (IdTrabajo = '$this->idtrabajo')";
		        // se ejecuta la query
		        $this->mysqli->query($sql);
		        // se devuelve el mensaje de borrado correcto
		    	return "Borrado correctamente";
		    } // si no existe el login a borrar se devuelve el mensaje de que no existe
		    else
		        return "No existe";
		} // fin metodo DELETE

		// funcion RellenaDatos()
		// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
		// en el atributo de la clase
		function RellenaDatos()
		{	// se construye la sentencia de busqueda de la tupla
		    $sql = "SELECT * FROM TRABAJO WHERE (IdTrabajo = '$this->idtrabajo')";
		    // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		    if (!($resultado = $this->mysqli->query($sql))){
				return 'No existe en la base de datos'; //
			}
		    else{ // si existe se devuelve la tupla resultado
				$result = $resultado->fetch_array();
				return $result;
			}
		} // fin del metodo RellenaDatos()


}

?>
