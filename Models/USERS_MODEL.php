
<?php
/*  Fichero para el archivo del modelo de usuarios
   Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 10/12/2017*/
 class USERS_MODEL
 {
 	var $login; //definimos la variable login que almacenará el valor perteneciente a dicha variable
 	var $password;//definimos la variable password que almacenará el valor perteneciente a dicha variable
 	var $dni;//definimos la variable dni que almacenará el valor perteneciente a dicha variable
 	var $nombre;//definimos la variable nombre que almacenará el valor perteneciente a dicha variable
 	var $apellidos; //definimos la variable apelldios que almacenará el valor perteneciente a dicha variable
 	var $correo;//definimos la variable correo que almacenará el valor perteneciente a dicha variable
 	var $direccion; //definimos la variable direccion que almacenará el valor perteneciente a dicha variable
    var $telefono; //definimos la variable telefono que almacenará el valor perteneciente a dicha variable
	var $bd; //definimos la variable bd que almacenará el valor perteneciente a dicha variable
	



 	function __construct($login,$password,$dni, $nombre, $apellidos, $correo,$direccion, $telefono){
 		$this->login = $login; //asignamos al atributo login el valor del parametro login
 		$this->password = $password; //asignamos al atributo password el valor del parametro password
		$this->dni = $dni; //asignamos al atributo dni el valor del parametro dni
		$this->nombre = $nombre; // asignamos al atributo nombre el valor del parametro nombre
		$this->apellidos = $apellidos; //asignamos al atributo apellidos el valor del parametro apellidos
		$this->correo = $correo; //asignamos al atributo correo el valor del parametro correo
        $this->direccion = $direccion; //asignamos al atributo direccion el valor del parametro direccion
        $this->telefono = $telefono; //asignamos al atributo telefoono el valor del parametro telefono
		include_once '../Models/BdAdmin.php'; // incluimos la función de acceso a la bd
		$this->mysqli = ConectarBD();  //almacenamos la conexión con la bd
	}

	//funcion ADD()
	//Mediante esta funcion añadiremos un nuevo USUARIO a la bd

	function ADD(){


		if (($this->login <> '')){ // si el atributo clave de la entidad no esta vacio

		// construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM USUARIO WHERE (login = '$this->login')";

		if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ // miramos si el resultado de la consulta es vacio (no existe el login)
				//construimos la sentencia sql de inserción en la bd
				$sql = "INSERT INTO USUARIO (
					login,
					password,
					dni,
					nombre,
					apellidos,
					correo,
					direccion,
                    telefono

					)
						VALUES (
						'$this->login',
						'$this->password',
						'$this->dni',
						'$this->nombre',
						'$this->apellidos',
						'$this->correo',
						'$this->direccion',
                        '$this->telefono'
						)";


				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción';
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}

			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje

        return 'Introduzca un valor'; // introduzca un valor para el usuario

	}

	}

	//funcion EDIT()
	//Mediante esta funcion editaremos un USUARIO ya existente en la bd

	function EDIT()
{
	// se construye la sentencia de busqueda de la tupla en la bd
    $sql = "SELECT * FROM USUARIO  WHERE (login = '$this->login') ";
    // se ejecuta la query
    $result = $this->mysqli->query($sql);
    // si el numero de filas es igual a uno es que lo encuentra

    if ($result->num_rows == 1)

    {	// se construye la sentencia de modificacion en base a los atributos de la clase

		$sql = "UPDATE USUARIO  SET
						login = '$this->login',
						password = '$this->password',
					    dni = '$this->dni',
						nombre = '$this->nombre',
						apellidos = '$this->apellidos',
						correo = '$this->correo',
						direccion = '$this->direccion',
                        telefono = '$this->telefono'

				WHERE ( login = '$this->login')";


		// si hay un problema con la query se envia un mensaje de error en la modificacion
        if (!($resultado = $this->mysqli->query($sql))){
			return 'Error en la modificación';
		}
		else{ // si no hay problemas con la modificación se indica que se ha modificado
			return 'Modificado correctamente';
		}
    }

    else{ // si no se encuentra la tupla se manda el mensaje de que no existe la tupla

    	return 'No existe en la base de datos';
    }
}

	//funcion SEARCH()
	//Medante esta funcion buscaremos cualquier dato relativo a un USUARIO


	function SEARCH()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "select login,
    				password,
    				dni,
                    nombre,
					apellidos,
					correo,
					direccion,
                    telefono
       	    from USUARIO where

    				((dni LIKE '%$this->dni%') &&
    				(login LIKE '%$this->login%') &&
    				(password LIKE '%$this->password%') &&
	 				(nombre LIKE '%$this->nombre%') &&
	 				(apellidos LIKE '%$this->apellidos%') &&
	 				(correo LIKE '%$this->correo%') &&
	 				(direccion LIKE '%$this->direccion%') &&
                    (telefono LIKE '%$this->telefono%'))";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
} // fin metodo SEARCH





		// funcion DELETE()
		// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
		// se manda un mensaje de que ese valor de clave no existe
		function DELETE()
		{	// se construye la sentencia sql de busqueda con los atributos de la clase
		    $sql = "SELECT * FROM USUARIO  WHERE (login = '$this->login') ";
		    // se ejecuta la query
		    $result = $this->mysqli->query($sql);
		    // si existe una tupla con ese valor de clave
		    if ($result->num_rows == 1)
		    {
		    	// se construye la sentencia sql de borrado
		        $sql = "DELETE FROM  USUARIO WHERE (login = '$this->login')";
		        // se ejecuta la query
		        $this->mysqli->query($sql);
		        // se devuelve el mensaje de borrado correcto
		    	return "Borrado correctamente";
		    } // si no existe el login a borrar se devuelve el mensaje de que no existe
		    else
		        return "No existe";
		} // fin metodo DELETE

		// funcion RellenaDatos()
		// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
		// en el atributo de la clase
		function RellenaDatos()
		{	// se construye la sentencia de busqueda de la tupla
		    $sql = "SELECT * FROM  USUARIO WHERE (login = '$this->login')";
		    // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		    if (!($resultado = $this->mysqli->query($sql))){
				return 'No existe en la base de datos'; //
			}
		    else{ // si existe se devuelve la tupla resultado
				$result = $resultado->fetch_array();
				return $result;
			}
		} // fin del metodo RellenaDatos()





		// funcion login: realiza la comprobación de si existe el usuario en la bd y despues si la pass
// es correcta para ese usuario. Si es asi devuelve true, en cualquier otro caso devuelve el
// error correspondiente
function Login(){

	$sql = "SELECT *
			FROM USUARIO
			WHERE (
				(login = '$this->login')
			)";
	$resultado = $this->mysqli->query($sql);
	if ($resultado->num_rows == 0){
		return 'El login no existe';
	}
	else{
		$tupla = $resultado->fetch_array();
		if ($tupla['password'] == $this->password){
			return true;
		}
		else{
			return 'La contraseña para este usuario no es correcta';
		}
	}
}//fin metodo login

//
function Register(){

		$sql = "select * from USUARIO where login = '".$this->login."'";

		$result = $this->mysqli->query($sql);
		if ($result->num_rows == 1){  // existe el usuario
				return 'El usuario ya existe';
			}
		else{
	    		return true; //no existe el usuario
		}

	}

	//funcion registrar()
	//Mediante esta funcion registraremos un nuevo usuario en la bd

function registrar(){

		//sentencia de inserción
		$sql = "INSERT INTO USUARIO (
			login,
			password,
			dni,
			nombre,
			apellidos,
			correo,
            direccion,
            telefono
			)
				VALUES (
					'".$this->login."',
					'".$this->password."',
					'".$this->dni."',
					'".$this->nombre."',
					'".$this->apellidos."',
					'".$this->correo."',
                    '".$this->direccion."',
                    '".$this->telefono."'
					)";

		if (!$this->mysqli->query($sql)) { //si ha habido algun fallo en la sentencia
			return 'Error en la inserción';
		}
		else{
			return 'Inserción realizada con éxito'; //operacion de insertado correcta
		}
	}




}




















?>
