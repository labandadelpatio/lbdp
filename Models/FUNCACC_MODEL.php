
<?php
/*  Fichero para el modelo de asignar acciones a funcionalidades
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 10/12/2017*/

 class FUNCACC_MODEL
 {
 	var $idfuncionalidad; //definimos la variable idfuncionalidad que almacenará el valor perteneciente a dicha variable
 	var $idaccion; //definimos la variable idaccion que almacenará el valor perteneciente a dicha variable
 	var $bd; //definimos la variable bd que almacenará el valor perteneciente a dicha variable

 	function __construct($idfuncionalidad, $idaccion ){

 		$this->idfuncionalidad = $idfuncionalidad;  //asignamos al atributo idfuncionalidad el valor del parametro idfuncionalidad
 		$this->idaccion = $idaccion;  //asignamos al atributo idaccio el valor del parametro idaccion


		include_once '../Models/BdAdmin.php'; // incluimos la función de acceso a la bd
		$this->mysqli = ConectarBD();  //almacenamos la conexión con la bd
	}

	//funcion ADD()
	//Mediante esta funcion añadiremos una FUNC_ACCION a la bd

	function ADD(){


		if (($this->idfuncionalidad <> '') || ($this->idaccion <> '')){ // si el atributo clave de la entidad no esta vacio

		// construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM FUNC_ACCION WHERE (idfuncionalidad = '$this->idfuncionalidad') && (idaccion = '$this->idaccion') ";

		if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ // miramos si el resultado de la consulta es vacio (no existe el login)
				//construimos la sentencia sql de inserción en la bd
				$sql = "INSERT INTO FUNC_ACCION (
					idfuncionalidad,
					idaccion
					)
						VALUES (
						'$this->idfuncionalidad',
						'$this->idaccion'
						)";


				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción';
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}

			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje

        return 'Introduzca un valor'; // introduzca un valor para el usuario

	}

	}//fin del metodo ADD

	//funcion EDIT()
	//mediante esta funcion editaremos una func_Acc ya existente en la bd


	function EDIT()
{
	// se construye la sentencia de busqueda de la tupla en la bd
    $sql = "SELECT * FROM  FUNC_ACCION  WHERE ((idfuncionalidad = '$this->idfuncionalidad') && (idaccion = '$this->idaccion'))";
    // se ejecuta la query
    $result = $this->mysqli->query($sql);
    // si el numero de filas es igual a uno es que lo encuentra

    if ($result->num_rows == 1)
    {	// se construye la sentencia de modificacion en base a los atributos de la clase

		$sql = "UPDATE USU_GRUPO  SET
						idfuncionalidad = '$this->idfuncionalidad',
						idaccion = '$this->idaccion'


				WHERE ((idfuncionalidad = '$this->idfuncionalidad') && (idaccion = '$this->idaccion'))";


		// si hay un problema con la query se envia un mensaje de error en la modificacion
        if (!($resultado = $this->mysqli->query($sql))){
			return 'Error en la modificación';
		}
		else{ // si no hay problemas con la modificación se indica que se ha modificado
			return 'Modificado correctamente';
		}
    }

    else{ // si no se encuentra la tupla se manda el mensaje de que no existe la tupla

    	return 'No existe en la base de datos';
    }
}


//funcion SEARCH()
//Mediante esta función buscaremos una fun_acc

	function SEARCH()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "select idfuncionalidad,
    				idaccion

       	    from  FUNC_ACCION where

    				((idfuncionalidad LIKE '%$this->idfuncionalidad%') &&
    				(idaccion LIKE '%$this->idaccion%'))";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
} // fin metodo SEARCH





		// funcion DELETE()
		// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
		// se manda un mensaje de que ese valor de clave no existe
		function DELETE()
		{	// se construye la sentencia sql de busqueda con los atributos de la clase
		    $sql = "SELECT * FROM  FUNC_ACCION  WHERE  ((idfuncionalidad = '$this->idfuncionalidad') && (idaccion = '$this->idaccion')) ";
		    // se ejecuta la query
		    $result = $this->mysqli->query($sql);
		    // si existe una tupla con ese valor de clave
		    if ($result->num_rows == 1)
		    {
		    	// se construye la sentencia sql de borrado
		        $sql = "DELETE FROM  FUNC_ACCION  WHERE  ((idfuncionalidad = '$this->idfuncionalidad') && (idaccion = '$this->idaccion'))";
		        // se ejecuta la query
		        $this->mysqli->query($sql);
		        // se devuelve el mensaje de borrado correcto
		    	return "Borrado correctamente";
		    } // si no existe el login a borrar se devuelve el mensaje de que no existe
		    else
		        return "No existe";
		} // fin metodo DELETE

		// funcion RellenaDatos()
		// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
		// en el atributo de la clase
		function RellenaDatos()
		{	// se construye la sentencia de busqueda de la tupla
		    $sql = "SELECT * FROM FUNC_ACCION   WHERE  ((idfuncionalidad = '$this->idfuncionalidad') && (idaccion = '$this->idaccion'))";
		    // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		    if (!($resultado = $this->mysqli->query($sql))){
				return 'No existe en la base de datos'; //
			}
		    else{ // si existe se devuelve la tupla resultado
				$result = $resultado->fetch_array();
				return $result;
			}
		} // fin del metodo RellenaDatos()



		function RellenaDatosNombres()
		{	// se construye la sentencia de busqueda de la tupla
		    $sql = "SELECT FUNC_ACCION.IdFuncionalidad, FUNC_ACCION.IdAccion, NombreFuncionalidad, NombreAccion FROM FUNC_ACCION, FUNCIONALIDAD, ACCION WHERE ((FUNCIONALIDAD.IdFuncionalidad = FUNC_ACCION.IdFuncionalidad) AND FUNC_ACCION.IdAccion = ACCION.idaccion)";
		    // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		    if (!($resultado = $this->mysqli->query($sql))){
				return 'No existe en la base de datos'; //
			}
		    else{ // si existe se devuelve la tupla resultado
				
				return $resultado;
			}
		}





    /* function mostrarFuncionalidades()
    *   Muestra todas las distintas Funcionalidades de la aplicación
    */
    function RellenaDatosFuncionalidad(){

      $sql = "SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad = $this->idfuncionalidad";

      // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    	if (!$resultado_Funcionalidad = $this->mysqli->query($sql)){
    		return 'Error en la consulta sobre la base de datos';
    	}
    	else{ // si la busqueda es correcta devolvemos el recordset resultado
        $result = $resultado_Funcionalidad->fetch_array();
    		return $result;
    	}
    }


    /*  rAccion()
    *   Comprueba si existe uen la tabla FUNC_ACCION de la base de datos un
    *   idfuncionalidad con un idaccion
    */
    function rAccion(){
        $sql = "SELECT * FROM FUNC_ACCION WHERE ((IdFuncionalidad = '$this->idfuncionalidad') && (IdAccion = '$this->idaccion'))";
        if (!($resultado = $this->mysqli->query($sql))){
          return false;
        }
        if ($resultado->num_rows == 1){
          return true;
        }
        else return false;
    }


   /* function mostrarAcciones()
   *   Muestra todas las acciones de la aplicación (Cuenta con repetidas para
   *   unirlas con las distintas funcionalidades)
   */
   /*
   function RellenaDatosAcciones(){

     $sql = "SELECT idaccion, nombreaccion, descripacion FROM ACCION"; // Almacenamos la consulta sql, el grupo al que pertenece ese usuario

     // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
     if (!$resultado_Accion = $this->mysqli->query($sql)){
       return 'Error en la consulta sobre la base de datos';
     }
     else{ // si la busqueda es correcta devolvemos el recordset resultado
       //$result = $resultado_Accion->fetch_array();
       return $resultado_Accion;
     }
   }
   */

}

?>
