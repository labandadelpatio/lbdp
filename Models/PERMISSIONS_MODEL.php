
<?php

/*  Fichero para el modelo de permisos
  Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 10/12/2017*/
 class PERMISSIONS_MODEL
 {
 	var $idfuncionalidad; //definimos la variable idfuncionalidad que almacenará el valor perteneciente a dicha variable
 	var $idaccion; //definimos la variable idaccion que almacenará el valor perteneciente a dicha variable
 	var $idgrupo; //definimos la variable idfgrupo que almacenará el valor perteneciente a dicha variable
 	var $bd; //definimos la variable bd que almacenará el valor perteneciente a dicha variable

 	function __construct($idgrupo,$idfuncionalidad,$idaccion){
 		$this->idgrupo = $idgrupo;  //asignamos al atributo idgrupo el valor del parametro idgrupo
 		$this->idfuncionalidad = $idfuncionalidad;  //asignamos al atributo idfuncionalidad el valor del parametro idfuncionalidad
 		$this->idaccion = $idaccion;  //asignamos al atributo idaccion el valor del parametro idaccion


		include_once '../Models/BdAdmin.php'; // incluimos la función de acceso a la bd
		$this->mysqli = ConectarBD();  //almacenamos la conexión con la bd
	}
     
     function ADD(){

         //fucnion ADD()
	   //Mediante esta función añadirmos un Permiso nuevo a la bd
		if (($this->idgrupo <> '') || ($this->idfuncionalidad <> '') || ($this->idaccion <> '')){ // si el atributo clave de la entidad no esta vacio

		// construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM PERMISO WHERE ((idgrupo = '$this->idgrupo') && (idfuncionalidad = '$this->idfuncionalidad') && (idaccion = '$this->idaccion'))";

		if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ // miramos si el resultado de la consulta es vacio (no existe el login)
				//construimos la sentencia sql de inserción en la bd
				$sql = "INSERT INTO PERMISO(
					idgrupo,
					idfuncionalidad,
					idaccion
					)
						VALUES (
						'$this->idgrupo',
						'$this->idfuncionalidad',
						'$this->idaccion'

						)";


				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción';
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}

			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje

        return 'Introduzca un valor'; // introduzca un valor para el usuario

	}

	}

	//fucnion EDIT()
	//Mediante esta función editaremos un Permiso ya existente en la bd

	function EDIT()
{
	// se construye la sentencia de busqueda de la tupla en la bd
    $sql = "SELECT * FROM  PERMISO WHERE (idgrupo = '$this->idgrupo') ";
    // se ejecuta la query
    $result = $this->mysqli->query($sql);
    // si el numero de filas es igual a uno es que lo encuentra

    if ($result->num_rows == 1)

    {	// se construye la sentencia de modificacion en base a los atributos de la clase

		$sql = "UPDATE PERMISO  SET
						 idgrupo = '$this->idgrupo',
						idfuncionalidad = '$this->idfuncionalidad',
					    idaccion = '$this->idaccion'

				WHERE ( idfuncionalidad = '$this->idfuncionalidad')";


		// si hay un problema con la query se envia un mensaje de error en la modificacion
        if (!($resultado = $this->mysqli->query($sql))){
			return 'Error en la modificación';
		}
		else{ // si no hay problemas con la modificación se indica que se ha modificado
			return 'Modificado correctamente';
		}
    }

    else{ // si no se encuentra la tupla se manda el mensaje de que no existe la tupla

    	return 'No existe en la base de datos';
    }
}


	//funcion SEARCH()
	//Mediante esta unción busaremos información relativa a un permiso

	function SEARCH()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "select  idgrupo,
    				idfuncionalidad,
    				idaccion

       	    from PERMISO where

    				((idgrupo LIKE '%$this->idgrupo%')&&
    				(idfuncionalidad LIKE '%$this->idfuncionalidad%') &&
    				(idaccion LIKE '%$this->idaccion%'))";


    		
    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
} // fin metodo SEARCH





		// funcion DELETE()
		// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
		// se manda un mensaje de que ese valor de clave no existe
		function DELETE()
		{	// se construye la sentencia sql de busqueda con los atributos de la clase
		    $sql = "SELECT * FROM PERMISO  WHERE ((idgrupo = '$this->idgrupo') && (idfuncionalidad = '$this->idfuncionalidad') && (idaccion = '$this->idaccion')) ";
		    // se ejecuta la query
		    $result = $this->mysqli->query($sql);
		    // si existe una tupla con ese valor de clave
		    if ($result->num_rows == 1)
		    {
		    	// se construye la sentencia sql de borrado
		        $sql = "DELETE FROM  PERMISO WHERE ((idgrupo = '$this->idgrupo') && (idfuncionalidad = '$this->idfuncionalidad') && (idaccion = '$this->idaccion'))";
		        // se ejecuta la query
		        $this->mysqli->query($sql);
		        // se devuelve el mensaje de borrado correcto
		    	return "Borrado correctamente";
		    } // si no existe el login a borrar se devuelve el mensaje de que no existe
		    else
		        return "No existe";
		} // fin metodo DELETE

		// funcion RellenaDatos()
		// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
		// en el atributo de la clase
		function RellenaDatos()
		{	// se construye la sentencia de busqueda de la tupla
		    $sql = "SELECT * FROM PERMISO  WHERE (idgrupo = '$this->idgrupo')";
		    // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		    if (!($resultado = $this->mysqli->query($sql))){
				return 'No existe en la base de datos'; //
			}
		    else{ // si existe se devuelve la tupla resultado
				$result = $resultado->fetch_array();
				return $result;
			}
		} // fin del metodo RellenaDatos()

		//funcion RellenaDatosNombres()
		//Mediante esta función mostraremos cada id con su nombre correspondiente de la tabla

		function RellenaDatosNombres()
		{	// se construye la sentencia de busqueda de la tupla
		    $sql = "SELECT PERMISO.IdGrupo, NombreGrupo, PERMISO.IdFuncionalidad, NombreFuncionalidad, PERMISO.IdAccion, NombreAccion FROM PERMISO, GRUPO, ACCION, FUNCIONALIDAD WHERE ((PERMISO.IdGrupo LIKE '%$this->idgrupo%') AND (PERMISO.IdFuncionalidad LIKE '%$this->idfuncionalidad%') AND (PERMISO.IdAccion LIKE '%$this->idaccion%') AND (PERMISO.IdGrupo = GRUPO.IdGrupo) AND (PERMISO.IdAccion = ACCION.IdAccion) AND (PERMISO.IdFuncionalidad = FUNCIONALIDAD.IdFuncionalidad))";
		    // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		    if (!($resultado = $this->mysqli->query($sql))){
				return 'No existe en la base de datos'; //
			}
		    else{ // si existe se devuelve la tupla resultado
				return $resultado;
			}
		}

		//funcion rPermiso()
		//Mediante esta funcion comprobaremos para cada grupo que permisos tiene

	function rPermiso(){
      $sql = "SELECT * FROM PERMISO WHERE ((IdGrupo = '$this->idgrupo') && (IdFuncionalidad = '$this->idfuncionalidad') && (IdAccion = '$this->idaccion'))";
      if (!($resultado = $this->mysqli->query($sql))){
        return false;
      }
      if ($resultado->num_rows == 1){ // Si hay una tupla en la bd
        return true;
      }
      else return false;
  }

  /* Funcion comprobarPermisosFunc()
  *   Comprueba si un grupo tiene permiso para acceder a una determinada funcionalidades
  */
  function comprobarPermisosFunc(){
      $sql = "SELECT * FROM PERMISO WHERE ((IdGrupo = '$this->idgrupo') && (IdFuncionalidad = '$this->idfuncionalidad'))";
      if (!($resultado = $this->mysqli->query($sql))){
        return false;
      }
      if ( $resultado->num_rows >= 1 ){ // Si hay una tupla en la bd
        return true;
      }
      else return false;
  }

  /* Funcion comprobarPermisosFuncAcc()
  *   Comprueba si un grupo tiene permiso para acceder acceder a una determinada
  *   funcionalidad y realizar una determinada accion
  */
  function comprobarPermisosFuncAcc(){
      $sql = "SELECT * FROM PERMISO WHERE ((IdGrupo = '$this->idgrupo') && (IdFuncionalidad = '$this->idfuncionalidad')
                                                && (IdAccion = '$this->idaccion'))";
      if (!($resultado = $this->mysqli->query($sql))){
        return false;
      }
      if ( $resultado->num_rows >= 1 ){ // Si hay una tupla en la bd
        return true;
      }
      else return false; // Si no hay tupla en la bd
  }

}

?>
