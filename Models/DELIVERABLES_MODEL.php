
<?php
/*  Fichero para el archivo del modelo de entregas
 Autores: Paula González Quintas, Francisco Lopez Alonso, Juio Quinteiro Soto, Andrés Soto de la Concepción, Milagros Somoza Salinas
  Fecha: 10/12/2017*/

 class DELIVERABLES_MODEL
 {
 	var $idtrabajo; //definimos la variable idtrabajo que almacenará el valor perteneciente a dicha variable
 	var $login; //definimos la variable login que almacenará el valor perteneciente a dicha variable
 	var $alias; //definimos la variable alias que almacenará el valor perteneciente a dicha variable
 	var $horas; //definimos la variable horas que almacenará el valor perteneciente a dicha variable
 	var $ruta; //definimos la variable ruta que almacenará el valor perteneciente a dicha variable
 	var $bd;//definimos la variable bd que almacenará el valor perteneciente a dicha variable


 	function __construct($idtrabajo,$login, $alias, $horas, $ruta){

 		$this->idtrabajo = $idtrabajo; //asignamos al atributo idtrabajo el valor del parametro idtrabajo
 		$this->login = $login; //asignamos al atributo login el valor del parametro login
 		$this->alias= $alias; //asignamos al atributo alias el valor del parametro alias
 		$this->horas = $horas; //asignamos al atributo horas el valor del parametro horas
 		$this->ruta= $ruta;


		include_once '../Models/BdAdmin.php'; // incluimos la función de acceso a la bd
		$this->mysqli = ConectarBD();  //almacenamos la conexión con la bd
	}

	//funcion ADD()
	//Mediante esta función añadiremos una entrega la bd


	function ADD(){


		if (($this->idtrabajo <> '') || ($this->login <> '')){ // si el atributo clave de la entidad no esta vacio

		// construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM ENTREGA  WHERE (IdTrabajo = '$this->idtrabajo') && (login = '$this->login')";

		if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ // miramos si el resultado de la consulta es vacio (no existe el login)
				//construimos la sentencia sql de inserción en la bd
				$sql = "INSERT INTO ENTREGA (
					login,
					IdTrabajo,
					Alias,
					Horas,
					Ruta

					)
						VALUES (
						'$this->login',
						'$this->idtrabajo',
						'$this->alias',
						'$this->horas',
						'$this->ruta'
						)";


				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción';
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}

			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje

        return 'Introduzca un valor'; // introduzca un valor para el usuario

	}

	}//fin del metodo ADD

	//funcion EDIT
	//Mediante esta función editaremos una ENTREGAS ya existente en la bd


	function EDIT()
{
	// se construye la sentencia de busqueda de la tupla en la bd
    $sql = "SELECT * FROM ENTREGA  WHERE (IdTrabajo = '$this->idtrabajo') && (login = '$this->login')";
    // se ejecuta la query
    $result = $this->mysqli->query($sql);
    // si el numero de filas es igual a uno es que lo encuentra
    $row = mysqli_fetch_array($result); // Obtenemos en u array todos los datos de ese usuario
    if ($result->num_rows == 1)

    {	// se construye la sentencia de modificacion en base a los atributos de la clase

		$sql = "UPDATE ENTREGA SET
						login = '$this->login',
						IdTrabajo = '$this->idtrabajo',
					    Alias = '$this->alias',
					    Horas = '$this->horas',
					    Ruta = '$this->ruta'

				WHERE (IdTrabajo = '$this->idtrabajo' && login = '$this->login')";


		// si hay un problema con la query se envia un mensaje de error en la modificacion
        if (!($resultado = $this->mysqli->query($sql))){
			return 'Error en la modificación';
		}
		else{ // si no hay problemas con la modificación se indica que se ha modificado
      //unlink($row['Ruta']); //Borramos del servidor el trabajo viejo
			return 'Modificado correctamente';
		}
    }

    else{ // si no se encuentra la tupla se manda el mensaje de que no existe la tupla

    	return 'No existe en la base de datos';
    }
}//fin de la funcion  EDIT

	//funcion SEARCH()
	//mediante esta función buscaremos una una ENTREGA en la bd

	function SEARCH()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "select login,
    				ENTREGA.IdTrabajo,
                    TRABAJO.NombreTrabajo,
    				Alias,
    				Horas,
    				Ruta

       	    from ENTREGA, TRABAJO where

    				((login LIKE '%$this->login%') &&
    				(ENTREGA.IdTrabajo LIKE '%$this->idtrabajo%') &&
                    (TRABAJO.IdTrabajo LIKE ENTREGA.IdTrabajo) &&
    				(Alias LIKE '%$this->alias%') &&
    				(Horas LIKE '%$this->horas%') &&
    				(Ruta LIKE '%$this->ruta%'))";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
} // fin metodo SEARCH

function SEARCHUSUARIOSENVIOTRABAJOS()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "SELECT login,
    				IdTrabajo,
    				Alias,
    				Horas,
    				Ruta

       	    FROM ENTREGA WHERE
    				(IdTrabajo LIKE '%$this->idtrabajo%') &&
    				(Ruta IS NOT NULL)";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
} // fin metodo SEARCHUSUARIOSENVIOTRABAJOS


function SEARCHRUTA()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "SELECT Ruta
       	    FROM ENTREGA WHERE
    				((IdTrabajo LIKE '$this->idtrabajo') &&
    				(Alias LIKE '$this->alias'))";

    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado

      while($ret = $resultado->fetch_assoc()){
          $segm = $ret['Ruta'];
          return $segm;
      }



	}
} // fin metodo SEARCHRUTA







		// funcion DELETE()
		// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
		// se manda un mensaje de que ese valor de clave no existe
		function DELETE()
		{	// se construye la sentencia sql de busqueda con los atributos de la clase
		    $sql = "SELECT * FROM  ENTREGA WHERE (IdTrabajo = '$this->idtrabajo') && (login = '$this->login')";
		    // se ejecuta la query
		    $result = $this->mysqli->query($sql);
		    // si existe una tupla con ese valor de clave
        $row = mysqli_fetch_array($result); // Obtenemos en u array todos los datos de ese usuario
		    if ($result->num_rows == 1)
		    {
		    	// se construye la sentencia sql de borrado
		        $sql = "DELETE FROM ENTREGA  WHERE (IdTrabajo = '$this->idtrabajo') && (login = '$this->login')";
		        // se ejecuta la query
		        $this->mysqli->query($sql);
		        // se devuelve el mensaje de borrado correcto
            unlink($row['Ruta']); //Borramos del servidor el trabajo
		    	return "Borrado correctamente";
		    } // si no existe el login a borrar se devuelve el mensaje de que no existe
		    else
		        return "No existe";
		} // fin metodo DELETE

		// funcion RellenaDatos()
		// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
		// en el atributo de la clase
		function RellenaDatos()
		{	// se construye la sentencia de busqueda de la tupla
		    $sql = "SELECT * FROM ENTREGA WHERE (IdTrabajo = '$this->idtrabajo') && (login = '$this->login')";
		    // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
		    if (!($resultado = $this->mysqli->query($sql))){
				return 'No existe en la base de datos'; //
			}
		    else{ // si existe se devuelve la tupla resultado
				$result = $resultado->fetch_array();
				return $result;
			}
		} // fin del metodo RellenaDatos()


    //Comrpueba si este usuario ha accedido ya  a la entrega para asignarle un alias o no
    function existe(){
      // se construye la sentencia sql de busqueda con los atributos de la clase (login y IdTrabajo)
      $sql = "SELECT * FROM ENTREGA WHERE (login = '$this->login' && IdTrabajo = '$this->idtrabajo') ";

      if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
  			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
  		}
      if($result->num_rows == 1){ // Si hay alguna tupla, es decir, y accedio a la entrega y se le asigno un alias
        return true;
      }
      else return false;
    }

/*
		function getRandomCode($alias){



		$an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$su = strlen($an) - 1;
		$alias= substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1) . substr($an, rand(0, $su), 1);
		return $alias;
  }
*/

  	function numeroEntregas(){
      // se construye la sentencia sql de busqueda con los atributos de la clase (login y IdTrabajo)
      $sql = "SELECT COUNT(*) FROM ENTREGA ";

      return $sql;
    }

    function entregaOk(){
      // se construye la sentencia sql de busqueda con los atributos de la clase (login y IdTrabajo)
      $sql = "SELECT * FROM ENTREGA WHERE (IdTrabajo = '$this->idtrabajo') && (login = '$this->login')";

      if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
  			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
  		}
  		if($result->num_rows == 1){ // Si hay alguna tupla, es decir, y accedio a la entrega y se le asigno un alias
       		return true;
    	}
    	else return false;
    }
}
?>
